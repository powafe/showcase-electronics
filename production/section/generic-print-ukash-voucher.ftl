<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title>Print Ukash Voucher</title>
    <link rel="stylesheet" href="${common.resourcePath}/styles/styles.css" type="text/css"/>
    <style type="text/css">
        @media print {
            .delivery {
                page-break-before: auto;
                page-break-inside: avoid;
                overflow: hidden;
            }
            .not-on-print {
                display: none;
            }
        }
    </style>
</head>
<body>
<div class="voucher">
    <input class="not-on-print middleBtn" value="Print" type="submit" onclick="window.print()"/>

    <div class="cont">
        <p>Thank you for shopping with ${section.shopName}, ${section.domainName}</p>

        <p>${section.refundDate?string('yyyy-MM-dd HH:mm')}</p>

        <p><img src="${common.resourcePath}/images/ukash_logo_medium.jpg"/></p>

        <p>Below are your UKASH voucher details, please keep them safe:</p>

        <p>VOUCHER VALUE: <b>${section.currency} ${section.amount}</b></p>

        <p>VOUCHER NO: <b>${section.voucherCode}</b></p>

        <p>USE BY: <b>${section.voucherExpirationDate?string('yyyy-MM-dd')}</b></p>

        <p>Please treat like cash. Only use online at genuine websites listed at <a href="http://www.ukash.com">www.ukash.com</a>
        </p>

        <p>For terms and conditions, customer service and more information go to <a href="http://www.ukash.com">www.ukash.com</a>
        </p>
    </div>
    <input class="not-on-print middleBtn" value="Cancel" type="submit" onclick="window.close()"/>
</div>
</body>
</html>