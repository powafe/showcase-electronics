<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<#macro renderSections sections>
    <#list sections as section>
        <#if section??>
        ${section}
        </#if>
    </#list>
</#macro>
<!DOCTYPE html>
<html lang="en">
<head>
<#if seo??>
    <title>${seo.title}</title>
    <#if seo.metaKeywords??>
    <meta name="keywords" content="${seo.metaKeywords}"/>
    </#if>
    <#if seo.metaDescription??>
    <meta name="description" content="${seo.metaDescription}"/>
    </#if>
</#if>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

    <link rel="stylesheet" href="${common.resourcePath}/styles/styles.css"/>
    <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <link rel="stylesheet" href="https://live.powatag.com/static/css/1.6.3/powatag.css"/>
    <link rel="shortcut icon" href="${common.resourcePath}/favicon.ico" type="image/x-icon" />

    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>    
    <script type="text/javascript" src="https://live.powatag.com/static/js/1.6.3/powatag.js"></script>
<#if headWebmasterVerification??>
${headWebmasterVerification}
</#if>
<#if common.googleAnalyticsEnabled>
    <#include "/${common.templatePath}google-analytics.ftl"/>
</#if>
</head>
<#if page.bodyClass??>
<body class="${page.bodyClass}">
<#else>
<body>
</#if>
<div class="demo-notice">
    <p>Please note this is a demo site and not for public use</p>
    <a class="close-demo-notice" href="#" title="Close"><i class="fa fa-times"></i></a>
</div>
<div id="wrapper">
    <div id="page">
        <div id="headerContainer"> 
            <#if page.headerContainer??>
                <@renderSections sections = page.headerContainer/>
            </#if>
        </div>
        <div id="middleContainer">
            <div class="container">
                <div class="row">
                    <#if (page.leftContainer??) && (page.leftContainer?size>0)>
                        <div class="leftContainer col-lg-3">
                            <#if page.leftContainer??>
                                <@renderSections sections = page.leftContainer/>
                            </#if>
                        </div>
                    </#if>
                    <div class="centerContainer">
                        <#if page.centerContainer??>
                            <@renderSections sections = page.centerContainer/>
                        </#if>
                    </div>
                    <#if (page.rightContainer??) && (page.rightContainer?size>0)>
                        <div class="rightContainer">
                            <#if page.rightContainer??>
                                <@renderSections sections = page.rightContainer/>
                            </#if>
                        </div>
                    </#if>
                </div>
            </div>
        </div>
        <#if (page.footerContainer??) && (page.footerContainer?size>0)>
            <div id="footerContainer">
                <#if page.footerContainer??>
                    <@renderSections sections = page.footerContainer/>
                </#if>
            </div>
        </#if>
    </div>
</div>
<#-- <script type="text/javascript" src="${common.resourcePath}/js/lib/jquery.inherit-1.0.7.min.js"></script>
<script type="text/javascript" src="${common.resourcePath}/js/lib/jquery.json.js"></script>
<script type="text/javascript" src="${common.resourcePath}/js/lib/prototype.js"></script>
<script type="text/javascript" src="${common.resourcePath}/js/lib/json2.js"></script>
<script type="text/javascript" src="${common.resourcePath}/js/lib/jsoner.commons.js"></script>
<script type="text/javascript" src="${common.resourcePath}/js/lib/xmldom.js"></script> 
<script type="text/javascript" src="${common.resourcePath}/js/common.js"></script>-->
<script type="text/javascript" src="${common.resourcePath}/messages/${common.localeMessages.locale}/messages_${common.localeMessages.locale}.js"></script>
<script type="text/javascript" src="${common.resourcePath}/js/validators.js"></script>
<script type="text/javascript" src="${common.resourcePath}/js/ajax_update.js"></script>

<script type="text/javascript" src="${common.resourcePath}/js/combined.min.js"></script>
<#-- <script type="text/javascript" src="${common.resourcePath}/js/jquery.cookie.js"></script>
<script type="text/javascript" src="${common.resourcePath}/js/modernizr.js"></script>
<script type="text/javascript" src="${common.resourcePath}/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${common.resourcePath}/js/owl.carousel.min.js"></script> -->
<script type="text/javascript" src="${common.resourcePath}/js/index.js"></script>
<script type="text/javascript">
    if ($("body").hasClass("pageProductDetails")) {
        document.write("<scr" + "ipt type='text/javascript' src='${common.resourcePath}/js/lib/jquery.galleriffic.js'></scr" + "ipt>");
        document.write("<scr" + "ipt type='text/javascript' src='${common.resourcePath}/js/lib/jquery.zooma-1.3.2.min.js'></scr" + "ipt>");
    }
</script>
</body>
</html>