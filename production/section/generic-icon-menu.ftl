<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<div class="iconMenu">
    <div class="block">
        <div class="corner1">
            <div class="corner2">
                <div class="lineTop"></div>
            </div>
        </div>
        <div class="cont">
            <ul>
                <#if section.pages.account??>
                            <#if section.pages.cart?? || section.pages.map??>
                    <li class="account">
                        <a href="${section.pages.account.link}"><span>${section.pages.account.name}</span></a>
                    </li>
                    <#else>
                    <li class="account last">
                        <a href="${section.pages.account.link}"><span>${section.pages.account.name}</span></a>
                    </li>
                    </#if>
                </#if>
                <#if section.pages.cart??>
                            <#if section.pages.map??>
                    <li class="basket">
                        <a href="${section.pages.cart.link}"><span>${section.pages.cart.name}</span></a>
                    </li>
                    <#else>
                    <li class="basket last">
                        <a href="${section.pages.cart.link}"><span>${section.pages.cart.name}</span></a>
                    </li>
                    </#if>
                </#if>
                <#if section.pages.map??>
                <li class="last map"><a href="${section.pages.map.link}"><span>${section.pages.map.name}</span></a>
                </li>
                </#if>
            </ul>
        </div>
        <div class="corner4">
            <div class="corner3">
                <div class="lineBtm"></div>
            </div>
        </div>
    </div>
    <div class="nofloat"></div>
</div>