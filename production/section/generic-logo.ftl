<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<div id="logoSection">
    <div class="block">
        <div class="corner1">
            <div class="corner2">
                <div class="lineTop"><h3 class="skip"><@js.message "logo_logo"/></h3></div>
            </div>
        </div>
        <div class="cont">
            ${section.html}
        </div>
        <div class="corner4">
            <div class="corner3">
                <div class="lineBtm"></div>
            </div>
        </div>
    </div>
    <div class="nofloat"></div>
</div>