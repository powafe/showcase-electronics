<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<link rel="stylesheet" href="${templatePath}/styles/styles.css" type="text/css" />
	<script type="text/javascript">
 	
 	<#if section.googleAnalyticsEnabled>
    	var _gaq = _gaq || [];
	  	_gaq.push(['_setAccount', '${section.googleAnalyticsAccount}']);
		_gaq.push(['_setDomainName', 'none']);
		_gaq.push(['_setAllowLinker', true]);
		_gaq.push(['_trackPageview']);
		(function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();
		
		
    	function redirectToTargetUrl()
    	{
    		<#if section.useGoogleAnalyticsLinker>		
    			 _gaq.push(function() {
    				var pageTracker = _gat._getTrackerByName(); // Gets the default tracker.
    				var linkerUrl = pageTracker._getLinkerUrl("${section.targetUrl}");
    				window.location.replace(linkerUrl);
  				});
    		<#else>
    			window.location.replace("${section.targetUrl}");
    		</#if>
    	}
    	
   	<#else>
   		function redirectToTargetUrl()
    	{
    		window.location.replace("${section.targetUrl}");
    	}
    </#if>
    
	</script>
</head>
<body onload="redirectToTargetUrl()">
	<div id="div3DSPaymentProcessingProgress">
	    <div class="hidden">
	    </div>
	    <div class="progressBar">
	        <table width="100%" border="0">
	        <tr><td>
	            <div class="width400">
	                <img src="${templatePath}/images/ajax-loader.gif" alt="" />
	                <span class="text">
	                    <br /><@js.message "pleaseWait"/><br /><br />
	                </span>
	            </div></td></tr>
	    </table>
	    </div>
	</div>	
</body>
</html>
