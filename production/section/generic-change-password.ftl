<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
 <#include "/${common.templatePath}password-policy-popup.ftl"/>
<div class="accountSection">
<div class="form">
    <div class="block">
        <div class="corner1">
            <div class="corner2">
                <div class="lineTop"></div>
            </div>
        </div>
        <div class="cont">
            <span id="change_password_error">
                <#if section.error??>
                <p class="error">
                   ${section.error}
                </p>
                </#if>
            </span>

            <form action="${section.submitLink}" method="POST" onsubmit="return validateForm(this, Validators.CustomerChangePassword, document.getElementById('change_password_error'));">
                <div class="currentPassword">
                    <label class="requiredParameter"><@js.message "change-password_currentPassword"/></label><input class="textInput" value="" name="currentPassword" type="password"/>
                </div>
                <div class="newPassword">
                    <label class="requiredParameter"><@js.message "change-password_newPassword"/></label><input class="textInput" value="" name="password" type="password"/>
                </div>
                <div class="confirmNewPassword">
                    <label class="requiredParameter"><@js.message "change-password_confirmNewPassword"/></label><input class="textInput" value="" name="confirmPassword" type="password"/>
                </div>
                <input type="hidden" name="<@csrf.name/>" value="<@csrf.value/>"/>
                <div class="nofloat"></div>
                    <input class="middleBtn rightBtn" value="<@js.message "change-password_change"/>" type="submit"/>
                <div class="nofloat"></div>
            </form>
        </div>
        <div class="corner4">
            <div class="corner3">
                <div class="lineBtm"></div>
            </div>
        </div>
    </div>
</div>
</div>