<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<div class="textMenu">
    <div class="block">
        <div class="corner1">
            <div class="corner2">
                <div class="lineTop"><h3 class="skip"><@js.message "text-menu_textMenu"/></h3></div>
            </div>
        </div>
        <#assign i = 0/>
        <#assign pageCount = section.pages?size/>
        <#assign className = ""/>
        <#if (pageCount != 0)>
        <ul>

            <#list section.pages as page>
                <#if i ==  pageCount - 1>
                    <#assign className="last"/>
                </#if>
                <li class="${className}"><a href="${page.link}">${page.name}</a></li>
                <#assign i = i + 1 />
            </#list>
        </ul>
        </#if>
        <div class="corner4">
            <div class="corner3">
                <div class="lineBtm"></div>
            </div>
        </div>
    </div>
    <div class="nofloat"></div>
</div>