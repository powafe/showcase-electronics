<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<#include "/${common.templatePath}cart-js.ftl"/>
<#if section.items??>
<div class="quickCartSection">
    <div class="row">
        <div class="col-ls-5 col-xs-12 quick-cart-header">
            <ul>
               <#--  <li class="title"><@js.message "quick-cart_quickCart"/></li> -->
                <li class="items"><img src="${common.resourcePath}/images/basket.png" class="icon-shopping-basket" alt="Shopping basket">&nbsp;&nbsp;&nbsp;&nbsp;<@js.message "cart_items"/>: &nbsp;<span>${section.itemsQuantity}</span>&nbsp;&nbsp;&nbsp;&nbsp;|</li>
                <li class="total"><@js.message "subtotal"/>: &nbsp;<span>${section.totalsBlock.subtotal}</span>&nbsp;&nbsp;&nbsp;&nbsp;|</li>
                <li><a href="/cart" title=""><@js.message "cart_checkout"/></a>

                    <#-- <div class="buttons">
                        <form id="qcActionForm" action="" method="POST">
                            <input name="shippingMethodId" type="hidden" value="${section.selectedShippingId}"/>
                            <input name="paymentMethodId" type="hidden"/>
                            <input type="hidden" name="<@csrf.name/>" value="<@csrf.value/>"/>
                            <input class="middleBtn leftBtn" type="submit" value="<@js.message "cart_viewCart"/>"
                                   onclick="cartAction('${section.cartLink}', null, 'qcActionForm')"/>
                            <input class="middleBtn rightBtn" type="submit" value="<@js.message "cart_checkout"/>"
                                   onclick="cartAction('${section.checkoutLink}', null, 'qcActionForm')"/>
                            <div class="nofloat"></div>
                        </form>
                    </div> -->
                </li>
            </ul>
        </div>
    </div>
</div>
<#else>
<div class="quickCartSection empty">
    <div class="row">
        <div class="col-lg-4 col-xs-12 quick-cart-header">
            <ul>
                <#-- <li class="title"><@js.message "quick-cart_quickCart"/></li> -->
                <li class="items"><img src="${common.resourcePath}/images/basket.png" class="icon-shopping-basket" alt="Shopping basket">&nbsp;&nbsp;&nbsp;&nbsp;<@js.message "cart_items"/>: &nbsp;<span>0</span>&nbsp;&nbsp;&nbsp;&nbsp;|</li>
                <li class="total"><@js.message "subtotal"/>: &nbsp;<span>0.00</span></li>
            </ul>
        </div>
    </div>
</div>
</#if>