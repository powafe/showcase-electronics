<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<div id="PoweredBy">
	<div class="section section-footer section-powered-by">
		<div class="container">
			<div class="row">
				<div class="col-sm-4 col-xs-12 content content-pci">
				    <div class="security">
				        <div class="pci">
				            <a onclick="window.open('/page/PCI+Compliant?referrer=${section.domain}', '', 'toolbar=no, status=no, scrollbars=no, resizable=no, width=565, height=665')">
				                <img src="${common.resourcePath}/images/pciLogo.png" title="PCI Compliant" alt="PCI Compliant"/>
				            </a>
				        </div>
				    </div>
				</div>
				<div class="col-sm-4 col-xs-12 content content-copyright">
				    <div class="copyright text-center">
				 		<p>&copy;2014 PowaTag Showcase Demo Site</p>
					</div>
				</div>
				<div class="col-sm-4 col-xs-12 content content-company">
				    <div class="company text-right">
			            <img src="${common.resourcePath}/images/LocaytaLogo.png" title="<@js.message "searchTechnologyProvided-byLocayta"/>" alt="<@js.message "searchTechnologyProvided-byLocayta"/>"/>
			            <a href="${section.outerLink}" target="_blank">
			                <img src="${common.resourcePath}/images/poweredBy.png" title="<@js.message "powered-by_powaWebsiteAndShoppingCartSolutions"/>" alt="<@js.message "powered-by_powaWebsiteAndShoppingCartSolutions"/>"/>
			            </a>
				    </div>
				</div>
			</div>	    
		</div>		    
	</div>
</div>