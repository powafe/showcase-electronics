<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<div class="searchSection">
    <div class="block col-lg-4 col-xs-12 search-section">
        <form action="${section.searchLink}" onsubmit="return validateForm(this, Validators.SearchProducts, document.getElementById('serch_error'));">
            <input class="textInput" name="${section.searchParamName}" value="" placeholder="Search Showcase Electronics..."/>
            <button type="submit"><i class="fa fa-search"></i></button> 
        </form>
    </div>
</div>