<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->

<#include "product-list.lib.ftl"/>

<#if section.products??>
<div class="productListTblSection">
    <div class="block">
        <div class="corner1">
            <div class="corner2">
                <div class="lineTop"><h2></h2></div>
            </div>
        </div>
        <div class="cont">
                <span id="product_error">
                    <#if section.error??>
                        <p class="error">
                        ${section.error}
                        </p>
                    </#if>
                </span>
            <table class="base" width="100%">
                <thead>
                <tr>
                    <th class="first" style="width:100px;">&nbsp;</th>
										<th class="name alignLeft"><@js.message "productName"/></th>
                    <#if section.products?first.displaySKU>
                        <th class="alignLeft sku"><@js.message "sku"/></th>
                    </#if>
                    <#if section.showPrice>
                        <th class="alignRight"><@js.message "price"/></th>
                    </#if>
                    <th class="last"></th>
                </tr>
                </thead>
                <tbody class="cont">
                    <#assign i = 1/>
                    <#list section.products as product>
                        <#if (i % 2 == 0)>
                            <#assign className = "even"/>
                        <#else>
                            <#assign className = ""/>
                        </#if>

                    <tr class="${className}">
											<td class="img">
												<span><a href="${product.link}"><img alt="${product.name} - ${product.SKU}" title="${product.name} - ${product.SKU}" src="${product.imageLink}"/></a></span>
												<#-- Do not change the order of these two img tags as the order of the img elements matters -->
												<#if section.showAlternativeImage && product.imageAlternativeLink??>
														<span class="skip altImage"><a href="${product.link}"><img alt="${product.name} - ${product.SKU}" title="${product.name} - ${product.SKU}" src="${product.imageAlternativeLink}"/></a></span>
												</#if>
											</td>
											
											
                        <td class="name"><a href="${product.link}">${product.name}</a></td>

                        <#if product.displaySKU>
                            <td class="sku">${product.SKU}</td>
                        </#if>

                        <#if section.showPrice>
                            <td class="price">
                                <#if product.promoted>
                                    <div class="priceNow">
                                        <span>${product.bestPrice}</span>
                                    </div>
                                    <div class="priceWas">
                                        <span>${product.oldPrice}</span>
                                    </div>
                                <#else>
                                    <div class="priceNow">
                                        <span>${product.price}</span>
                                    </div>
                                </#if>
                            </td>
                        </#if>
                        <td class="last">
                            <#if product.canBeBought>
                                <@buyButton product>
                                    <#if product.buyButtonEnabled>
                                        <input class="smallBtn" type="submit" value="<@js.message "buy"/>"/>
                                    <#else>
                                        <input class="smallBtn opacity" disabled="true" type="submit" value="<@js.message "buy"/>"/>
                                    </#if>
                                </@buyButton>
                            </#if>
                        </td>
                    </tr>

                        <#assign i = i+1/>
                    </#list>
                </tbody>
            </table>
        </div>
        <div class="corner4">
            <div class="corner3">
                <div class="lineBtm"></div>
            </div>
        </div>
    </div>
    <div class="nofloat"></div>
</div>
<#else>
    <#if section.search>
    <span id="no_products_found">
            <p class="error">
                <#if section.error??>
                    ${section.error}
                <#else>
                    <@js.message "product-list_noProductsFound"/>
                </#if>
            </p>
        </span>
    </#if>
</#if>