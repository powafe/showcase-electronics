<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->

<#include "generic-product-details.lib.ftl" />

<#-- Large Image section -->
<#if section.product.largeImageLink??>
    <div id="divLargeImageTable" style="display: none" onclick="closeLargeImage()">
        <div class="hidden"></div>
        <div class="largeImage">
            <iframe></iframe>
            <table id="idLargeImage" class="imgBlock">
                <tr>
                    <td class="image-modal">
                        <a class="close" onclick="closeLargeImage()"><i class="fa fa-times"></i></a>

                        <div class="nofloat"></div>
                        <#--Thumbnails table starts -->
                        <#--Do not show thumbs if there are only one image-->
                        <#if (section.product.catalogueImages?size > 1)>
                            <#assign blockDisplay = "block" />
                        <#else>
                            <#assign blockDisplay = "none" />
                        </#if>
                        <div style="display:${blockDisplay}">
                            <table id="thumbsLarge" class="thbLine" style="margin: 0 auto;">
                                <tr class="thumbs">
                                    <#list section.product.catalogueImages as theImage>
                                        <td>
                                            <a class="thumb" href="${theImage.enlargedLink}" title="${section.product.name} - ${section.product.SKU}">
                                                <img src="${theImage.thumbnailLink}"
                                                     class="cursor-pointer img-responsive" width="${thumbnailSize}"
                                                     alt="${section.product.name} - ${section.product.SKU}"/>
                                            </a>
                                        </td>
                                    </#list>
                                </tr>
                            </table>
                        </div>
                        <#--Thumbnails table ends -->
                        <table class="slideshowLargeTable">
                            <tr>
                                <td>
                                    <div id="slideshowLarge"></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</#if>
<#-- End large Image section -->

<div class="productDetailsSection section section-product-details">
    <div class="block">

        <div class="cont">
            <span id="product_error">
                <#if section.error??>
                    <p class="error">
                        ${section.error}
                    </p>
                <#elseif section.product.errorMessage??>
                    <p class="error">
                        ${section.product.errorMessage}
                    </p>
                </#if>
            </span>

            <div class="col-sm-5 col-xs-12 component component-product-images">
                <h1 class="productName visible-xs">${section.product.name}</h1>
                 <#assign hasImages=((section.product.catalogueImages??)&&(section.product.catalogueImages?size > 0))/>
                <div>
                    <div id="zoomBlock" style="display:none; height: ${imageSize}px;">
                        <div class="zoomLoading" style="display:none;">
                            <table width="100%" border="0" style="margin-top:100px;">
                                <tr>
                                    <td>
                                        <div style="width:120px;margin:auto;">
                                            <span class="text"><br/><@js.message "product-details_loadingImage"/><br/></span><img src="${common.resourcePath}/images/ajax-loader.gif" alt=""/>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

                <table width="100%" class="imgBlock">
                    <#if hasImages>
                        <tr>
                            <td class="fullSizeImg">
                                <div id="slideshow"></div>
                            </td>
                        </tr>
                        <tr>
                            <td id="caption"></td>
                        </tr>
                        <tr>
                            <td>
                                <#--Thumbnails table starts -->
                                <#--Do not show thumbs if there are only one image-->
                                <#if (section.product.catalogueImages?size > 1)>
                                    <#assign blockDisplay = "block" />
                                <#else>
                                    <#assign blockDisplay = "none" />
                                </#if>
                                <div style="display:${blockDisplay}">
                                    <table id="thumbs" class="thbLine col-xs-12" style="margin:auto;">
                                        <tr class="thumbs noscript">
                                            <#list section.product.catalogueImages as theImage>
                                                <#--Click listener to handle click not only on image, but on table cell too-->
                                                <td>
                                                    <a class="thumb" href="${theImage.fullSizeLink}" title="${section.product.name} - ${section.product.SKU}">
                                                        <img src="${theImage.thumbnailLink}"
                                                             alt="${section.product.name} - ${section.product.SKU}"
                                                             class="cursor-pointer img-responsive" width="${thumbnailSize}"/>
                                                    </a>

                                                    <#-- ############################# ZOOM IMAGE DETAILS NOTE  ############################## -->
                                                    <#-- To use zoom we should have a div with id like zoomImageHolder1, zoomImageHolder2, etc
                                                         with 'a' element inside that has href to large image URL  -->
                                                    <#-- ############################# ZOOM DETAILS NOTE END ################################# -->
                                                    <div id="zoomImageHolder${theImage_index}" style="display:none;">
                                                        <a href="${theImage.enlargedLink}"></a>
                                                    </div>

                                                    <#-- <div class="caption">
                                                        <#if section.product.largeImageLink??>
                                                            <a onclick="showLargeImage2(${theImage_index})"
                                                               href="javascript://" class="linkLargeImage">${section.product.largeImageLinkText!''}</a>
                                                        </#if>
                                                        <#if section.product.zoomImageLinkText??>
                                                            <a onclick="showZoomedImage(${theImage_index})"
                                                               href="javascript://"
                                                               class="linkZoomImage linkZoom">${section.product.zoomImageLinkText!''}</a>
                                                        </#if>
                                                    </div> -->
                                                </td>
                                            </#list>
                                        </tr>
                                    </table>
                                </div>
                                <#--Thumbnails table ends -->
                            </td>
                        </tr>
                    </#if>
                    <#--No Image Section-->
                    <#if !hasImages>
                        <tr>
                            <td>
                                <img alt="${section.product.name} - ${section.product.SKU}"
                                     title="${section.product.name} - ${section.product.SKU}"
                                     src="${section.product.imageLink}"/>
                            </td>
                        </tr>
                    </#if>
                </table>
            </div>

            <div class="col-sm-7 col-xs-12 component component-product-short-description">
                <h1 class="productName hidden-xs">${section.product.name}</h1>
                 <@optForm>
                    
                    <div class="productBlocks">
                        <div class="col-xs-7 col-sm-8 productPriceBlock">
                            <div class="product-actions">
                                <span class="total">
                                    <#if section.product.promoted>
                                        <span id="was_price" class="priceWas">${section.product.oldPrice}</span>
                                    </#if>
                                    <h5 id="product_price">Price: <span class="price-amount">${section.product.price}</span></h5>
                                </span>
                                <#if section.options??>
                                    <div class="productOptionsBlock">
                                        <#list section.options as opt>
                                            <#if opt.type == 'C'>
                                                <@comboBox option = opt pid = section.product.pid updateLink = section.product.updateProductPriceLink/>
                                            <#elseif opt.type == 'T'>
                                                <@textField option = opt pid = section.product.pid />
                                            <#elseif opt.type == 'H'>
                                                <@checkBox option = opt pid = section.product.pid updateLink = section.product.updateProductPriceLink />
                                            </#if>
                                        </#list>
                                    </div>
                                </#if>
                                <#if section.product.canBeBought>
                                    <span class="qty">
                                        <h5><@js.message "quantity"/>:&nbsp;</h5>
                                        <input name="${section.product.pid}.qty" class="textInput" type="text" value="1" maxlength="7"/>
                                        <#if section.product.buyButtonEnabled>
                                        <button id="buy_button" class="smallBtn button-green" type="submit"><img src="${common.resourcePath}/images/basket.png" class="icon-shopping-basket">ADD TO BASKET</button>
                                        <#else>
                                        <button id="buy_button" class="smallBtn opacity" disabled="true" type="submit"><img src="${common.resourcePath}/images/basket.png" class="icon-shopping-basket">ADD TO BASKET</button>
                                        </#if>
                                    </span>
                                </#if>
                            </div>
                        </div>
                    </@optForm>
                    <div class="col-xs-5 col-sm-4 powatag-block">
                        <!-- Container for regular PowaTag -->
                        <div class="powaTagWrapper">
                            <div id="powatag">
                                <div id="powatag0-powatag" class="powatag-powatag">
                                    <div id="powatag0-overlay"></div>
                                    <div id="powatag0-tagline-line1-overlay" class="powatag-tagline powatag-label">Processing securely for:</div>
                                    <div id="powatag0-tagline-line2-overlay" class="powatag-tagline powatag-message"></div>
                                </div>
                            </div>
                            <!-- Link to explain what PowaTag is --> 
                            <a id="powatagPopuplink"  rel="powaTagPopup" href="#powaTagPopup">What's this?</a> 
                            <!-- End Explanation --> 
                        </div>
                        <!-- End PowaTag Container -->
                    </div> 
                     
                    <!-- Container for mouseover overlay -->
                    <div id="powaTagZoom">
                        <div class="powaTagPopupWrapper">
                            <div class="powaTagContent"></div>
                        </div>
                    </div>
                    <!-- End popup container --> 
                     
                    <!-- Container for popup showing larger tag, video, and link to App Store -->
                    <div id="powaTagPopup">
                        <a class="powaTagClose">X</a>
                        <div class="powaTagPopupOverlay" id="powaTagPopupOverlay"></div>
                            <div class="powaTagPopupWrapper">
                                <div class="powaTagContent">
                                    <div class="powaTagLeft">
                                    <h3>WHAT IS POWATAG?</h3>
                                    <p>PowaTag is an easy to use free app that allows you to quickly and securely make a purchase using your phone.</p>
                                    <p>It is not an 'e-wallet' because the app never holds a balance - it sends orders directly to a merchant who has signed up to use the PowaTag service.</p>
                                    <p>If there's a PowaTag you can buy that item, whether it's on the internet, a printed ad or in audio - without having to go through the normal checkout experience.</p>
                                    <p>
                                        <iframe width="400" height="225" src="http://www.youtube.com/embed/p9VhmPWPzQE?rel=0" frameborder="0" allowfullscreen></iframe>
                                    </p>
                                    <a class="powatag-center powatag-download-link-ios" href="http://itunes.apple.com/gb/app/powatag/id667504703?ls=1&mt=8" target="_blank">Download from the App Store</a>
                                    <a class="powatag-center powatag-download-link-android " href="https://play.google.com/store/apps/details?id=com.powatag.android.apps.powatag" target="_blank">Download from Google Play</a></div>
                                    <div class="powaTagRight">
                                        <h3 class="powatag-center">SCAN THE TAG BELOW TO PAY</h3>
                                        <p class="powatag-center">using the PowaTag app on your mobile device</p>
                                        <div class="powaTagWrapper-large">
                                            <div id="powatag0-powatag-large" class="powatag-powatag-large">
                                            <div id="powatag0-overlay-large"></div>
                                            <div id="powatag0-tagline-line1-overlay-large" class="powatag-tagline powatag-label">Processing securely for:</div>
                                            <div id="powatag0-tagline-line2-overlay-large" class="powatag-tagline powatag-message"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Large Overlay container -->
                     <script type="text/javascript">
                     $(document).ready(function() {
                        generatePowaTag('https://live.powatag.com/generator/powatag', '0861a52f-680e-4680-bf98-9959400bd995', '${section.product.SKU}');
                    });   
                    </script>
                </div>
                <div class="col-xs-12 short-description-block">
                    <#if section.product.displayStockInfo>
                        <div class="qty">
                            <p>${section.product.quantityValue}  available in stock.</p>
                        </div>
                    </#if>
                    <div class="text"><span class="short-description">${section.product.description}</span>..&nbsp;<a href="#fullDetails">read more</a></div>
                </div>

                <div class="component component-social-share">
                    <img class="img-responsive" src="/file/ItEixpG/66b3248d-d718-45cf-aa67-3afc8495557e.jpg">
                </div>
            </div>

            <div id="fullDetails" class="col-xs-12 product-tabs nav top-nav">
                <ul class="nav-set top-nav-set">
                    <li class="tab-1 active nav-item top-nav-item">
                        <a href="#" title="" data-instant>Overview</a>
                    </li>
                     <li class="tab-2 nav-item top-nav-item">
                     <a href="#" title="" data-instant>Spec<span class="hidden-xs">ification</span>s</a>
                    </li>
                     <li class="tab-3 nav-item top-nav-item">
                        <a href="#" title="" data-instant>Reviews</a>
                    </li>
                </ul>
            </div>

</div></div></div></div></div></div><!--//container-->
            <div class="box-hasline"></div>
<div class="container">
    <div class="row">
           
            <div class="col-xs-12 component component-product-full-description">
                <div class="descBlock">
                   <#--  <h1 class="productName">${section.product.name}</h1> -->
                   <#--  <#if section.product.displaySKU>
                        <div class="sku">
                            <label><@js.message "sku"/>: </label><b>${section.product.SKU}</b>
                        </div>
                    </#if> -->


                    <#-- <h2 class="brandName">${section.product.brand}</h2> -->
                    <div class="text">${section.product.description}</div>
                    <div class="productAttributes">
                        <#list section.product.productAttributes as productAttribute>
                            <#if productAttribute.display>
                                <div class="productAttribute">
                                    <label>${productAttribute.name}: </label><b>${productAttribute.value}</b>
                                </div>
                            </#if>
                        </#list>
                    </div>
                </div>
            </div>
           

            <div class="nofloat"></div>
            <div id="linked_products">
                <#assign linkedProducts=section.linkedProducts>
                <#include "/${common.templatePath}linked-products-block.ftl"/>
            </div>
        </div>

        <div class="corner4">
            <div class="corner3">
                <div class="lineBtm"></div>
            </div>
        </div>
    </div>
    <div class="nofloat"></div>
</div>
<script type="text/javascript">
// Initially set opacity on thumbs and add
// additional styling for hover effect on thumbs
var galleryLarge;
var gallery;
var onMouseOutOpacity = 0.67;

var globalZoom;

<#if section.product.largeImageLink??>
    var enlargeModeEnabled = true;
<#else>
    var enlargeModeEnabled = false;
</#if>

<#if section.product.zoomImageLinkText??>
    var zoomModeEnabled = true;
<#else>
    var zoomModeEnabled = false;
</#if>

$('.short-description').text(function(index, currentText) {
    return currentText.substr(0, 300);
});

jQuery('#thumbs tr.thumbs td').css('opacity', onMouseOutOpacity)
        .hover(
        function ()
        {
            jQuery(this).not('.selected').fadeTo('fast', 1.0);
        },
        function ()
        {
            jQuery(this).not('.selected').fadeTo('fast', onMouseOutOpacity);
        }
        );

jQuery(document).ready(function()
{

    gallery = jQuery('#gallery').galleriffic('#thumbs', {
        delay:                  2000,
        numThumbs:              5,
        preloadAhead:           1,
        enableTopPager:         false,
        enableBottomPager:      false,
        imageContainerSel:      '#slideshow',
        controlsContainerSel:   '',
        captionContainerSel:    '#caption',
        loadingContainerSel:    '',
        renderSSControls:       false,
        renderNavControls:      false,
        playLinkText:           '',
        pauseLinkText:          '',
        prevLinkText:           '',
        nextLinkText:           '',
        nextPageLinkText:       '',
        prevPageLinkText:       '',
        enableHistory:          false,
        autoStart:              false,
        onChange:               function(prevIndex, nextIndex)
        {
            jQuery('#thumbs tr.thumbs').children().eq(prevIndex).fadeTo('fast', onMouseOutOpacity).end().eq(nextIndex).fadeTo('fast', 1.0);
        },
        onTransitionOut:        function(callback)
        {
            jQuery('#caption').fadeTo('fast', 1.0);
            jQuery('#slideshow').fadeTo('fast', 1.0, callback);
        },
        onTransitionIn:         function()
        {
            jQuery('#slideshow').fadeTo('fast', 1.0);
            jQuery('#caption').fadeTo('fast', 1.0);
            switchImageCallback();
        },
        onPageTransitionOut:    function(callback)
        {
            jQuery('#thumbs tr.thumbs').fadeTo('fast', 1.0, callback);
        },
        onPageTransitionIn:     function()
        {
            jQuery('#thumbs tr.thumbs').fadeTo('fast', 1.0);
        },
        onImageClick:            function()
        {
            handleProductDetailsImageClick();
        },
        imageContainerClass:    'cursor-pointer'
    });
    galleryLarge = jQuery('#gallery').galleriffic('#thumbsLarge', {
        delay:                  2000,
        numThumbs:              5,
        preloadAhead:           1,
        enableTopPager:         false,
        enableBottomPager:      false,
        imageContainerSel:      '#slideshowLarge',
        controlsContainerSel:   '',
        captionContainerSel:    '',
        loadingContainerSel:    '',
        renderSSControls:       false,
        renderNavControls:      false,
        playLinkText:           '',
        pauseLinkText:          '',
        prevLinkText:           '',
        nextLinkText:           '',
        nextPageLinkText:       '',
        prevPageLinkText:       '',
        enableHistory:          false,
        autoStart:              false,
        onChange:               function(prevIndex, nextIndex)
        {
            jQuery('#thumbsLarge tr.thumbs').children()
                    .eq(prevIndex).fadeTo('fast', onMouseOutOpacity).end()
                    .eq(nextIndex).fadeTo('fast', 1.0);
        },
        onTransitionOut:        function(callback)
        {
            jQuery('#slideshowLarge').fadeTo('fast', 1.0, callback);
        },
        onTransitionIn:         function()
        {
            jQuery('#slideshowLarge').fadeTo('fast', 1.0);
        },
        onPageTransitionOut:    function(callback)
        {
            jQuery('#thumbsLarge tr.thumbs').fadeTo('fast', 1.0, callback);
        },
        onPageTransitionIn:     function()
        {
            jQuery('#thumbsLarge tr.thumbs').fadeTo('fast', 1.0);
        },
        onImageClick:            function()
        {
            return false;
        },
        imageContainerClass:    'cursor-pointer'
    });

    $('#slideshowLarge').find('img').addClass('img-responsive');

    $(document).ready(tagResize);
    $(window).on("resize", tagResize);
    function tagResize() {
        var powatagWrap = $('.powaTagWrapper').height(),
            productBlocks = $('.productBlocks').height(),
            powatagBlockPadding = ((productBlocks - powatagWrap) / 2);
        $('.powatag-block').css({
            "padding-top": powatagBlockPadding + "px",
            "padding-bottom": powatagBlockPadding + "px"
        });
    }

    $('.product-tabs li').click(function (e) {
        e.preventDefault();
        var navClass = $(this).attr("class").split(" ")[0],
            contentText = $(".tabs li");
        $(this).siblings().removeClass('active');
        $(this).addClass('active');
        contentText.removeClass('active');
        $('.tabs li.' + navClass).addClass('active').fadeIn("slow");
    });

});
</script>