<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->

<span id="address_result">
    <#if section.result??>
        <p class="warning">
            ${section.result}
        </p>
    </#if>
</span>

<#if section.addressBook?has_content>
<table class="base alignLeft">
    <tr>
        <td>Address</td>
    </tr>
    <#list section.addressBook as address>
        <tr>
            <td>${address.getAddressAsSingleLine()} </td>
        </tr>
    </#list>
</table>
</#if>

<br/>
<br/>

<a href="#" onclick="$('#addressEntry').show()">Add a non-school address manually</a>

<div id="addressEntry" hidden="true">
    <span id="register_error">
    <#if section.error??>
        <p class="error">
        ${section.error}
        </p>
    </#if>
    </span>
    <form method="POST" action="${section.submitLink}" onsubmit="submitNewAddress(this); return false;">
        <div class="addressDetails">
            <h1>ADDRESS SELECTION PLACEHOLDER</h1>
            <div class="title">
                <label class="requiredParameter"><@js.message "title"/></label>
                <input class="textInput" type="text" name="title" value="${(section.customerDetails.title)!}"/>
            </div>    
            <div class="firstName">
                <label class="requiredParameter"><@js.message "firstName"/></label>
                <input class="textInput" type="text" name="firstName" value="${(section.customerDetails.firstName)!}"/>
            </div>
            <div class="lastName">
                <label class="requiredParameter"><@js.message "lastName"/></label>
                <input class="textInput" type="text" name="lastName" value="${(section.customerDetails.lastName)!}"/>
            </div>
            <div class="addressLine1">
                <label class="requiredParameter"><@js.message "addressLine"/> 1</label>
                <input class="textInput" type="text" name="addressLine1"/>
            </div>
            <div class="addressLine2">
                <label><@js.message "addressLine"/> 2</label>
                <input class="textInput" type="text" name="addressLine2"/>
            </div>
            <div class="townCity">
                <label class="requiredParameter"><@js.message "billing-shipping-details_townCity"/></label>
                <input class="textInput" type="text" name="town"/>
            </div>
            <div class="countyState" id="states">
                <#include "/${common.templatePath}states-block.ftl"/>
            </div>
            <div class="postcode">
                <label class="requiredParameter"><@js.message "postcode"/></label>
                <input class="textInput" type="text" name="postcode"/>
            </div>
            <div class="country">
                <label class="requiredParameter"><@js.message "country"/></label>
                <select name="countryCode" onchange="countryChange(this);">
                <#list section.countries as country>
                    <option value="${country.code}" <#if country.code == section.defaultCountryCode>selected="true"</#if> >${country.name}</option>
                </#list>
                </select>
            </div>
        </div>
        <input type="submit" value="Submit"/>
        <input type="button" value="Cancel" onclick="$('#addressEntry').hide()"/>
    </form>
</div>
