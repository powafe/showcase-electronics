<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<div class="enquirySection">
    <div class="form">
        <div class="block">
            <div class="corner1">
                <div class="corner2">
                    <div class="lineTop"></div>
                </div>
            </div>
            <div class="cont">
                <span id="enquiry_error">
                <#if section.error??>
                    <p class="error">
                    ${section.error}
                    </p>
                </#if>
                </span>

                <script type="text/javascript"><!--
                function formClear(curForm)
                {
                    // iterate over fields and set empty value to them:
                    $(':input', curForm)
                            .not(':button, :submit, :reset, :hidden')
                            .val('');
                    document.getElementById('confImg').src="${section.confImgLinkEmptyQuery}"+'?tm='+new Date().getTime();
                    document.getElementById('enquiry_error').innerHTML = '';
                };
                // -->  </script>

                <form action="${section.submitLink}" method="post" onsubmit="return validateForm(this, Validators.Enquiry, document.getElementById('enquiry_error'));">
                    <div class="title"><label><@js.message "title"/></label><input class="textInput" <#if  section.title??>value="${section.title}" </#if> id="title" name="title" type="text"/></div>
                    <div class="name"><label class="requiredParameter"><@js.message "name"/></label><input class="textInput" <#if  section.name??>value="${section.name}" </#if> id="name" name="name" type="text"/></div>
                    <div class="phone"><label><@js.message "phone"/></label><input class="textInput" <#if  section.phone??>value="${section.phone}" </#if> id="phone" name="phone" type="text"/></div>
                    <div class="email"><label class="requiredParameter"><@js.message "email"/></label><input class="textInput" <#if  section.email??>value="${section.email}" </#if> id="email" name="email" type="text"/></div>
                    <div class="note"><label><@js.message "enquiry-form_note"/></label><textarea class="textInput" id="note" name="note" cols="36" rows="10"><#if  section.note??>${section.note}</#if></textarea></div>
                    <div class="nofloat"></div>
                    <input type="hidden" name="<@csrf.name/>" value="<@csrf.value/>"/>
                    <input class="middleBtn rightBtn" value="<@js.message "enquiry-form_submit"/>" type="submit"/>
                    <input class="middleBtn leftBtn" value="<@js.message "enquiry-form_reset"/>" type="button" onclick="formClear(this.form)"/>

                </form>
            </div>
            <div class="corner4">
                <div class="corner3">
                    <div class="lineBtm"></div>
                </div>
            </div>
        </div>
        <div class="nofloat"></div>
    </div>
</div>