<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<#if (section.pages??) && (section.pages?size > 0)>
    <div class="breadcrumbSection">
        <ul>
            <#assign pageCount = section.pages?size/>
            <#assign i = 0/>
            <#assign className = "first"/>
            <#list section.pages as page>
                <#if (i == pageCount - 1)>
                    <#assign className = "last" />
                    <#else/>
                    <#if (i != 0)>
                        <#assign className = "" />
                    </#if>
                </#if>
                <li class="${className}">
                    <#if i != pageCount - 1 || section.includeLast>
                        <a href="${page.link}">${page.name}</a>
                        <#else/>
                        ${page.name}
                    </#if>
                </li>
                <#assign i = i + 1 />
            </#list>
        </ul>
    </div>


    <#assign last = section.pages?last/>
    <#assign pageCount = last.children?size/>
    <#if pageCount != 0 && !section.includeLast>
    <div class="categoryLinkListSection">
        <div class="cont">
            <#assign i = 1/>
            <#assign className = ""/>
            <#list last.children as page>
                <#if (i % template.categoryPerRow == 0)>
                    <#assign className = "last" />
                    <#else/>
                    <#assign className = "" />
                </#if>

                <h4 class="${className}"><a href="${page.link}">${page.name}</a></h4>
                <#if (i % template.categoryPerRow == 0)>
                    <div class="nofloat"></div>
                </#if>
                <#assign i = i + 1 />
            </#list>
        </div>
    </#if>

</#if>