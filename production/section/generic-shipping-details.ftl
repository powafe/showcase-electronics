<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<div class="shippingDetailsSection">
<div class="form">
    <div class="block">
        <div class="corner1"><div class="corner2"><div class="lineTop"></div></div></div>
        <div class="cont">
            <span id="enquiry_error">
                <#if section.error??>
                <p class="error">
                   ${section.error}
                </p>
                </#if>
            </span>
            <form action="${section.submitLink}" method="POST" onsubmit="return validateForm(this, Validators.CustomerInfo, document.getElementById('enquiry_error'));">
                <div class="title">
                    <label><@js.message "title"/></label>
                    <input class="textInput" <#if  section.title??>value="${section.title}" </#if> type="text" name="title" />
                </div>
                <div class="firstName">
                    <label class="requiredParameter"><@js.message "firstName"/></label>
                    <input class="textInput" <#if  section.firstName??>value="${section.firstName}" </#if> type="text" name="firstName" />
                </div>
                <div class="lastName">
                    <label class="requiredParameter"><@js.message "lastName"/></label>
                    <input class="textInput" <#if  section.lastName??>value="${section.lastName}" </#if> type="text" name="lastName" />
                </div>
                <div class="company">
                    <label><@js.message "company"/></label>
                    <input class="textInput" <#if  section.company??>value="${section.company}" </#if> type="text" name="company" />
                </div>
                <div class="addressLine1">
                    <label class="requiredParameter"><@js.message "addressLine"/> 1</label>
                    <input class="textInput" <#if  section.address1??>value="${section.address1}" </#if> type="text" name="address1" />
                </div>
                <div class="addressLine2">
                    <label><@js.message "addressLine"/> 2</label>
                    <input class="textInput" <#if  section.address2??>value="${section.address2}" </#if> type="text" name="address2" />
                </div>
                <div class="townCity">
                    <label class="requiredParameter"><@js.message "billing-shipping-details_townCity"/></label>
                    <input class="textInput" <#if  section.town??>value="${section.town}" </#if> type="text" name="town"/>
                </div>
                <div class="countyState" id="states">
                    <#include "/${common.templatePath}states-block.ftl"/>
                </div>
                <div class="postcode">
                    <label class="requiredParameter"><@js.message "postcode"/></label>
                    <input class="textInput" <#if  section.postcode??>value="${section.postcode}" </#if> type="text" name="postcode" />
                </div>
                <div class="country">
                    <label class="requiredParameter"><@js.message "country"/></label>
                    <select name="countryCode" onchange="countryChange(this);">
                        <option value=""><@js.message "pleaseSelect"/></option>
                        <#if section.countryList??>
                            <#list section.countryList as country>
                                <#if section.countryCode == country.code>
                                    <option selected="selected" value="${country.code}">${country.name}</option>
                                <#else>
                                    <option value="${country.code}">${country.name}</option>
                                </#if>
                            </#list>
                        </#if>
                    </select>
                </div>
                <div class="email">
                    <label class="requiredParameter"><@js.message "email"/></label>
                    <input class="textInput" <#if  section.email??>value="${section.email}" </#if> type="text" name="email" />
                </div>
                <div class="phone">
                    <label class="requiredParameter"><@js.message "phone"/></label>
                    <input class="textInput" <#if  section.phone??>value="${section.phone}" </#if> type="text" name="phone" />
                </div>
                <input type="hidden" name="prevLink" value="${section.currentLink}"/>
                <input type="hidden" name="<@csrf.name/>" value="<@csrf.value/>"/>
                <input class="middleBtn rightBtn" type="submit" value="Continue"/>
            </form>
             <form action="${section.backLink}" enctype="text/plain">
             <input class="middleBtn leftBtn" type="submit" value="Back" />
             </form>
    <div class="nofloat"></div>
        </div>
        <div class="corner4"><div class="corner3"><div class="lineBtm"></div></div></div>
    </div>
    <div class="nofloat"></div>
</div>
</div>
