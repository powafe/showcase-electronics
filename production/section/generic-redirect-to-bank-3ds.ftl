<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html><head>
<link rel="stylesheet" href="${templatePath}/styles/styles.css" type="text/css" />
</head>
<body onload="javascript: document.paypalForm.submit();">
<form name="paypalForm" action="${url}" method="POST">
    <input type="hidden" name="PaReq" value="${payload}"/>
    <input type="hidden" name="TermUrl" value="${returnUrl}"/>
    <input type="hidden" name="MD" value="${orderUniqueToken}"/>
</form>

<div id="div3DSPaymentProcessingProgress">
    <div class="hidden">
    </div>
    <div class="progressBar">
        <table width="100%" border="0">
        <tr><td>
            <div class="width400">
                <span class="text">
                <br /><@js.message "redirect-to-bank-3ds_message"/><br/>
                </span>
                <img src="${templatePath}/images/ajax-loader.gif" alt="" />
                <span class="text">
                    <br /><@js.message "pleaseWait"/><br /><br />
                </span>
            </div></td></tr>
    </table>
    </div>
</div>

</body>
</html>
