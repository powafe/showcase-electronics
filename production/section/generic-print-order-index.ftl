<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<#macro renderSections sections>
    <#list sections as section>
        <#if section??>
            ${section}
        </#if>
    </#list>
</#macro>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <title><@js.message "print-order_printInvoice"/></title>
        <style type="text/css">
            table {
                border-collapse: collapse;
                margin: 10px 0;
                width: 800px;
                border: 1px solid #000;
                }
            table th {
                font-weight: normal;
                border: 1px solid #000;
            }
            table td {
                vertical-align: top;
                white-space: nowrap;
                padding: 2px 5px;
            }
            table .optionLine1 td {
                border: 1px solid #000;
                border-bottom: none;
            }
            table .optionLine2 td {
                border: 1px solid #000;
                border-top: none;
            }
            table td.name {
                white-space: normal;
            }
            table.total {
                margin-top: 0;
                width: 280px;
                border: 1px solid #000;
                }
            table.total td {
                white-space: normal;
                border: none;
                }
            table.total td.red {
                vertical-align: bottom;
                white-space: nowrap;
            }
            .note { width: 500px;
                border: 1px solid #000;
                padding: 5px;
                }
            label {
                float: left;
                clear: both;
                text-align: right;
                padding: 0 10px;
                line-height: 1.3em;
                font-weight: bold;
            }

            .value {
                float: right;
                padding: 0;
                text-align: right;
                line-height: 1.3em;
            }

            .height10 {
                clear: both;
                height: 10px;
            }

            .nofloat {
                clear: both;
                font-size: 0;
            }

            .float-left {
                float: left;
            }

            .float-right {
                float: right;
            }

            .order, .horLine {
                clear: both;
                border-top: 1px dashed #000;
                padding-top: 15px;
                width: 800px;
            }
            .red {
                color: #fe0202;
            }
            .delivery .details {
                width: 600px;
                font-size: 150%;
            }
            .delivery .value {
                text-align: left;
                margin-bottom: 20px;
            }
            @media print {
                .delivery {
                page-break-before: auto;
                page-break-inside:avoid;
                overflow: hidden;
                            }
            }

        </style>
    </head>
    <#if page.bodyClass??>
    <body class="${page.bodyClass}">
    <#else>
    <body>
    </#if>
        <#if page.centerContainer??>
            <@renderSections sections = page.centerContainer/>
        </#if>
    </body>