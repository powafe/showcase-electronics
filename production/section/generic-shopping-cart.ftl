<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<#include "/${common.templatePath}cart-js.ftl"/>
<#if section.items??>
<div class="col-md-9 col-xs-12 shoppingCartSection section section-shopping-cart">
    <span id="shoppingcart_error">
        <#if section.promoCodeData??>
            <#if section.promoCodeData.error??>
                <p class="error">
                ${section.promoCodeData.error}
                </p>
            </#if>
        </#if>
        <#if section.error??>
            <p class="error">
            ${section.error}
            </p>
        </#if>
    </span>
    <div class="component component-checkout-buttons clearfix">
        <a class="button button-dark" href="/index.html" title="">CONTINUE SHOPPING &gt;</a>
        <button class="button button-green" type="submit">CHECKOUT ></button>
    </div>
    <#-- Shopping Cart Details -->
    <div class="wrapperShoppingCartDet component component-shopping-cart-details">
        <form id="actionForm" method="POST" action="${section.updateLink}">
            <input name="shippingMethodId" type="hidden"/>
            <input name="paymentMethodId" type="hidden"/>
            <input type="hidden" name="<@csrf.name/>" value="<@csrf.value/>"/>
            <div class="shoppingCartDet">
                <table class="base">
                    <thead>
                    <tr>
                        <th><@js.message "productName"/></th>
                        <th>Qty</th>
                        <th><@js.message "price"/></th>
                        <th class="hidden-xs"><@js.message "subtotal"/></th>
                        <th class="del hidden-xs">Remove</th>
                    </tr>
                    </thead>

                    <tbody>
                        <#assign i = 1/>
                        <#assign items = section.items/>
                        <#list items as item>
                            <#assign isEven = (i % 2 == 0)/>
                            <#if isEven>
                                <#assign trclass = "even"/>
                                <#else/>
                                    <#assign trclass = ""/>
                            </#if>
                            <#if item.options??>
                                <#assign rowspan = item.options?size + 1>
                            <tr class="${trclass}">
                              <#--   <td class="productImage"><a href="${item.link}"><img alt="${item.name} - ${item.SKU}" title="${item.name} - ${item.SKU}" src="${item.imageLink}"/></a></td> -->
                                <td class="name">
                                    <a href="${item.link}">
                                        <img class="image-thumb" alt="${item.name} - ${item.SKU}" title="${item.name} - ${item.SKU}" src="${item.imageLink}"/>
                                        ${item.name}
                                        <#list item.options as option>
                                            <p class="${trclass} nameOption">
                                                <span class="nameOption">${option.name}: <b>${option.value}</b></span>
                                                <span class="price">${option.price}</span>
                                            </p>
                                        </#list>
                                    </a>
                                    <button class="dlt visible-xs" type="button" onclick="dlt(${item.id})">Remove</button>
                                </td>
                                <td><input class="qty-input" name="${item.id}.qty"value="${item.quantity}" type="text" maxlength="7"/>
                                <td class="price">${item.price}</td>
                                    <input name="pid" type="hidden" value="${item.id}"/>
                                </td>
                                <td class="price last hidden-xs">${item.subtotal}</td>
                                <td  class="del hidden-xs">
                                    <button class="dlt" type="button" onclick="dlt(${item.id})"><i class="fa fa-trash-o"></i></button>
                                </td>
                            </tr>
                                
                                <#else/>
                                <tr class="${trclass}">
                                    <td class="name">
                                        <a href="${item.link}"><img class="image-thumb" alt="${item.name} - ${item.SKU}" title="${item.name} - ${item.SKU}" src="${item.imageLink}"/>${item.name}</a>
                                        <button class="dlt visible-xs" type="button" onclick="dlt(${item.id})">Remove</button>
                                    </td>
                                    <td><input class="qty-input" name="${item.id}.qty" value="${item.quantity}"type="text" maxlength="7"></td>
                                    <td class="price">${item.price}</td>
                                        <input name="pid" type="hidden" value="${item.id}"/>
                                    <td class="price hidden-xs">${item.subtotal}</td>
                                    <td class="del hidden-xs">
                                        <button class="dlt" type="button" onclick="dlt(${item.id})"><i class="fa fa-trash-o"></i></button>
                                    </td>
                                </tr>
                            </#if>
                            <#assign i = i + 1/>
                        </#list>
                    </tbody>
                </table>
            </div>
        </form>
    </div>
    <#-- End Shopping Cart Details -->

    <#-- PROMOTION FEATURE -->
    <div class="component component-checkout-buttons clearfix">
        <#if section.hasPromocode>
            <div class="promoCode">
                <form id="promocode" method="POST" action="${section.applyPromotionLink}"
                      onsubmit="return validateForm(this, Validators.PromoCode, document.getElementById('shoppingcart_error'));">
                    <#if section.promoCodeData??>
                        <#assign promoCodeValue = section.promoCodeData.code>
                        <#else>
                            <#assign promoCodeValue = "">
                    </#if>
                    <input type="hidden" name="<@csrf.name/>" value="<@csrf.value/>"/>
                    <span>
                        <label>Promo Code:</label>
                        <input name="promoCode" class="textInput" type="text" value="${promoCodeValue}"/>
                        <input class="smallBtn" type="submit" value="<@js.message "shopping-cart_apply"/>"/>
                    </span>
                </form>
            </div>
        </#if>

        <input class="button button-dark pull-right" value="<@js.message "shopping-cart_updateCart"/>" type="submit"
               onclick="cartAction('${section.updateLink}', null, 'actionForm')"/>
    </div>
    <#-- END PROMOTION FEATURE -->

    <#-- Shipping Method Block -->
    <#if section.shippingMethods??>
        <div class="wrapperShippingMethodBlock component component-shopping-shipping-methods">
            <div class="shippingMethodBlock">
                <h4><@js.message "shippingMethod"/></h4>
                <div class="shipping-methods-list">
                <#list section.shippingMethods as method>
                    <#if section.selectedShippingId == method.id>
                        <input id="shipping${method.id}" checked="true" class="radio" value="${method.id}" name="shippingMethod"
                                   type="radio"
                                   onclick="updateShippingMethod('${common.requestHost}${section.updateShippingURL}', '${method.id}')"/><label class="text" for="shipping${method.id}">${method.name}</label><br/>
                    <#else/>
                        <input id="shipping${method.id}" class="radio" value="${method.id}" name="shippingMethod" type="radio"
                                   onclick="updateShippingMethod('${common.requestHost}${section.updateShippingURL}', '${method.id}')"/><label class="text" for="shipping${method.id}">${method.name}</label><br/>
                    </#if>
                </#list>
                </div>
            </div>
        </div>
    </#if>
    <#-- End Shipping Method Block -->

    <#-- Totals Block -->
    <div class="wrapperTotals component component-shopping-totals">
        <div id="totals" class="totals">
            <#assign totals = section.totalsBlock/>
            <#include "/${common.templatePath}totals-block.ftl"/>
        </div>
    </div>
    <#-- End Totals Block -->
    <div class="component component-checkout-buttons clearfix">
        <a class="button button-dark" href="/index.html" title="">CONTINUE SHOPPING &gt;</a>
        <#-- <input class="button button-green pull-right" value="<@js.message "cart_checkout"/>" type="submit" onclick="cartAction('${section.checkoutLink}', null, 'actionForm')"> -->
        <button class="button button-green" type="submit">CHECKOUT ></button>
    </div>

    <#if section.expressPayEnabled>
        <#list section.expressMethods as method>
            <div class="sep">--- <@js.message "shopping-cart_orUse"/> ---</div>
            <div class="nofloat"></div>
            <div class="checkoutBtn">
                <input src="${method.imageLink}" alt="<@js.message "quick-cart_fastCheckout"/>" type="image"
                       onclick="cartAction('${section.expressLink}', ${method.methodId}, 'actionForm')"/>
            </div>
            <div class="nofloat"></div>
        </#list>
    </#if>
</div>

<#else/>
<div class="shoppingCartSection">
    <div class="col-md-12 component component-system-message">
        <div class="system-message">
        <#if section.error??>
            <#-- <span id="shoppingcart_error">
                <p class="error">
                ${section.error}
                </p>
            </span> -->
            <h1>YOUR BASKET IS EMPTY</h1>
            <p>Please search for alternative products to add to your basket.</p>
            <a href="/index.html"><button class="button button-green" type="submit">CONTINUE SHOPPING &gt;</button></a>
        <#else/>
            <#-- <p><@js.messageArgs "shopping-cart_yourIsEmpty" , ["${section.title}"] /></p> -->
            <h1>YOUR BASKET IS EMPTY</h1>
            <p>Please search for alternative products to add to your basket.</p>
            <a href="/index.html"><button class="button button-green" type="submit">CONTINUE SHOPPING &gt;</button></a>
        </#if>
        </div>
    </div>
</div>
<script type="text/javascript">
$('.leftContainer').hide();
$('.page-title').hide();
$('#middleContainer').addClass('background-full background-system-message');
$(document).ready(function(){
    $('.cart-side-menu').hide();
});
</script>
</#if>

