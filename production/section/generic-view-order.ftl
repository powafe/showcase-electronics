<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<div class="orderDetailsSection">
    <div class="block">
        <div class="corner1"><div class="corner2">
            <div class="lineTop">
            </div>
        </div>
     </div>
        <div class="cont">
<#-- Order Details -->
        <div class="print">
            <#if section.printOrderLink??>
                <a target="_blank" href="${section.printOrderLink}" class="link">Print Invoice</a>
            </#if>
        </div>
            <#if section.orderData??>
<div class="wrapperOrderDet">
    <div class="orderDet">
            <table class="base alignLeft">
                <thead>
                    <tr>
                        <#if section.orderData.creditNote>
                            <th class="first widthfix"><@js.message "view-order_creditNoteDetails"/></th>
                        <#else/>
                            <th class="first widthfix"><@js.message "orderDetails"/></th>
                        </#if>
                        <th class="last"/>
                    </tr>
                </thead>
                <tbody>
                    <tr class="orderNo">
                        <td class="name"><span><@js.message "order_no"/></span></td>
                        <td class="last"><span><#if section.orderData.id??>${section.orderData.id}</#if></span></td>
                    </tr>
                    <tr class="orderDate even">
                        <td class="name"><span><@js.message "order_date"/></span></td>
                        <td class="last"><span>${section.orderData.orderDateObj?string('yyyy-MMM-dd HH:mm')}</span></td>
                    </tr>
                </tbody>
            </table>
    </div>
</div>
            </#if>
<#-- End Order Details -->
<#-- Billing Details -->
            <#if (section.billingDetails?? && section.billingDetails.hasInfo)>
            <#assign bilDet = section.billingDetails.info>
<div class="wrapperBillingDet">
    <div class="billingDet">
            <table class="base alignLeft">
                <thead>
                    <tr>
                        <th class="first widthfix"><@js.message "billingDetails"/></th>
                        <th class="last"></th>
                    </tr>
                </thead>
                <tbody>
        <#assign i = 1/>
                <#list bilDet as line>
                        <#assign className = "${line.name?replace(' ', '')}"/>
                        <#assign className = "${className?replace('/', '')}"/>
                        <#assign className = "${className?uncap_first}"/>
                        <#assign isEven = (i % 2 == 0)/>
                        <#if isEven>
                            <tr class="${className} even">
                        <#else/>
                            <tr class="${className}">
                        </#if>
                        <td class="name"><span>${line.name}</span></td>
                        <td class="last"><span>${line.value}</span></td>
                    </tr>
                <#assign i = i + 1/>
                </#list>
                 </tbody>
            </table>
    </div>
</div>
            </#if>
<#-- End Billing Details -->
<#-- Shipping Details -->
            <#if (section.shippingDetails?? && section.shippingDetails.hasInfo)>
            <#assign shipDet = section.shippingDetails.info>
<div class="wrapperShippingDet">
    <div class="shippingDet">
            <table class="base alignLeft">
                <thead>
                    <tr>
                        <th class="first widthfix"><@js.message "shippingDetails"/></th>
                        <th class="last"></th>
                    </tr>
                </thead>
                <tbody>
             <#assign i = 1/>
                <#list shipDet as line>
                        <#assign className = "${line.name?replace(' ', '')}"/>
                        <#assign className = "${className?replace('/', '')}"/>
                        <#assign className = "${className?uncap_first}"/>
                        <#assign isEven = (i % 2 == 0)/>
                        <#if isEven>
                            <tr class="${className} even">
                        <#else/>
                            <tr class="${className}">
                        </#if>
                        <td class="name"><span>${line.name}</span></td>
                        <td class="last"><span>${line.value}</span></td>
                    </tr>
                <#assign i = i + 1/>
                </#list>
                 </tbody>
            </table>
    </div>
</div>
            </#if>
<#-- End Shipping Details -->
<#-- Shopping Cart Details -->
<div class="wrapperShoppingCartDet">
    <div class="shoppingCartDet">
            <table class="base">
                <thead>
                    <tr>
                        <th class="first"><@js.message "productName"/></th>
                        <#if section.displaySKU>
                            <th class="alignLeft"><@js.message "sku"/></th>
                        </#if>
                        <th class="alignRight"><@js.message "price"/></th>
                        <th class="alignRight"><@js.message "quantity"/></th>
                        <th class="alignRight last"><@js.message "subtotal"/></th>
                    </tr>
                </thead>
                <tbody>
                <#if section.itemList??>
        <#assign i = 1/>
                        <#list section.itemList as item>
            <#assign isEven = (i % 2 == 0)/>
                        <#if isEven>
                            <#assign trclass = "even"/>
                        <#else/>
                            <#assign trclass = ""/>
                        </#if>
                            <#if item.options??>
                            <#assign rowspan = item.options?size + 1>
                            <tr class="${trclass}">
                                <td class="name">
                                <#if item.link??>
                                    <a href="${item.link}">${item.name}</a>
                                <#else/>
                                    ${item.name}
                                </#if></td>
                                <#if section.displaySKU>
                                    <td class="sku" rowspan="${rowspan}"><input type="hidden" value="0"/>
                                        <#if item.SKU??>${item.SKU}</#if>
                                    </td>
                                </#if>
                                <td class="price">${item.price}</td>
                                <td class="qty" rowspan="${rowspan}">${item.quantity}</td>
                                <td class="price last" rowspan="${rowspan}">${item.subtotal}</td>
                            </tr>
                                <#list item.options as option>
                                    <tr class="nameOption">
                                        <td class="nameOption">${option.name} <b>${option.value}</b></td>
                                        <td class="price">${option.price}</td>
                                    </tr>
                                </#list>
                            <#else>
                            <tr class="${trclass}">
                                    <td class="name"><a href="${item.link}">${item.name}</a></td>
                                    <#if section.displaySKU>
                                        <td class="sku"><input type="hidden" value="0"/>${item.SKU}</td>
                                    </#if>
                                    <td class="price">${item.price}</td>
                                    <td class="qty">${item.quantity}</td>
                                    <td class="price last">${item.subtotal}</td>
                                </tr>
                            </#if>
            <#assign i = i + 1/>
                        </#list>
                </#if>
                </tbody>
            </table>
    </div>
</div>
<#-- End Shopping Cart Details -->
<#-- Totals Block -->
            <#if section.shoppingCartData??>
<div class="wrapperTotals">
    <div class="totals">
            <table class="totalBlock">
                <tbody>
                    <tr class="productSubtotal">
                        <td><span><@js.message "productSubtotal"/></span></td>
                        <td class="price last"><span>${section.shoppingCartData.subtotal}</span></td>
                    </tr>
                    <tr class="shippingTotal">
                        <td><span><@js.message "shipping"/></span></td>
                        <td class="price last"><span>${section.shoppingCartData.shippingTotal}</span></td>
                    </tr>

                    <#if section.shoppingCartData.discount??>
                        <tr class="discount">
                            <td><span>${section.shoppingCartData.discount.name}</span></td>
                            <td class="price last"><span>- ${section.shoppingCartData.discount.price}</span></td>
                        </tr>
                    </#if>

                       <#if section.shoppingCartData.oldTaxList??>
                                <#list section.shoppingCartData.oldTaxList as tax>
                                    <#assign className = "${tax.name?replace(' ', '')}"/>
                                    <#assign className = "${className?uncap_first}"/>
                                    <tr class="${className}">
                                        <td><span>${tax.name}</span></td>
                                        <td class="price last"><span>${tax.cost}</span></td>
                                    </tr>
                                </#list>
                                <tr class="total">
                                    <td><span><@js.message "total"/></span></td>
                                    <td class="price last"><#if section.shoppingCartData.total??><span>${section.shoppingCartData.total}</span></#if></td>
                                </tr>
                            <#else>
                                <#if section.shoppingCartData.taxIncluded>
                                     <#if section.shoppingCartData.flatTaxList??>
                                     <#list section.shoppingCartData.flatTaxList as tax>
                                         <#assign className = "${tax.name?replace(' ', '')}"/>
                                         <#assign className = "${className?uncap_first}"/>
                                         <tr class="${className}">

                                            <td><span>${tax.name}</span></td>
                                            <td class="price last"><span>${tax.cost}</span></td>
                                        </tr>
                                     </#list>
                                     </#if>
                                     <tr class="total">
                                        <td><span><@js.message "total"/></span></td>
                                        <td class="price last"><#if section.shoppingCartData.total??><span>${section.shoppingCartData.total}</span></#if></td>
                                     </tr>
                                <#else>
                                    <#if section.shoppingCartData.percTaxList??>
                                    <#list section.shoppingCartData.percTaxList as tax>
                                        <#assign className = "${tax.name?replace(' ', '')}"/>
                                        <#assign className = "${className?uncap_first}"/>
                                        <tr class="${className}">

                                            <td><span>${tax.name}</span></td>
                                            <td class="price last"><span>${tax.cost}</span></td>
                                        </tr>
                                    </#list>
                                    </#if>
                                    <#if section.shoppingCartData.flatTaxList??>
                                    <#list section.shoppingCartData.flatTaxList as tax>
                                        <#assign className = "${tax.name?replace(' ', '')}"/>
                                        <#assign className = "${className?uncap_first}"/>
                                        <tr class="${className}">

                                            <td><span>${tax.name}</span></td>
                                            <td class="price last"><span>${tax.cost}</span></td>
                                        </tr>
                                    </#list>
                                    </#if>
                                    <tr class="total">
                                        <td><span><@js.message "total"/></span></td>
                                        <td class="price last"><#if section.shoppingCartData.total??><span>${section.shoppingCartData.total}</span></#if></td>
                                    </tr>
                                </#if>
                       </#if>
                </tbody>
            </table>
    </div>
</div>
            </#if>
<#-- End Totals Block -->
            <div class="nofloat"></div>
        </div>
        <div class="corner4">
            <div class="corner3">
                <div class="lineBtm"></div>
            </div>
        </div>
     </div>
</div>