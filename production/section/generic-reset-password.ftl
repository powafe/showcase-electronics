<#--
 * Copyright (c) 2007-2009 Venda Small Business Services Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Venda Small Business Services Limited.
 -->
<div class="accountSection">
<div class="form">
    <div class="block">
        <div class="corner1">
            <div class="corner2">
                <div class="lineTop"></div>
            </div>
        </div>
        <div class="cont">
            <span id="change_password_error">
                <#if section.error??>
                <p class="error">
                   ${section.error}
                </p>
                </#if>
            </span>
            <form action="${section.submitLink}?q=${common.resetPasswordHash}" method="POST" onsubmit="return validateForm(this, Validators.CustomerChangePassword, document.getElementById('change_password_error'));">
                <label class="requiredParameter">New Password</label><input class="textInput" value="" name="password" type="password"/>
                <br/><label class="requiredParameter">Confirm New Password</label><input class="textInput" value="" name="confirmPassword" type="password"/>
                <div class="nofloat"></div>
                    <input class="middleBtn rightBtn" value="Change" type="submit"/>
                <div class="nofloat"></div>
            </form>
        </div>
        <div class="corner4">
            <div class="corner3">
                <div class="lineBtm"></div>
            </div>
        </div>
    </div>
</div>
</div>
