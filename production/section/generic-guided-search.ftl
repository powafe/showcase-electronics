<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->

<script type="text/javascript"> <!--
function filter(baseUrl, filterName, displayName, form)
{
    var value = form.input.value;
    var isValid = validateInputFilterAndShowError(value, displayName);
    if (isValid && (baseUrl != '#'))
    {
        window.location = baseUrl + "&filter=" + encodeURIComponent(filterName + ":" + value);
    }
    return false;
};

function validateInputFilterAndShowError(value, displayName)
{
    var isValid = true;
    var message;
    if(value == '')
    {
        isValid = false;
        message = getMessageByKey(Validators.LocaytaInputFilter.ERROR_MESSAGES.REQUIRED.input, [displayName]);
    }
    if(isValid && value.length > Validators.LocaytaInputFilter.MAX_LENGTH_FIELDS.input)
    {
        isValid = false;
        message = getMessageByKey(Validators.LocaytaInputFilter.ERROR_MESSAGES.MAX_LENGTH.input, new Array(displayName, Validators.LocaytaInputFilter.MAX_LENGTH_FIELDS.input));
    }
    if(isValid && !value.match(Validators.LocaytaInputFilter.REGEXP_FIELDS.input))
    {
        isValid = false;
        message = getMessageByKey(Validators.LocaytaInputFilter.ERROR_MESSAGES.INVALID.input, [displayName]);
    }
    if (! isValid)
    {
        document.getElementById('serch_error').innerHTML = '<p class="error">' + message + '</p>';
    }
    return isValid;
};

function moreLessList(list, button, lessCount, showMore, showLess)
{
    function hideItems()
    {
        $('#'+list+' li:gt('+(lessCount-1)+')').hide()
        $('#'+button).attr("value",showMore);
    };
    function showItems()
    {
        $('#'+list+' li:gt('+(lessCount-1)+')').show()
        $('#'+button).attr("value",showLess);
    };
    hideItems();
    var hidden = true;
    function buttonOnClick(event)
    {
        if (hidden)
        {
            showItems();
            hidden = false;
        }
        else
        {
            hideItems();
            hidden = true;
        }
    };
    $('#'+button).click(buttonOnClick)
}
//--></script>
<#if section.filters??>
<#assign filtersCount = section.filters?size/>
</#if>
<div class="panel panel-default">
    <div class="guidedSearchSection section-refine-search">
        <#if (filtersCount > 0) >
            <div class="appliedFiltersSection visible-lg">
                <div class="block floatLeft">
                    <div class="component-head component-head-searched-for">
                        <h3><@js.message "pagesection.content.default.guided.search.title"/></h3>
                        <div class="component component-search-filter">
                            <ul>
                                <#assign i = 0/>
                                <#assign className = ""/>
                                <#list section.filters as filter>
                                    <#if (i == filtersCount - 1)>
                                        <#assign className = "last" />
                                    </#if>
                                    <li class="${className}">
                                        
                                            <#if (filtersCount > 1) && filter.removeLink?? >
                                            <form action="${filter.removeLink}" method="post">
                                                <button type="submit" class="dlt"></button>
                                            </form>
                                            </#if>
                                            <h4 class="filterName">${filter.name}</h4>
                                        <a class="nolink">
                                            <#if filter.value.numeric>
                                                <#if filter.money>
                                                    <span class="filterValue">${section.currencySymbol} ${filter.value.from} - ${section.currencySymbol} ${filter.value.to}</span>
                                                <#elseif filter.weight>
                                                    <span class="filterValue">${filter.value.from}<@js.message "guided-search_weightUnit"/> - ${filter.value.to}<@js.message "guided-search_weightUnit"/></span>
                                                <#else>
                                                    <span class="filterValue">${filter.value.from}-${filter.value.to}</span>
                                                </#if>
                                            <#else>
                                                <i class="fa fa-times-circle-o"></i><span class="filterValue">${filter.value.stringValue}</span>
                                            </#if>
                                        </a>
                                    </li>
                                    <#assign i = i + 1 />
                                </#list>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </#if>
        <#assign listNumber = 0 />
        <#list section.facets as facet>
            <#assign facetClass><#if (facet.type == "BASIC")>filtersLinkListSection<#elseif (facet.type == "PICTOGRAM")>filtersThbListSection<#elseif (facet.type == "INPUT")>filterTextBoxSection<#elseif (facet.type == "SLIDER")>filterSliderSection</#if></#assign>
            <div class="${facetClass} component component-facet-class">
                <div class="block floatLeft">
                    <div class="panel-heading hidden-lg">
                        <h4 class="panel-title"><a data-toggle="collapse" class="accordion-toggle" data-parent="#left-accordion" href="#collapseLeftThree">REFINE BY</a></h4>
                     </div>
                     <div id="collapseLeftThree" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <h4><#if facet.displayName?? && facet.displayName != "">${facet.displayName}<#else>&nbsp;</#if></h4>
                            <div class="cont">
                                <#if (facet.type == "BASIC")>
                                    <ul id="facet-list-${listNumber}"> 
                                        <#assign i = 0/>
                                        <#assign className = ""/>
                                        <#assign optionCount = facet.options?size/>
                                        <#list facet.options as option>
                                            <#if (i == optionCount - 1)>
                                                <#assign className = "last" />
                                            </#if>
                                            <li class="${className}">
                                                <a <#if option.filterLink??> href="${option.filterLink}"<#else> class="nolink"</#if>>
                                                    <span class="facetOption">
                                                    <#if facet.allowMultypleSelection >
                                                        <input type="checkbox" class="checkbox" onclick="this.blur();" <#if option.filterLink??>onchange="window.location.href='${option.filterLink}'"<#else>onchange="this.checked=true;"</#if> <#if option.checked>checked</#if>/>
                                                    </#if>
                                                    <span class="optionData">
                                                    <#if option.represent.numeric>
                                                        <#if facet.money>
                                                            <span class="optionValue">${section.currencySymbol} ${option.represent.from} - ${section.currencySymbol} ${option.represent.to}</span>
                                                        <#elseif facet.weight>
                                                            <span class="optionValue">${option.represent.from}<@js.message "guided-search_weightUnit"/> - ${option.represent.to}<@js.message "guided-search_weightUnit"/></span>
                                                        <#else>
                                                            <span class="optionValue">${option.represent.from}-${option.represent.to}</span>
                                                        </#if>
                                                    <#elseif option.represent.category>
                                                        <span class="optionValue">${option.represent.stringValue}</span>
                                                    <#else>
                                                        <span class="optionValue">${option.represent.stringValue}</span>
                                                    </#if>
                                                    </span></span>
                                                </a>
                                            </li>
                                            <#assign i = i + 1 />
                                        </#list>
                                    </ul>
                                    <#if (i > facet.displayOptionCount)>
                                            <div class="buttonLine">
                                                <input class="middleBtn" type="submit" id="facet-more-button-${listNumber}"></input>
                                            </div>
                                            <script type="text/javascript">moreLessList('facet-list-${listNumber}', 'facet-more-button-${listNumber}', ${facet.displayOptionCount}, '${facet.showMoreLabel}', '${facet.showLessLabel}')</script>
                                        </#if>
                                        <#assign listNumber = listNumber + 1 />
                                <#elseif (facet.type == "PICTOGRAM")>
                                        <#list facet.options as option>
                                            <a <#if option.filterLink??> href="${option.filterLink}"<#else> class="nolink"</#if>><span class="pictogram"><span class="optionImg<#if option.checked && !facet.checkedOptionImage??> checked</#if>"><img src="${option.represent.stringValue}" width="${section.pictogramWidth}px" alt="${option.represent.hint}" title="${option.represent.hint}"/></span>
                                                        <#if facet.checkedOptionImage?? && option.checked><span class="checkedImg"><img src="${facet.checkedOptionImage}" width="${section.pictogramWidth}px" alt="${option.represent.hint}" title="${option.represent.hint}"/></span></#if></span><!--<span class="optionCount">(${option.count})</span>--></a>
                                        </#list>
                                <#elseif (facet.type == "INPUT")>
                                    <form onsubmit="return filter('${facet.baseFilerLink}', '${facet.locaytaName}', '${facet.displayName}', this);">
                                        <input type="text" name="input" class="textInput"/>
                                        <input type="submit" value="Go" class="goBtn">
                                    </form>
                                </#if>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </#list>
    </div>
</div>