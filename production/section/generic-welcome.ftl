<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<div class="accountSection">
<div class="form logout">
          <div class="block">
               <div class="corner1"><div class="corner2"><div class="lineTop"></div></div></div>
               <div class="cont">
                <div class="center">
                <p>Welcome, ${section.userName}</p>
                <form action="${section.logoutLink}" enctype="text/plain">
                <input class="middleBtn" type="submit" value="<@js.message "welcome_logOut"/>" />
                </form>
                    <div class="nofloat"></div>
                </div>
               </div>
               <div class="corner4"><div class="corner3"><div class="lineBtm"></div></div></div>
          </div>
</div>
</div>