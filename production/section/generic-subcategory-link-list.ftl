<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<#if (section.pages??) && (section.pages?size > 0)>
    <#assign last = section.pages?last/>
    <#assign pageCount = last.children?size/>
    <#if pageCount != 0 && !section.includeLast>
    <div class="categoryLinkListSection">
        <div class="block">
            <div class="corner1">
                <div class="corner2">
                    <div class="lineTop"></div>
                </div>
            </div>
            <div class="cont">

                <#assign i = 1/>
                <#assign className = ""/>
                <#list last.children as page>
                    <#if (i % template.categoryPerRow == 0)>
                        <#assign className = "last" />
                        <#else/>
                        <#assign className = "" />
                    </#if>

                    <h4 class="${className}"><a href="${page.link}">${page.name}</a></h4>
                    <#if (i % template.categoryPerRow == 0)>
                        <div class="nofloat"></div>
                    </#if>
                    <#assign i = i + 1 />
                </#list>
            <div class="nofloat"></div>
            </div>
            <div class="corner4">
                <div class="corner3">
                    <div class="lineBtm"></div>
                </div>
            </div>
        </div>
        <div class="nofloat"></div>
    </div>
    </#if>

</#if>