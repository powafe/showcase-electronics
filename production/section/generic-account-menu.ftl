<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<div class="accountMenu">
    <div class="block floatLeft">
        <div class="corner1">
            <div class="corner2">
                <div class="lineTop"><h3><@js.message "account-menu_myAccount"/></h3></div>
            </div>
        </div>
        <div class="cont">
            <ul>
                <#if section.loggedIn>
                    <li class="accountHome"><a href="${section.accountHomeLink}"><@js.message "account-menu_home"/></a></li>
                    <li class="viewOrder"><a href="${section.accountViewOrdersLink}"><@js.message "account-menu_viewOrders"/></a></li>
                    <#if section.showChangeCreditCardDetails>
                        <li class="changeCreditCardDetails"><a href="${section.accountChangeCreditCardDetailsLink}"><@js.message "account-menu_changeCreditCardDetails"/></a></li>
                    </#if>
                    <li class="myDetails"><a href="${section.accountDetailsLink}"><@js.message "account-menu_myDetails"/></a></li>
                    <#if section.context.shopData.addressBookEnabled = "YES">
                        <li class="addressBook"><a href="${section.accountAddressBookLink}"><@js.message "account-menu_addressBook"/></a></li>
                    </#if>
                    <li class="changePassword"><a href="${section.accountChangePasswordLink}"><@js.message "account-menu_changePassword"/></a>
                    </li>
                    <li class="logOut"><a href="${section.accountLogOutLink}"><@js.message "account-menu_logOut"/></a></li>
                    <#else/>
                    <li class="myDetails"><a href="${section.accountRegisterLink}"><@js.message "account-menu_registerUser"/></a>
                    </li>
                    <li class="logIn"><a href="${section.accountLoginLink}"><@js.message "account-menu_logIn"/></a></li>
                </#if>

            </ul>
        </div>
        <div class="corner4">
            <div class="corner3">
                <div class="lineBtm"></div>
            </div>
        </div>
    </div>

    <div class="nofloat"></div>

</div>