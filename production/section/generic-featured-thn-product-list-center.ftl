<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<#if section.viewSettings.viewSettings??>
    <#assign settings = section.viewSettings.viewSettings/>
</#if>
<script type="text/javascript"> <!--
function updateAndSubmit(el, formId)
{
    var formToSubmit = this.document.getElementById(formId);
    var inputs = formToSubmit.getElementsByTagName("input");
    for (var i = 0; i < inputs.length; i++)
    {
        if (inputs[i].name == el.name)
        {
            inputs[i].value = el.value;
            formToSubmit.submit();
        }
    }
}
// --></script>


<#if section.products??>
    <div class="featuredProductListThbSection">
    <div class="corner1"><div class="corner2"><div class="lineTop"></div></div></div>
    <div class="cont">
        <#assign i = 1/>
        <#list section.products as product>
            <#assign isLast = (i % template.featuredProductCountPerRow == 0)/>
            <#if isLast>
                <#assign blockclass = " n${i} last"/>
                <#else/>
                <#assign blockclass = " n${i}"/>
            </#if>
            <div class="featuredProductListThbBlock block ${blockclass}">
                    <div class="corner1">
                        <div class="corner2">
                            <div class="lineTop"><#if section.title?? && section.title != ""><h3><span>${section.title}</span></h3></#if>
                                <h4><a href="${product.link}">${product.name}</a></h4>
                            </div>
                        </div>
                    </div>
                <div class="cont">
                    <div class="imgBlock">
                        <a href="${product.link}">
                            <span><img class="thnSize" alt="${product.name} - ${product.SKU}" title="${product.name} - ${product.SKU}" src="${product.imageLink}"/></span>
                            <#--Order of img elements matters: JS-4885 -->
                            <#if section.showAlternativeImage && product.imageAlternativeLink??>
                            <span class="skip altImage"><img class="thnSize" alt="${product.name} - ${product.SKU}" title="${product.name} - ${product.SKU}" src="${product.imageAlternativeLink}"/></span>
                            </#if>
                            <span class="skip"><img class="fullSize" alt="${product.name} - ${product.SKU}" title="${product.name} - ${product.SKU}" src="${product.fullImageLink}"/></span>
                        </a>
                        <#if section.showPrice>
                            <div class="wrapperPriceBlock">
                                <div class="priceBlock">
                                    <#if product.promoted>
                                        <div class="priceNow">
                                            <span>${product.bestPrice}</span>
                                        </div>
                                        <div class="priceWas">
                                            <span>${product.oldPrice}</span>
                                        </div>
                                    <#else>
                                        <div class="priceNow">
                                            <span>${product.price}</span>
                                        </div>
                                    </#if>
                                </div>

                            </div>
                        </#if>
                    </div>
                    <div class="descBlock">
                    <a class="moreInfo" href="${product.link}">More Info</a>
                        <h4><a href="${product.link}">${product.name}</a></h4>
                        <#if product.displaySKU>
                            <div class="sku"><label>SKU: </label><b>${product.SKU}</b></div>
                        </#if>
                        <#if product.displayStockInfo>
                <div class="qty"><label>${product.quantityLabel}</label><b>${product.quantityValue}</b></div>
            </#if>
                        <#if product.displayShortDescription>
                            <div class="text">${product.description}</div>
                        </#if>
                    </div>

                    <#if product.canBeBought>
                        <div class="buyBlock">
                            <form id="buyForm" action="${product.buyLink}" method="POST">
                                <input name="pid" type="hidden" value="${product.pid}"/>
                                <input name="${product.pid}.qty" type="hidden" value="1"/>
                                <input type="hidden" name="<@csrf.name/>" value="<@csrf.value/>"/>
                                <#if product.buyButtonEnabled>
                                    <input class="smallBtn" type="submit" value="<@js.message "buy"/>"/>
                                <#else>
                                    <input class="smallBtn opacity" disabled="true" type="submit" value="<@js.message "buy"/>"/>
                                </#if>
                            </form>
                        </div>
                    </#if>
                    <div class="nofloat"></div>
                </div>

                <div class="corner4">
                    <div class="corner3">
                        <div class="lineBtm"></div>
                    </div>
                </div>

            </div>
            <#if isLast>
                <div class="nofloat rowSeparator"></div>
            </#if>
            <#assign i = i + 1/>
        </#list>
        <#--Alternative product image script JS-4885 -->
        <script type="text/javascript">
         $('div.imgBlock span:first-child').hover(
           function () {
            // if element has alt image
            if ($(this).siblings('.altImage').length != 0) {
                $(this).hide();
                $(this).siblings('.altImage').show();
            }
           },
           function () {

           }
         );

         $('div.imgBlock span.altImage').hover(
           function () {

           },
           function () {
            // if element has alt image
            if ($(this).prev().length != 0) {
                $(this).prev().show();
                $(this).hide();
            }
           }
         );
        </script>
        <div class="nofloat"></div>
        </div>
        <div class="corner4"><div class="corner3"><div class="lineBtm"></div></div></div>

    </div>
</#if>