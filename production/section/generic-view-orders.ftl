<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<div class="orderDetailsSection">
    <div class="block">
        <div class="corner1">
            <div class="corner2">
                <div class="lineTop"></div>
            </div>
        </div>
        <div class="cont">
            <p><@js.message "view-orders_clickOnTheOrder"/></p>

            <form method="post" id="periods" name="periods" action="/unitrader/shop/customer/account/view/order">
                <#--<p>View Orders<input type="hidden" name="off" value="1"/>
                    <input type="hidden" name="offop" value="33"/>
                    <select name="offv">
                        <option value="">All Orders</option>
                        <option value="6,3">Last 3 Days</option>
                        <option value="3,1">Last Week</option>
                        <option value="2,1">Last Month</option>
                        <option value="1,1">Last Year</option>
                    </select></p>-->
                <p><span><@js.message "view-orders_totalOrders"/>:</span><b>${section.totalOrdersCount}</b></p>
            </form>
            <#if section.orders??>
        <div class="wrapperOrderDet">
            <div class="orderDet">
                <table class="base">
                    <thead>
                        <tr>
                            <th class="first"><@js.message "order_date"/></th>
                            <th class="alignLeft"><@js.message "order_orderNo"/></th>
                            <th><@js.message "view-orders_viewOrder"/></th>
                            <th class="alignRight"><@js.message "total"/></th>
                            <th class="last alignLeft"><@js.message "view-orders_paymentStatus"/></th>
                        </tr>
                    </thead>
                    <tbody>
                        <#assign ordersCount = section.orders?size/>
                        <#assign i = 1/>
                        <#assign className = ""/>
                        <#list section.orders as order>
                            <#if (i % 2 == 0)>
                                <#assign className = "even"/>
                                <#else/>
                                <#assign className = ""/>
                            </#if>
                            <#if order.creditNote>
                                <#assign className = className + " credit-note"/>
                            </#if>
                            <tr class="${className}">
                                <td class="date">${order.orderDateObj?string('yyyy-MMM-dd HH:mm')}</td>
                                <td class="sku">${order.id}</td>
                                <td>
                                    <a href="${order.link}">View&nbsp;Order</a>
                                </td>
                                <td class="price">${order.total}</td>
                                <td class="last alignLeft">${order.paymentStatus}</td>
                            </tr>
                        <#assign i = i + 1/>
                        </#list>
                    </tbody>
                </table>
            </div>
        </div>
            </#if>
            <div class="nofloat"></div>
        </div>
        <div class="corner4">
            <div class="corner3">
                <div class="lineBtm"></div>
            </div>
        </div>
    </div>
    <div class="nofloat"></div>
</div>