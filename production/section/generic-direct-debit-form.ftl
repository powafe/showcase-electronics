<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<style>
    body, form {
        margin: 0px;
    }

    .paymentDetailsSection .paymentFormRow, h3 {
        float: left;
        clear: both;
        width: 650px;
        line-height: 20px;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 12px;
        color: #666;
    }

    h3 {
        font-size: 15px;
    }

    .paymentDetailsSection .paymentFormRow label {
        width: 220px;
        display: block;
        float: left;
        text-transform: uppercase;
    }

    .paymentDetailsSection .paymentFormRow input[type=text] {
        background: none;
        font-size: 15px;
        margin-bottom: 5px;
        -moz-box-shadow: inset 0 0 4px #ddd;
        -webkit-box-shadow: inset 0 0 4px #ddd;
        box-shadow: inset 0 0 4px #ddd;
        border: 1px solid #dcdcdc;
        color: #666;
        padding: 5px 5px 3px;
        width: 250px;
    }

    .paymentDetailsSection .paymentFormRow input.paymentFormSubmit {
        background: #10b4d3;
        /* IE fallback */

        background-image: url(${common.resourcePath}/images/white_arrow.png);
        /* IE fallback */

        background-image: url(${common.resourcePath}/images/white_arrow.png), -webkit-gradient(linear, left top, left bottom, from(#12c9eb), to(#0fa5c1));
        /* Saf4+, Chrome */

        background-image: url(${common.resourcePath}/images/white_arrow.png), -webkit-linear-gradient(top, #12c9eb, #0fa5c1);
        /* Chrome 10+, Saf5.1+ */

        background-image: url(${common.resourcePath}/images/white_arrow.png), -moz-linear-gradient(top, #12c9eb, #0fa5c1);
        /* FF3.6+ */

        background-image: url(${common.resourcePath}/images/white_arrow.png), -ms-linear-gradient(top, #12c9eb, #0fa5c1);
        /* IE10 */

        background-image: url(${common.resourcePath}/images/white_arrow.png), -o-linear-gradient(top, #12c9eb, #0fa5c1);
        /* Opera 11.10+ */

        background-image: url(${common.resourcePath}/images/white_arrow.png), linear-gradient(top, #12c9eb, #0fa5c1);
        /* W3C */

        border-radius: 2px;
        border: none;
        -webkit-border-radius: 2px;
        -moz-border-radius: 2px;
        background-position: right;
        background-repeat: no-repeat;
        display: block;
        color: #fff;
        width: 110px;
        font-size: 14px;
        padding: 5px 8px 5px 2px;
        margin-right: 180px;
        line-height: 1em;
        float: right;
        height: 32px;
    }

    .paymentDetailsSection .paymentFormRow label.invalid {
        color: red;
        text-transform: none;
        float: right;
        margin-right: 5px;
        width: 175px;
    }

    .paymentDetailsSection .paymentFormRow.tncCheckbox input {
        float: left;
        margin: 6px 0 0 0;
    }

    .paymentDetailsSection .paymentFormRow.tncCheckbox label.invalid {
        float: left;
        width: 220px;
    }

    .paymentAddressDetails {
        font-size: 12px;
        border-collapse: collapse;
        color: #666;
        margin-bottom: 5px;
    }

    .paymentAddressDetails th {
        text-align: left;
        vertical-align: top;
        width: 220px;
        padding: 0;
        margin: 0;
        text-transform: uppercase;
        font-weight: normal;
    }

    .paymentAddressDetails td {
        padding: 0;
        margin: 0;
    }

    .paymentAddressDetails td.first {
        font-weight: bold;
    }
</style>
<script type="text/javascript" language="JavaScript" src="${common.resourcePath}/js/lib/jquery-1.6.4.min.js"></script>
<script type="text/javascript" language="JavaScript" src="${common.resourcePath}/js/jquery.validate.min.js"></script>
<script type="text/javascript" language="JavaScript"
        src="${common.resourcePath}/js/jquery.additional.methods.min.js"></script>
<script type="text/javascript" language="JavaScript" src="${common.resourcePath}/messages/i18n-common.js"></script>
<script type="text/javascript" language="JavaScript"
        src="${common.resourcePath}/messages/${common.localeMessages.locale}/messages_${common.localeMessages.locale}.js"></script>

<!-- Locale: ${common.localeMessages.locale} -->
<#assign now = .now>
<!-- Time: ${now} -->
<!-- ShopId: ${form.shopId!"Not set in DirectDebitFormModel"} -->
<div class="paymentDetailsSection paymentMethodElv">
    <form novalidate method="POST" id="paymentForm">

    <#-- NAME AND ADDRESS OF MERCHANT -->
    <#if form.companyAddress??>
        <h3><@js.message "dd.merchant.details" /></h3>
        <div class="paymentFormRow">
            <table width="100%" border="0" class="paymentAddressDetails">
                <tr>
                    <th><@js.message "dd.merchant.company_name" /></th>
                    <td>${form.companyAddress.companyName!""}</td>
                </tr>
                <#if form.companyAddress.address??>
                    <tr>
                        <th><@js.message "dd.merchant.address" /></th>
                        <td>
                            <#list form.companyAddress.address.addressLines as addressLine>
                                <#if addressLine?? && (addressLine?length > 0)>
                                ${addressLine}<br/>
                                </#if>
                            </#list>
                        ${form.companyAddress.address.postcode!""} ${form.consumerAddress.address.town!""}<br/>
                        ${form.companyAddress.address.country!""}
                        </td>
                    </tr>
                </#if>
            </table>
        </div>
    </#if>

    <#-- NAME AND ADDRESS OF CONSUMER -->
        <h3><@js.message "dd.customer.details" /></h3>
        <div class="paymentFormRow">
            <table width="100%" border="0" class="paymentAddressDetails">
                <tr>
                    <th><@js.message "dd.customer.name" /></th>
                    <td>${form.consumerAddress.fullName}</td>
                </tr>
                <tr>
                    <th><@js.message "dd.customer.address" /></th>
                    <td>
                    <#list form.consumerAddress.address.addressLines as addressLine>
                        <#if addressLine?? && (addressLine?length > 0)>
                        ${addressLine}<br/>
                        </#if>
                    </#list>
                    ${form.consumerAddress.address.postcode} ${form.consumerAddress.address.town}<br/>
                    ${form.consumerAddress.address.country}
                    </td>
                </tr>
            </table>
        </div>

        <h3><@js.message "dd.payment.details" /></h3>
    <#-- REASON OF DEBITING -->
        <div class="paymentFormRow">
            <label><@js.message "dd.payment.reference" /></label> <span
                class="paymentReference">${form.paymentReference}</span>
        </div>
    <#-- ACCOUNT NUMBER AND SORT CODE -->
        <div class="paymentFormRow">
            <label><@js.message "dd.amount" /></label>
            <span>${form.amount!""} ${form.currencyCode!""}</span></div>
        <div class="paymentFormRow">
            <label><@js.message "dd.account_holder" />:</label>
            <span>${form.accountHolder!""}</span></div>
        <div class="paymentFormRow">
            <label for="bankSortCode"><@js.message "dd.sort_code" />: <span>*</span></label>
            <input type="text" id="bankSortCode" name="bankSortCode"
                   placeholder='<@js.message "dd.sort_code.placeholder" />'
                   value="${form.bankSortCode!""}"/>
        </div>
        <div class="paymentFormRow">
            <label for="bankAccountNumber" class="requiredParameter"><@js.message "dd.account_number" />: <span>*</span></label>
            <input type="text" id="bankAccountNumber" name="bankAccountNumber"
                   placeholder='<@js.message "dd.account_number.placeholder" />'
                   value="${form.bankAccountNumber!""}"/>
        </div>
    <#if form.paymentType == "GLOBAL_COLLECT_DIRECT_DEBIT_DOMICILIACION_BANCARIA">
        <div class="paymentFormRow">
            <label for="branchCode" class="requiredParameter"><@js.message "dd.branch_code" />: <span>*</span></label>
            <input type="text" id="branchCode" name="branchCode"
                   placeholder='<@js.message "dd.branch_code.placeholder" />'
                   value="${form.branchCode!""}"/>
        </div>
        <div class="paymentFormRow">
            <label for="bankCheckDigit"><@js.message "dd.bank_check_digit" />: <span>*</span></label>
            <input type="text" id="bankCheckDigit" name="bankCheckDigit"
                   placeholder='<@js.message "dd.bank_check_digit.placeholder" />'
                   value="${form.bankCheckDigit!""}"/>
        </div>
    </#if>
        <div class="paymentFormRow">
            <label><@js.messageArgs "dd.address_line_n", ["1"] />:</label>
            <span>${form.addressLines[0]!""}</span>
        </div>
        <div class="paymentFormRow">
            <label><@js.messageArgs "dd.address_line_n", ["2"] />:</label>
            <span>${form.addressLines[1]!""}</span>
        </div>
        <div class="paymentFormRow">
            <label><@js.message "dd.postcode"/>:</label>
            <span>${form.postcode!""}</span>
        </div>
        <div class="paymentFormRow">
            <label><@js.message "dd.country" />:</label>
            <span>${form.country!""}</span>
        </div>
        <div class="paymentFormRow">
            <label><@js.message "Reversal period" /></label>
        </div>
        <div class="paymentFormRow tncCheckbox">
            <label for="tncCheckbox" class="checkbox"><@js.message "dd.agree_tnc" /></label>
            <input type="checkbox" id="tncCheckbox" name="tncCheckbox"/>
        </div>
        <div class="paymentFormRow">
            <input class="paymentFormSubmit" type="submit" value="<@js.message "dd.confirm" />"/>
        </div>
    </form>
</div>
<!-- VALIDATION RULES, REQUIRES -->
<script type="text/javascript">
    $(document).ready(function () {
        /* Payment page for Direct Debit form validation */
        if ($('.paymentDetailsSection').length) {
            $('#paymentForm').validate({
                rules:{
                    bankSortCode:{
                        required:true,
                        minlength:1,
                        maxlength:8,
                        pattern:/^[0-9]+$/
                    },
                    bankAccountNumber:{
                        required:true,
                        minlength:1,
                        maxlength:10,
                        pattern:/^[0-9]+$/
                    },
                <#if form.paymentType == "GLOBAL_COLLECT_DIRECT_DEBIT_DOMICILIACION_BANCARIA">
                    bankCheckDigit:{
                        required:  true,
                        minlength: 2,
                        maxlength: 2,
                        pattern:   /^[0-9]+$/
                    },
                    branchCode:{
                        required:  true,
                        minlength: 1,
                        maxlength: 5,
                        pattern:   /^[0-9]+$/
                    },
                </#if>
                    tncCheckbox:"required"
                },
                messages:{
                    bankSortCode:{
                        required:renderMessage('Sort Code required'),
                        minlength:renderMessage('Sort Code too short'),
                        maxlength:renderMessage('Sort Code too long')
                    },
                    bankAccountNumber:{
                        required:renderMessage('Bank Account Number required'),
                        minlength:renderMessage('Bank Account Number too short'),
                        maxlength:renderMessage('Bank Account Number too long')
                    },
                <#if form.paymentType == "GLOBAL_COLLECT_DIRECT_DEBIT_DOMICILIACION_BANCARIA">
                    bankCheckDigit: {
                        required:  renderMessage('Check digit required'),
                        minlength: renderMessage('Check digit required'),
                        maxlength: renderMessage('Check digit required')
                    },
                    branchCode: {
                        required:  renderMessage('Branch code required'),
                        minlength: renderMessage('Branch code required'),
                        maxlength: renderMessage('Branch code required')
                    },
                </#if>
                    tncCheckbox:renderMessage('Please confirm that you agree to T&C')
                },

                errorClass:'invalid'
            });
        }
    });
</script>