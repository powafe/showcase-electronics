<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<style>
    .orderSummarySection .paymentMethodBlock .first-column,
    .orderSummarySection .paymentMethodBlock .second-column
    {
        float: left;
    }
</style>
<div class="orderSummarySection">
    <div class="block">
        <div class="corner1">
            <div class="corner2">
                <div class="lineTop">
                    <#--<h2></h2>-->
                </div>
            </div>
        </div>
        <span id="error">
                <#if section.error??>
                <p class="error">
                   ${section.error}
                </p>
                </#if>
        </span>
        <div class="cont">
            <#--<div class="text">
             todo intro text here
            </div>-->
<#-- Billing Details -->
        <#if !section.confirmation || section.shippingList??>
            <#if (section.billingDetails?? && section.billingDetails.hasInfo)>
            <#assign bilDet = section.billingDetails.info>
<div class="wrapperBillingDet">
    <div class="billingDet">
            <table class="base alignLeft">
                <thead>
                    <tr>
                        <th class="first widthfix"><@js.message "billingDetails"/></th>
                        <th class="last"></th>
                    </tr>
                </thead>
                <tbody>
        <#assign i = 1/>
                <#list bilDet as line>
                        <#assign className = "${line.name?replace(' ', '')}"/>
                        <#assign className = "${className?replace('/', '')}"/>
                        <#assign className = "${className?uncap_first}"/>
                        <#assign isEven = (i % 2 == 0)/>
                        <#if isEven>
                            <tr class="${className} even">
                        <#else>
                            <tr class="${className}">
                        </#if>
                        <td class="name"><span>${line.name}</span></td>
                        <td class="last"><span>${line.value}</span></td>
                    </tr>
                <#assign i = i + 1/>
                </#list>
                 </tbody>
            </table>
    </div>
</div>
                </#if>
            </#if>
<#-- End Billing Details -->
<#-- Shipping Details -->
            <#if (section.shippingDetails?? && section.shippingDetails.hasInfo)>
                <#assign shipDet = section.shippingDetails.info>
            </#if>

            <#if (section.confirmation && !section.shippingList??)>
                <#if !(shipDet??)>
                    <#if (section.billingDetails?? && section.billingDetails.hasInfo)>
                        <#assign shipDet = section.billingDetails.info>
                    </#if>
                </#if>
            </#if>
            <#if section.showShippingInfo || (section.confirmation && !section.shippingList??)>
                <#if shipDet??>
    <div class="wrapperShippingDet">
        <div class="shippingDet">
                    <table class="base alignLeft">
                        <thead>
                            <tr>
                                <th class="first widthfix"><@js.message "shippingDetails"/></th>
                                <th class="last"></th>
                            </tr>
                        </thead>
                        <tbody>
             <#assign i = 1/>
                   <#list shipDet as line>
                        <#assign className = "${line.name?replace(' ', '')}"/>
                        <#assign className = "${className?replace('/', '')}"/>
                        <#assign className = "${className?uncap_first}"/>
                        <#assign isEven = (i % 2 == 0)/>
                        <#if isEven>
                            <tr class="${className} even">
                        <#else>
                            <tr class="${className}">
                        </#if>
                                <td class="name"><span>${line.name}</span></td>
                                <td class="last"><span>${line.value}</span></td>
                            </tr>
                        <#assign i = i + 1/>
                   </#list>
                         </tbody>
                    </table>
        </div>
    </div>
                </#if>
            </#if>
<#-- End Shipping Details -->
            <#if !section.shippingList??>
            <span id="error">
                <p class="error">
                    ${section.emptyShippingMessage}
                </p>
            </span>
            </#if>

            <#if !section.confirmation>
            <a class="link" href="${section.viewCart.link}"><@js.message "order-summary_viewCart"/></a>
            </#if>
            <div class="nofloat"></div>
            <#if section.confirmation>
                <form name="paymentForm" action="${section.submitLink}" method="POST" onsubmit="return showProgressForOrderSummary('#divPaymentProcessingProgress', '${common.resourcePath}');">
            <#else>
                <form name="paymentForm" action="${section.submitLink}" method="POST" onsubmit="return showProgressForOrderSummary('#divProgressBar', '${common.resourcePath}');">
            </#if>
<#-- Shipping Method Block -->
            <#if section.shippingList??>
        <div class="wrapperShippingMethodBlock">
                <div class="shippingMethodBlock">
            <div class="list">
                        <h4><@js.message "shippingMethod"/></h4>
                            <#list section.shippingList as shipping>
                            <div class="shipping${shipping.id}">
                                 <input id="${shipping.id}" class="radio" type="radio"  name="shippingMethodId" value="${shipping.id}" onclick="updateShippingMethod('${common.requestHost}${section.updateShippingURL}', '${shipping.id}')" <#if section.command?? &&  section.command.shippingMethodId?? && shipping.id == section.command.shippingMethodId> checked='true' </#if> />
                                 <label class="text" for="${shipping.id}">${shipping.name}</label>
                            </div>
                           </#list>
                    </div>
                </div>
        </div>
           </#if>
<#-- End Shipping Method Block -->
           <#if !section.confirmation || section.shippingList??>
<#-- Totals Block -->
        <div class="wrapperTotals">
                <div id="totals" class="totals">
                        <#if section.shoppingCartData??>
                            <#assign totals = section.shoppingCartData/>
                            <#include "/${common.templatePath}totals-block.ftl"/>
                        </#if>
                </div>
                </div>
<#-- End Totals Block -->
<#-- Payment Method Block -->
                <#if !section.confirmation>
                    <#if section.paymentMethodList??>
                        <div class="wrapperPaymentMethodBlock">
                            <#if !section.shoppingCartData.zeroValueCart>
                                <div class="paymentMethodBlock">
                            <#else>
                                <div class="paymentMethodBlock skip">
                            </#if>
                                <div class="list">
                                    <h4><@js.message "paymentMethod"/></h4>
                                    
                                    <#if section.channelPartner == 'ELECTROLUX_UK'>
                                        <#assign paymentMethodCounter=0>
                                        <div class="first-column">
                                    </#if>
                                    
                                    <#list section.paymentMethodList as paymentMethod>
                                       <div class="payment${paymentMethod.id}">
                                            <#assign checked = (section.command.paymentMethodAndCardBrand.paymentMethodId)?? && 
                                                paymentMethod.id == section.command.paymentMethodAndCardBrand.paymentMethodId>
                                                
                                            <#assign disabled = section.confirmation>
                                            
                                            <#if paymentMethod.cardBrandName??>
                                                <#assign checked = checked && 
                                                    section.command?? &&
                                                    (section.command.paymentMethodAndCardBrand.cardBrandName)?? &&
                                                    paymentMethod.cardBrandName == section.command.paymentMethodAndCardBrand.cardBrandName>
                                                
                                                <#assign radioId = "${paymentMethod.id}-${paymentMethod.cardBrandName}">
                                            <#else>
                                                <#assign radioId = "${paymentMethod.id}">
                                            </#if>
                                            
                                            <input id="payment${radioId}" 
                                                <#if checked>checked="true"</#if> 
                                                <#if disabled>disabled="true"</#if> 
                                                class="radio" 
                                                type="radio" 
                                                name="paymentMethodAndCardBrand" 
                                                value="{'paymentMethodId':${paymentMethod.id}, 'cardBrandName':'${paymentMethod.cardBrandName!}'}" />
                                            
                                            <#if paymentMethod.cardBrandName??>
                                                <label class="text" for="payment${radioId}">
                                                    <img src="${common.resourcePath}/images/card-brand-${paymentMethod.cardBrandName?lower_case}.jpg" />
                                                    ${paymentMethod.cardBrandDisplayName}
                                                </label>
                                            <#else>
                                                <label class="text" for="payment${radioId}">${paymentMethod.name}</label>
                                            </#if>
                                            
                                        </div>
                                        
                                        <#if section.channelPartner == 'ELECTROLUX_UK'>
                                            <#assign paymentMethodCounter=paymentMethodCounter+1>
                                            <#if paymentMethodCounter == 4>
                                                </div>
                                                <div class="second-column">
                                            </#if>
                                        </#if>
                                        
                                    </#list>
                                    
                                    <#if section.channelPartner == 'ELECTROLUX_UK'>
                                        </div>
                                    </#if>
                                </div>
                             </div>
                        </div>
                    </#if>
                </#if>
<#-- End Payment Method Block -->
            </#if>
                <div class="nofloat"></div>
                <input type="hidden" name="prevLink" value="${section.currentLink}"/>
                <#if section.confirmation>
                    <#assign buttonName ><@js.message "order-summary_placeOrder"/></#assign>
                <#else>
                    <#assign buttonName ><@js.message "continue"/></#assign>
                </#if>

                <#if section.shippingList??>
                    <input class="middleBtn rightBtn" type="submit" value="${buttonName}"/>
                </#if>
                <input type="hidden" name="<@csrf.name/>" value="<@csrf.value/>"/>
            </form>
 <form action="${section.backLink}" enctype="text/plain">
  	<input class="middleBtn leftBtn" type="submit" value="<@js.message "back"/>" />
 </form>
            <div class="nofloat"></div>
        </div>
        <div class="corner4"><div class="corner3"><div class="lineBtm"></div></div></div>
    </div>
    <div class="nofloat"></div>
</div>
