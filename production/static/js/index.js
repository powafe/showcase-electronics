$(function() {
    //header-carousel
    $('.header-carousel').owlCarousel({
        loop: true,
        autoplay: true,
        autoplayTimeout: 12500,
        dots: true,
        nav: true,
        navText: [
            "<i class='fa fa-angle-left'></i>",
            "<i class='fa fa-angle-right'></i>"
            ],
        items: 1
    });
    //testimonials-carousel
    $('.carousel-testimonials').owlCarousel({
        loop: true,
        autoplay: true,
        autoplayTimeout: 25000,
        dots: true,
        items: 1
    });
    //featured-products-carousel
    $('.carousel-hasfeatured-products').owlCarousel({
        loop: true,
        dots: false,
        margin: 10,
        nav: true,
        navText: [
            "<i class='fa fa-angle-left'></i>",
            "<i class='fa fa-angle-right'></i>"
        ],
        responsive:{
            0:{
                items: 1
            },
            480:{
                items: 2
            },
            767:{
                items: 3
            },
            1200:{
                items: 4
            }
        }
    })
});
$(function() {
    //arrows-resize
    $('.header-carousel .owl-controls').addClass('container');
    $(document).ready(arrowsResize);
    $(window).on('resize',arrowsResize);

    function arrowsResize() {
        $('.header-carousel .owl-nav').width($('.header-carousel .owl-nav').parent('.container').width());
        if($(window).width() < 768) {
            $('.header-carousel .owl-nav').hide();
        } else {
            $('.header-carousel .owl-nav').show();
        }
    }
});
$(function() {
    //demo-notice
    if ($.cookie('demo') == null) { 
        $('.demo-notice').show();
        $('.close-demo-notice').on('click', function(e){
            e.preventDefault();
            $('.demo-notice').slideUp('fast');
            $.cookie('demo', '5');
        });
    }
});
$(function() {
    //page-title
    $('.page-title').appendTo('#headerContainer');
});