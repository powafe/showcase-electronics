var customizedMessages = {
    "requiredCardNumber" : "Please enter Card Number.",
    "requiredExpireMonth" : "Please enter Expiry Date.",
    "requiredExpireYear" : "Please enter Expiry Date.",
    "requiredCreditCardType": "Please specify Card Type.",
    "requiredSecurityCode": "Please enter Security Code.",

    "maxLenCardNumber": "Invalid Credit Card number.",
    "maxlenSecurityCode": "Security Code must be 3 characters long and can contain only digits.",

    "minLenSecurityCode": "Security Code must be 3 characters long and can contain only digits.",

    "invalidCardNumber": "Card Number is invalid. Only digits allowed, please remove any spaces if present.",
    "invalidSecurityCode": "Security Code must be 3 characters long and can contain only digits.",

    "maxLenPassword": "Password should not exceed 32 characters. Please read <a href=\"javascript:showPasswordPolicyPopup()\">\"Password Strength Policy\"</a> for more details.",
    "minLenPassword": "Password should be at least 7 characters long. Please read <a href=\"javascript:showPasswordPolicyPopup()\">\"Password Strength Policy\"</a> for more details.",
    "ivalidPassword": "Password could contain only: a-z, A-Z, 0-9, and underscore. Please read <a href=\"javascript:showPasswordPolicyPopup()\">\"Password Strength Policy\"</a> for more details.",

    "maxLenSsv": "Search string should not exceed 255 characters.",
    "minLenSsv": "Search string should be at least 2 characters long.",
    "invalidSsv": "Search string contains disallowed characters.",

    "requiredPromoCode": "Please enter Promo Code.",
    "maxLenPromoCode": "Promo Code should not exceed 32 characters.",
    "invalidPromoCode": "Promo Code is invalid.",

    "maxLenCardHolder": "Card Holder Name should not exceed 255 characters.",
    "invalidCardHolder": "Card Holder Name is invalid.",

    "isRequired":"{0} is required.",
    "shouldNotExceed":"{0} should not exceed {1} characters.",
    "shouldBeAtLeast":"{0} should be at least {1} characters long.",
    "isInvalid":"{0} is invalid.",
    "doNotMatch": "{0} and {1} do not match.",
    "pleaseEnter": "Please enter '{0}'.",
    "containsIllegalCharacters": "'{0}' contains illegal characters. Please check and try again."

};

function getMessageByKey(key, args)
{
    message = customizedMessages[key];
    return getMessage(message, args);
}


function getMessage(message, values)
{
    if (typeof(values) !== 'undefined')
    {

        for (var i = 0; i < values.length; i++)
        {
            var holder = '{' + i + '}';
            var value = values[i];

            message = message.replace(holder, value);
        }
    }
    return message;
}
