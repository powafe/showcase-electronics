// INTERNATIONALIZATION UTILITIES //

var messageVariable = /{(\d+)}/g;

function getValue(values, index, default_value)
{
    if(index >= 0 && index < values.length) {
        return values[index];
    } else {
        return default_value;
    }
}

function renderMessage(messageKey, values) {
    if (!customizedMessages.hasOwnProperty(messageKey)) {
        return messageKey;
    } else {
        var message = customizedMessages[messageKey];
        if ((typeof(values) !== 'undefined') && (values.constructor.name == 'Array' || values instanceof Array)) {
            return message.replace(
                messageVariable,
                function (_match, index) {
                    var default_value = "";
                    return getValue(values, index, default_value);
                }
            )
        } else {
            return message;
        }
    }
}
