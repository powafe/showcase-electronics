var customizedMessages = {
    "requiredCardNumber" : "Tast inn kortnummer.",
    "requiredExpireMonth" : "Tast inn utl�psdato.",
    "requiredExpireYear" : "Tast inn utl�psdato.",
    "requiredCreditCardType": "Oppgi korttype.",
    "requiredSecurityCode": "Tast inn sikkerhetskode.",

    "maxLenCardNumber": "Ugyldig kredittkortnummer.",
    "maxlenSecurityCode": "Sikkerhetskoden m� v�re 3 tegn lang og kan kun inneholde tall.",

    "minLenSecurityCode": "Sikkerhetskoden m� v�re 3 tegn lang og kan kun inneholde tall.",

    "invalidCardNumber": "Kortnummeret er ugyldig. Kun tall er tillatt, fjern mulige mellomrom.",
    "invalidSecurityCode": "Sikkerhetskoden m� v�re 3 tegn lang og kan kun inneholde tall.",

    "maxLenPassword": "Passordet m� ikke overskride 32 tegn. Vennligst les <a href=\"javascript:showPasswordPolicyPopup()\">\"Retningslinjer for passordstyrke\"</a> for mer informasjon.",
    "minLenPassword": "Passordet m� minimum inneholde 7 tegn. Vennligst les <a href=\"javascript:showPasswordPolicyPopup()\">\"Retningslinjer for passordstyrke\"</a> for mer informasjon.",
    "ivalidPassword": "Passordet kan bare inneholde: a-z, A-Z, 0-9 og understrek. Vennligst les <a href=\"javascript:showPasswordPolicyPopup()\">\"Retningslinjer for passordstyrke\"</a> for mer informasjon.",

    "maxLenSsv": "S�kestrengen kan ikke overskride 255 tegn.",
    "minLenSsv": "S�kestrengen skal v�re minimum 2 tegn.",
    "invalidSsv": "S�kestrengen inneholder ugyldige tegn.",

    "requiredPromoCode": "Tast inn kampanjekode.",
    "maxLenPromoCode": "Kampanjekoden skal ikke overstige 32 tegn.",
    "invalidPromoCode": "Kampanjekoden er ugyldig.",

    "maxLenCardHolder": "Kortholderens navn skal ikke overstige 255 tegn.",
    "invalidCardHolder": "Kortholderens navn er ugyldig.",

    "isRequired":"{0} er n�dvendig.",
    "shouldNotExceed":"{0} skal ikke overskride {1} tegn.",
    "shouldBeAtLeast":"{0} m� v�re minimum {1} tegn langt.",
    "isInvalid":"{0} er ugyldig.",
    "doNotMatch": "{0} og {1} samsvarer ikke.",
    "pleaseEnter": "Tast inn '{0}'.",
    "containsIllegalCharacters": "{0}' inneholder ugyldige tegn. Vennligst kontroller og pr�v igjen."

};

function getMessageByKey(key, args)
{
    message = customizedMessages[key];
    return getMessage(message, args);
}


function getMessage(message, values)
{
    if (typeof(values) !== 'undefined')
    {

        for (var i = 0; i < values.length; i++)
        {
            var holder = '{' + i + '}';
            var value = values[i];

            message = message.replace(holder, value);
        }
    }
    return message;
}
