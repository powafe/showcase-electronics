var customizedMessages = {
    "requiredCardNumber" : "Bitte geben Sie die Kartennummer ein.",
    "requiredExpireMonth" : "Bitte geben Sie das Ablaufdatum ein.",
    "requiredExpireYear" : "Bitte geben Sie das Ablaufdatum ein.",
    "requiredCreditCardType": "Bitte geben Sie den Kartentyp an.",
    "requiredSecurityCode": "Bitte geben Sie den Sicherheitscode ein.",

    "maxLenCardNumber": "Ung�ltige Kreditkartennummer.",
    "maxlenSecurityCode": "Der Sicherheitscode muss drei Stellen lang sein und kann nur aus Zahlen bestehen.",

    "minLenSecurityCode": "Der Sicherheitscode muss drei Stellen lang sein und kann nur aus Zahlen bestehen.",

    "invalidCardNumber": "Die Kartennummer ist ung�ltig. Es sind nur Zahlen zul�ssig, bitte entfernen Sie n�tigenfalls auch alle Leerzeichen.",
    "invalidSecurityCode": "Der Sicherheitscode muss drei Stellen lang sein und kann nur aus Zahlen bestehen.",

    "maxLenPassword": "Das Passwort darf nicht l�nger als 32 Zeichen sein. Bitte lesen Sie die <a href=\"javascript:showPasswordPolicyPopup()\">\"Passwort St�rke Politik\"</a>, um mehr zu erfahren.",
    "minLenPassword": "Das Passwort muss mindestens 7 Zeichen lang sein. Bitte lesen Sie die <a href=\"javascript:showPasswordPolicyPopup()\">\"Passwort St�rke Politik\"</a>, um mehr zu erfahren.",
    "ivalidPassword": "Das Passwort darf nur aus den Zeichen a-z, A-Z, 0-9 und Unterstrichen bestehen. Bitte lesen Sie die <a href=\"javascript:showPasswordPolicyPopup()\">\"Passwort St�rke Politik\"</a>, um mehr zu erfahren.",

    "maxLenSsv": "Die Suchzeichenfolge darf aus nicht mehr als 255 Zeichen bestehen.",
    "minLenSsv": "Die Suchzeichenfolge muss aus mindestens zwei Zeichen bestehen.",
    "invalidSsv": "Die Suchzeichenfolge enth�lt unzul�ssige Zeichen.",

    "requiredPromoCode": "Bitte geben Sie den Aktionscode ein.",
    "maxLenPromoCode": "Der Aktionscode darf aus nicht mehr als 32 Zeichen bestehen.",
    "invalidPromoCode": "Der Aktionscode ist ung�ltig.",

    "maxLenCardHolder": "Der Name des Karteninhabers kann aus nicht mehr als 255 Zeichen bestehen.",
    "invalidCardHolder": "Der Name des Karteninhabers ist ung�ltig.",

    "isRequired":"{0} erforderlich.",
    "shouldNotExceed":"{0} darf aus nicht mehr als {1} Zeichen bestehen.",
    "shouldBeAtLeast":"{0} muss aus mindestens {1} Zeichen bestehen.",
    "isInvalid":"{0} ist ung�ltig.",
    "doNotMatch": "{0} und {1} stimmen nicht �berein.",
    "pleaseEnter": "Bitte geben Sie {0} ein.",
    "containsIllegalCharacters": "{0} enth�lt unzul�ssige Zeichen. Bitte �berpr�fen Sie und versuchen Sie es erneut.",

    "Bank Account Number required":"Kontonummer ist ein pflichtfeld",
    "Bank Account Number too short":"Kontonummer ist zu kurz",
    "Bank Account Number too long":"Kontonummer ist zu lang",

    "Sort Code required":"Bankleitzahl ist ein pflichtfeld",
    "Sort Code too short":"Bankleitzahl ist zu kurz",
    "Sort Code too long":"Bankleitzahl ist zu lang",

    "Please confirm that you agree to T&C":"DE Please confirm that you agree to T&C"

};

function getMessageByKey(key, args)
{
    message = customizedMessages[key];
    return getMessage(message, args);
}


function getMessage(message, values)
{
    if (typeof(values) !== 'undefined')
    {

        for (var i = 0; i < values.length; i++)
        {
            var holder = '{' + i + '}';
            var value = values[i];

            message = message.replace(holder, value);
        }
    }
    return message;
}
