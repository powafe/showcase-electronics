var customizedMessages = {
    "requiredCardNumber" : "S'il vous pla�t entrez le num�ro de carte.",
    "requiredExpireMonth" : "S'il vous pla�t entrer la date d'expiration.",
    "requiredExpireYear" : "S'il vous pla�t entrer la date d'expiration.",
    "requiredCreditCardType": "S'il vous pla�t sp�cifier le type de carte.",
    "requiredSecurityCode": "S'il vous pla�t entrez le code de s�curit�.",

    "maxLenCardNumber": "Blancs num�ro de carte de cr�dit.",
    "maxlenSecurityCode": "Code de la s�curit� doit �tre de 3 caract�res de long et peut contenir que des chiffres.",

    "minLenSecurityCode": "Code de la s�curit� doit �tre de 3 caract�res de long et peut contenir que des chiffres.",

    "invalidCardNumber": "Card Number is invalid. Allowed digits only, please remove this if Any spaces.",
    "invalidSecurityCode": "Code de la s�curit� doit �tre de 3 caract�res de long et peut contenir que des chiffres.",

    "maxLenPassword": "Mot de passe ne doit pas d�passer 32 caract�res. S'il vous pla�t lire <a href=\"javascript:showPasswordPolicyPopup()\">\"Politique Password Strength\"</a> pour plus de d�tails.",
    "minLenPassword": "Mot de passe doit �tre d'au moins 7 caract�res. S'il vous pla�t lire <a href=\"javascript:showPasswordPolicyPopup()\">\"Politique Password Strength\"</a> pour plus de d�tails.",
    "ivalidPassword": "Mot de passe peut contenir uniquement: az, AZ, 0-9, et de soulignement. S'il vous pla�t lire <a href=\"javascript:showPasswordPolicyPopup()\">\"Politique Password Strength\"</a> pour plus de d�tails.",

    "maxLenSsv": "Cha�ne de recherche ne doit pas d�passer 255 caract�res.",
    "minLenSsv": "Cha�ne de recherche doit �tre d'au moins 2 caract�res.",
    "invalidSsv": "Cha�ne de recherche contient des caract�res non autoris�s.",

    "requiredPromoCode": "S'il vous pla�t entrez le code promotionnel.",
    "maxLenPromoCode": "Code promotionnel ne doit pas d�passer 32 caract�res.",
    "invalidPromoCode": "Code promotionnel n'est pas valide.",

    "maxLenCardHolder": "Nom du titulaire de carte ne doit pas d�passer 255 caract�res.",
    "invalidCardHolder": "Nom du titulaire de carte n'est pas valide.",

    "isRequired":"{0} est requis.",
    "shouldNotExceed":"{0} ne doit pas d�passer {1} caract�res.",
    "shouldBeAtLeast":"{0} doit �tre au moins {1} caract�res.",
    "isInvalid":"{0} n'est pas valide.",
    "doNotMatch": "{0} et {1} ne correspondent pas.",
    "pleaseEnter": "S'il vous pla�t entrez{0}'.",
    "containsIllegalCharacters": "'{0}' contient des caract�res ill�gaux. S'il vous pla�t v�rifiez et essayez � nouveau."

};

function getMessageByKey(key, args)
{
    message = customizedMessages[key];
    return getMessage(message, args);
}


function getMessage(message, values)
{
    if (typeof(values) !== 'undefined')
    {

        for (var i = 0; i < values.length; i++)
        {
            var holder = '{' + i + '}';
            var value = values[i];

            message = message.replace(holder, value);
        }
    }
    return message;
}
