var customizedMessages = {
    "requiredCardNumber" : "Por favor, ingrese el n�mero de tarjeta de.",
    "requiredExpireMonth" : "Por favor, introduzca Fecha de Expiraci�n.",
    "requiredExpireYear" : "Por favor, introduzca Fecha de Expiraci�n.",
    "requiredCreditCardType": "Por favor, especificar el tipo de tarjeta.",
    "requiredSecurityCode": "Por favor, introduzca el c�digo de seguridad.",

    "maxLenCardNumber": "De cr�dito v�lida n�mero de la tarjeta.",
    "maxlenSecurityCode": "C�digo iSecurity debe ser de 3 caracteres y s�lo puede contener d�gitos.",

    "minLenSecurityCode": "C�digo de Seguridad debe ser de 3 caracteres y s�lo puede contener d�gitos.",

    "invalidCardNumber": "N�mero de la tarjeta no es v�lida. S�lo se permiten d�gitos, por favor, quitar todos los espacios si est�n presentes.",
    "invalidSecurityCode": "C�digo de Seguridad debe ser de 3 caracteres y s�lo puede contener d�gitos.",

    "maxLenPassword": "La contrase�a no debe exceder los 32 caracteres. Por favor, lea <a href=\"javascript:showPasswordPolicyPopup()\">\"Contrase�a Pol�tica de Fuerza\"</a> para m�s detalles.",
    "minLenPassword": "Contrase�a debe ser de al menos 7 caracteres de longitud. Por favor, lea <a href=\"javascript:showPasswordPolicyPopup()\">\"Contrase�a Pol�tica de Fuerza\"</a> para m�s detalles.",
    "ivalidPassword": "Contrase�a podr�an contener s�lo: az, AZ, 0-9 y subrayado. Por favor, lea <a href=\"javascript:showPasswordPolicyPopup()\">\"Contrase�a Pol�tica de Fuerza\"</a> para m�s detalles.",

    "maxLenSsv": "Cadena de b�squeda no debe superar los 255 caracteres.",
    "minLenSsv": "Cadena de b�squeda debe ser por lo menos 2 caracteres.",
    "invalidSsv": "Cadena de b�squeda contiene caracteres no permitidos.",

    "requiredPromoCode": "Por favor, introduzca C�digo de promoci�n.",
    "maxLenPromoCode": "C�digo de promoci�n no debe superar los 32 caracteres.",
    "invalidPromoCode": "C�digo de promoci�n no es v�lida.",

    "maxLenCardHolder": "Nombre titular de la tarjeta no debe superar los 255 caracteres.",
    "invalidCardHolder": "Nombre titular de la tarjeta no es v�lida.",

    "isRequired":"{0} es necesaria.",
    "shouldNotExceed":"{0} no debe exceder de {1} caracteres.",
    "shouldBeAtLeast":"{0} debe ser por lo menos {1} caracteres de largo.",
    "isInvalid":"{0} no es v�lido.",
    "doNotMatch": "{0} y {1} no coinciden.",
    "pleaseEnter": "Por favor, introduzca '{0}'.",
    "containsIllegalCharacters": "'{0}' contiene caracteres no v�lidos. Por favor revise y vuelva a intentarlo.",

    "Bank Account Number required":  "N�mero de la cuenta necesario",
    "Bank Account Number too long":  "N�mero de la cuenta demasiado largo",
    "Bank Account Number too short": "N�mero de la cuenta demasiado corto",

    "Sort Code required":  "C�digo del Entitad necesario",
    "Sort Code too long":  "C�digo del Entitad demasiado largo",
    "Sort Code too short": "C�digo del Entitad demasiado corto",

    "Check digit required":  "Digito de Control necesario",
    "Check digit too long":  "Digito de Control demasiado largo",
    "Check digit too short": "Digito de Control demasiado corto",

    "Branch code required":  "C�digo de sucursal necesario",
    "Branch code too long":  "C�digo de sucursal demasiado largo",
    "Branch code too short": "C�digo de sucursal demasiado corto",

    "Please confirm that you agree to T&C":"Please confirm that you agree to T&C"
};

function getMessageByKey(key, args)
{
    message = customizedMessages[key];
    return getMessage(message, args);
}


function getMessage(message, values)
{
    if (typeof(values) !== 'undefined')
    {

        for (var i = 0; i < values.length; i++)
        {
            var holder = '{' + i + '}';
            var value = values[i];

            message = message.replace(holder, value);
        }
    }
    return message;
}
