<#assign version="1.0"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name=”viewport” content=”width=device-width, initial-scale=1, maximum-scale=1″>
    <title>Order Confirmation</title>
    <style type="text/css">
        /*reset*/
        #outlook a {padding:0;}
        body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;} 
        .ExternalClass {width:100%;}
        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
        #backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
        img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;} 
        a img {border:none;} 
        .image_fix {display:block;}
        table td {border-collapse: collapse;}
        table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
        /*styles*/
        table {
            font-size: 100%;
            border-collapse: collapse;
            clear: both;
            background: #fff;
            margin-bottom: 20px;
            font-family: arial, sans-serif;
            font-weight: 300; 
            white-space: normal;
            color: #545454;
        }
        table th {
            font-weight: 300;
            text-transform: uppercase;
            font-size: 16px;
            padding: 20px 0 10px;
            border-top: 1px solid #eee;
            border-bottom: 1px solid #eee;
            text-align: left;
        }
        table td {
            font-size: 14px;
            line-height: 1.5;
        }
        a {
            text-decoration: none;
        }
        a:hover {
            text-decoration: underline;
        }
    </style>
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="backgroundTable">
    <tr>
    <td width="10"><img border="0" height="1" src="http://namwpm.eccmp.com/wpm/390/ContentUploads/DynamicEmail/spacer_002.gif" style="display:block" width="10"></td>

        <td align="center" style="max-width: 600px; display: block; clear: both; margin: 0 auto;">
                    

            <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 20px;">
                <tr>
                    <td align="center" style="padding: 20px 40px;"><a href="http://electronics.site.powa.com" title="Showcase Electronics"><img alt="" src="http://www.electronics.sandbox.powa.com/file/Q553Bua/b04ec47f-db41-4d9a-a210-9a7aad8470ce.png"></a></td>
                </tr>
                <tr>
                    <td align="center"><img alt="" src="http://www.electronics.sandbox.powa.com/file/Q553Bua/163d45cf-2e78-4c2e-a12f-4b0262cc8301.jpg"></td>
                </tr>
            </table>

            <#if section.orderData??>
            <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 20px;">
                <tbody align="left">
                    <tr>
                        <td class="first" style="padding-bottom: 30px;">Thank you for your order.</td>
                        <td align="right" valign="top"><a href="http://electronics.site.powa.com" title="Showcase Electronics"><img alt="" src="http://www.electronics.sandbox.powa.com/file/Q553Bua/7a2e61cc-d1bb-4003-b3da-dc510e137f7a.jpg"></a></td>
                    </tr>
                    <tr>
                        <td class="name"><@js.message "order.details.order.number"/>&nbsp;${section.orderData.id}</td>
                    </tr>
                    <tr class="even">
                        <td class="name"><@js.message "order.details.order.date"/>&nbsp;${section.orderData.orderDateObj?string('dd MMM yyyy')}</td>
                    </tr>
                </tbody>
            </table>
            </#if>

            <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 0;">
                <thead align="left"> 
                    <tr>
                        <th class="first" style="font-weight: 300;text-transform: uppercase;font-size: 16px;padding: 20px 0 10px;border-top: 1px solid #eee;border-bottom: 1px solid #eee;"><@js.message "order.details.product.name"/></th>
                        <th style="text-align: center;font-weight: 300;text-transform: uppercase;font-size: 16px;padding: 20px 0 10px;border-top: 1px solid #eee;border-bottom: 1px solid #eee;"><@js.message "order.details.quantity"/></th>
                        <th style="text-align: center;font-weight: 300;text-transform: uppercase;font-size: 16px;padding: 20px 0 10px;border-top: 1px solid #eee;border-bottom: 1px solid #eee;"><@js.message "order.details.price"/></th>
                        <th class="last" style="text-align: center;font-weight: 300;text-transform: uppercase;font-size: 16px;padding: 20px 0 10px;border-top: 1px solid #eee;border-bottom: 1px solid #eee;"><@js.message "order.details.subtotal"/></th>
                    </tr>
                </thead>
                <tbody align="left">
                <#if section.itemList??>
                        <#list section.itemList as item>
                            <#if item.SKU??>
                                <#assign sku = item.SKU>
                            <#else>
                                <#assign sku = "">                                
                            </#if>
                            <#if item.options??>
                            <#assign rowspan = item.options?size + 1>
                            <tr>
                                <td class="name" style="border-bottom: 1px solid #eee; padding: 10px 0;">
                                    <a href="http://www.electronics.sandbox.powa.com${item.link}" style="color: #535353; text-transform: uppercase; text-decoration: underline;">
                                        <span style="line-height:1;">${item.name}</span>
                                        <#list item.options as option>
                                            <p class="nameOption" style="font-size: 14px; line-height: 1; margin: 0 0 10px;">
                                                <span class="nameOption" style="color: #535353; text-transform: uppercase; text-decoration: underline;">${option.name}: <b style="font-weight: bold;">${option.value}</b></span>
                                                <span class="price" style="color: #535353; text-transform: uppercase; text-decoration: underline;">${option.price}</span>
                                            </p>
                                        </#list>
                                    </a>
                                </td>
                                <td class="qty" rowspan="${rowspan}" style="text-align: center; border-bottom: 1px solid #eee; padding: 10px 0;">${item.quantity}</td>
                                <td class="price" style="text-align: center; border-bottom: 1px solid #eee; padding: 10px 0;">${item.price}</td>
                                <td class="price last" rowspan="${rowspan}" style="text-align: center; border-bottom: 1px solid #eee; padding: 10px 0;">${item.subtotal}</td>
                            </tr>
                                
                            <#else/>
                                <tr>
                                    <td class="name" style="border-bottom: 1px solid #eee; padding: 10px 0;"><a href="http://www.electronics.sandbox.powa.com${item.link}" style="color: #535353; text-transform: uppercase; text-decoration: underline;"><span style="line-height:1;">${item.name}<span></a></td>
                                    <td class="qty" style="text-align: center; border-bottom: 1px solid #eee; padding: 10px 0;">${item.quantity}</td>
                                    <td class="price" style="text-align: center; border-bottom: 1px solid #eee; padding: 10px 0;">${item.price}</td>
                                    <td class="price last" style="text-align: center; border-bottom: 1px solid #eee; padding: 10px 0;">${item.subtotal}</td>
                                </tr>
                            </#if>

                        </#list>
                </#if>
                </tbody>
            </table>

            <div style="width: 48%; float: left;">

                <#if (section.billingDetails?? && section.billingDetails.hasInfo)>
                <#assign bilDet = section.billingDetails.info>
                <table width="100%" border="0" cellpadding="0" cellspacing="0" align="left" style="margin-bottom: 0;">
                    <thead align="left">
                        <tr>
                            <th class="first widthfix" style="border-top: none; text-align: left; font-weight: 300;text-transform: uppercase;font-size: 16px;padding: 20px 0 10px;border-bottom: 1px solid #eee;"><@js.message "order.details.billing.details"/></th>
                            <th class="last" style="border-top: none; font-weight: 300;text-transform: uppercase;font-size: 16px;padding: 20px 0 10px;border-bottom: 1px solid #eee;"></th>
                        </tr>
                    </thead>
                    <tbody align="left">
                    <tr>
                        <td style="padding-top: 10px;"></td>
                        <td style="padding-top: 10px;"></td>
                    </tr>
                        <#list bilDet as line>
                        <#if line.name == bilDet?first.name>
                        <tr>
                        <#else>
                        <tr class="even">
                        </#if>
                            <td class="name">${line.name}</td>
                            <td class="last" align="right">${line.value}</td>
                        </tr>
                        </#list>
                    <tr>
                        <td style="padding-top: 10px;"></td>
                        <td style="padding-top: 10px;"></td>
                    </tr>
                     </tbody>
                </table>
                </#if>

            </div>

            <div style="width: 48%; float: right;">

                <table width="100%" border="0" cellpadding="0" cellspacing="0" align="right" style="margin-bottom: 0;">
                    <thead>
                        <tr>
                            <th class="first widthfix" style="border-top: none; text-align: left; font-weight: 300;text-transform: uppercase;font-size: 16px;padding: 20px 0 10px;border-bottom: 1px solid #eee;"><@js.message "order.details.shipping.details"/></th>
                            <th class="last" style="border-top: none;font-weight: 300;text-transform: uppercase;font-size: 16px;padding: 20px 0 10px;border-bottom: 1px solid #eee;"></th>
                        </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td style="padding-top: 10px;"></td>
                        <td style="padding-top: 10px;"></td>
                    </tr>
                    <#if (section.shippingDetails?? && section.shippingDetails.hasInfo)>
                        <#assign details = section.shippingDetails.info>
                    <#else>
                        <#assign details = section.billingDetails.info>
                    </#if>
                    <#list details as line>
                        <#if line.name== details?first.name>
                        <tr>
                        <#else>
                        <tr class="even">
                        </#if>
                        <td class="name">${line.name}</td>
                        <td class="last" align="right">${line.value}</td>
                        </tr>
                    </#list>
                    <tr>
                        <td style="padding-top: 10px;"></td>
                        <td style="padding-top: 10px;"></td>
                    </tr>
                    </tbody>
                </table>

            </div>

            
            <#if section.shoppingCartData??>
            <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 20px;">
                <thead>
                    <tr>
                        <th style="text-align: left;font-weight: 300;text-transform: uppercase;font-size: 16px;padding: 20px 0 10px;border-top: 1px solid #eee;border-bottom: 1px solid #eee;">Subtotal</th>
                        <th style="font-weight: 300;text-transform: uppercase;font-size: 16px;padding: 20px 0 10px;border-top: 1px solid #eee;border-bottom: 1px solid #eee;"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="border-bottom: 1px solid #eeeeee; padding: 10px 0;"><@js.message "order.details.product.subtotal"/></td>
                        <td class="price last" style="border-bottom: 1px solid #eeeeee; padding: 10px 0;" align="right">${section.shoppingCartData.subtotal}</td>
                    </tr>
                    <tr>
                        <td style="border-bottom: 1px solid #eeeeee; padding: 10px 0;"><@js.message "order.details.shipping"/></td>
                        <td class="price last" style="border-bottom: 1px solid #eeeeee; padding: 10px 0;" align="right">${section.shoppingCartData.shippingTotal}</td>
                    </tr>
                    <#if section.shoppingCartData.discount??>
                        <tr>
                            <td style="border-bottom: 1px solid #eeeeee; padding: 10px 0;">${section.shoppingCartData.discount.name}</td>
                            <td class="price last" style="border-bottom: 1px solid #eeeeee; padding: 10px 0;" align="right">- ${section.shoppingCartData.discount.price}</td>
                        </tr>
                    </#if>
                    <#if section.shoppingCartData.oldTaxList??>
                        <#list section.shoppingCartData.oldTaxList as tax>
                            <tr>
                                <td style="border-bottom: 1px solid #eeeeee; padding: 10px 0;">${tax.name}</td>
                                <td class="price last" style="border-bottom: 1px solid #eeeeee; padding: 10px 0;" align="right">${tax.cost}</td>
                            </tr>
                        </#list>
                        <tr>
                            <td style="border-bottom: 1px solid #eeeeee; padding: 10px 0; text-transform: uppercase;"><@js.message "order.details.total"/></td>
                            <td class="price last" style="border-bottom: 1px solid #eeeeee; padding: 10px 0;" align="right"><#if section.shoppingCartData.total??>${section.shoppingCartData.total}</#if></td>
                        </tr>
                    <#else>
                        <#if section.shoppingCartData.taxIncluded>
                             <#if section.shoppingCartData.flatTaxList??>
                             <#list section.shoppingCartData.flatTaxList as tax>
                                <tr>
                                    <td style="border-bottom: 1px solid #eeeeee; padding: 10px 0;">${tax.name}</td>
                                    <td class="price last" style="border-bottom: 1px solid #eeeeee; padding: 10px 0;" align="right">${tax.cost}</td>
                                </tr>
                             </#list>
                             </#if>
                             <tr>
                                <td style="border-bottom: 1px solid #eeeeee; padding: 10px 0; text-transform: uppercase;"><@js.message "order.details.total"/></td>
                                <td class="price last" style="border-bottom: 1px solid #eeeeee; padding: 10px 0;" align="right"><#if section.shoppingCartData.total??>${section.shoppingCartData.total}</#if></td>
                             </tr>
                        <#else>
                            <#if section.shoppingCartData.percTaxList??>
                            <#list section.shoppingCartData.percTaxList as tax>
                                <tr>
                                    <td style="border-bottom: 1px solid #eeeeee; padding: 10px 0;">${tax.name}</td>
                                    <td class="price last" style="border-bottom: 1px solid #eeeeee; padding: 10px 0;" align="right">${tax.cost}</td>
                                </tr>
                            </#list>
                            </#if>
                            <#if section.shoppingCartData.flatTaxList??>
                            <#list section.shoppingCartData.flatTaxList as tax>
                                <tr>
                                    <td style="border-bottom: 1px solid #eeeeee; padding: 10px 0;">${tax.name}</td>
                                    <td class="price last" style="border-bottom: 1px solid #eeeeee; padding: 10px 0;" align="right">${tax.cost}</td>
                                </tr>
                            </#list>
                            </#if>
                            <tr>
                                <td style="border-bottom: 1px solid #eeeeee; padding: 10px 0; text-transform: uppercase;"><@js.message "order.details.total"/></td>
                                <td class="price last" style="border-bottom: 1px solid #eeeeee; padding: 10px 0;" align="right"><#if section.shoppingCartData.total??>${section.shoppingCartData.total}</#if></td>
                            </tr>
                        </#if>
                    </#if>
                </tbody>
            </table>
            </#if>
            <table width="100%" border="0" cellpadding="0" cellspacing="0" style="background-color: #666666;" style="margin-bottom: 20px;">
                <tbody>
                    <tr>
                        <td align="center" style="padding: 15px 0; color: #ffffff; font-size: 12px;">
                            <a href="http://www.electronics.site.powa.com/page/About+Us" title="About Us" style="color: #ffffff;">About Us</a>&nbsp;|&nbsp;
                            <a href="http://www.electronics.site.powa.com/page/About+Us" title="Shipping & Returns" style="color: #ffffff;">Shipping & Returns</a>&nbsp;|&nbsp;
                            <a href="http://www.electronics.site.powa.com/page/About+Us" title="Privacy Notice" style="color: #ffffff;">Privacy Notice</a>&nbsp;|&nbsp;
                            <a href="http://www.electronics.site.powa.com/page/Conditions+of+Use" title="Condiditons of Use" style="color: #ffffff;">Conditions of Use</a>&nbsp;|&nbsp;
                            <a href="http://www.electronics.site.powa.com/page/About+Us" title="Contact Us" style="color: #ffffff;">Contact Us</a>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 20px;">
                <tbody>
                    <tr>
                        <td align="left">
                            <img src="http://www.electronics.sandbox.powa.com/template/resource/runtime/542/g16/static/images/pciLogo.png" title="PCI Compliant" alt="PCI Compliant">
                        </td>
                        <td align="center">
                            <img src="http://www.electronics.sandbox.powa.com/file/Q553Bua/5a5cc343-05c1-4f1e-99bb-4e7811a35fa0.jpg" />
                        </td>
                        <td align="right">
                            <img src="http://www.electronics.sandbox.powa.com/template/resource/runtime/542/g16/static/images/LocaytaLogo.png" title="Search technology provided by Locayta" alt="Search technology provided by Locayta"></a>
                            <img src="http://www.electronics.sandbox.powa.com/template/resource/runtime/542/g16/static/images/poweredBy.png" title="Powa Website and Shopping Cart Solutions" alt="Powa Website and Shopping Cart Solutions">
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td align="center" style="font-size: 11px; color: #aaaaaa; text-align: center;">©2014 PowaTag Showcase Demo Site</td>
                    </tr>
                </tbody>
            </table>

        </td>

        <td width="10"><img border="0" height="1" src="http://namwpm.eccmp.com/wpm/390/ContentUploads/DynamicEmail/spacer_002.gif" style="display:block" width="10"></td>

    </tr>
</table>  
</body>
</html>