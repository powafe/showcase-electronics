<html>
<head>
	<style type="text/css">
		<!--
		body {
			font: 70% tahoma, arial, helvetica, sans-serif;
		}

		table {
			font-size: 100%;
			border-collapse: collapse;
			width: 600px;
			clear: both;
			background: #eee;
			margin-bottom: 5px;
		}

		table th {
			background: #87c2f8;
			text-transform: uppercase;
			font-size: 90%;
			text-align: center;
			padding: 5px 1px;
		}

		table th.widthfix {
			width: 300px;
		}

		table td {
			text-align: left;
			padding: 5px;
			border-left: 1px solid #333;
			border-top: 2px solid #fff;
            vertical-align:top;
            white-space: nowrap;
        }

		table td.name, table td.nameOption {
			border-left: none;
		}

		table td.nameOption {
			padding: 0 10px 10px;
			padding-left: 35px !important;
		}

		table td.nameOption i {
			padding-left: 10px;
		}

		table tr.nameOption td {
			border-top: none;
			padding: 1px 5px;
		}

		table.totalBlock {
			margin-left: 300px;
			width: 300px;
			background: #eee;
		}

		table td.price, table td.qty {
			text-align: right;
		}

		table td.price, table td.qty {
			width: 90px;
		}

		table td.qty {
			width: 30px;
		}

        div.cont {
			width: 600px;
		}

		//-->
	</style>
</head>
<body id="shop" bgcolor="#ffffff">

 <div class="cont">
            <@js.message "globalcollect.payment.rejected.header"/>
            <br/><br/>
            <@js.messageArgs "dear.firstname" , ["${firstName}"] />
            <br/><br/>
            <@js.message "globalcollect.payment.rejected.intro"/>
            <br/><br/>

            <#if (section.billingDetails?? && section.billingDetails.hasInfo)>
            <#assign bilDet = section.billingDetails.info>
            <table class="base alignLeft">
                <thead>
                    <tr>
                        <th class="first widthfix"><@js.message "order.details.billing.details"/></th>
                        <th class="last"></th>
                    </tr>
                </thead>
                <tbody>
                    <#list bilDet as line>
                    <#if line.name == bilDet?first.name>
                    <tr>
                    <#else>
                    <tr class="even">
                    </#if>
                        <td class="name">${line.name}</td>
                        <td class="last">${line.value}</td>
                    </tr>
                    </#list>
                 </tbody>
            </table>
            </#if>

            <div class="nofloat"></div>
            <table class="base alignLeft">
                <thead>
                    <tr>
                        <th class="first widthfix"><@js.message "order.details.shipping.details"/></th>
                        <th class="last"></th>
                    </tr>
                </thead>
                <tbody>
                <tr class="even">
                    <td class="name"><@js.message "order.details.shipping.method"/></td>
                    <td class="last">${section.orderData.shippingMethodName}</td>
                </tr>
                <#if (section.shippingDetails?? && section.shippingDetails.hasInfo)>
                    <#assign details = section.shippingDetails.info>
                <#else>
                    <#assign details = section.billingDetails.info>
                </#if>
                <#list details as line>
                    <#if line.name== details?first.name>
                    <tr>
                    <#else>
                    <tr class="even">
                    </#if>
                    <td class="name">${line.name}</td>
                    <td class="last">${line.value}</td>
                    </tr>
                </#list>
                </tbody>
            </table>


            <table class="base">
			    <thead>
				    <tr>
					    <th class="first"><@js.message "order.details.product.name"/></th>
                        <#if section.displaySKU>
                            <th><@js.message "order.details.sku"/></th>
                        </#if>
                        <th><@js.message "order.details.price"/></th>
						<th><@js.message "order.details.quantity"/></th>
						<th class="last"><@js.message "order.details.subtotal"/></th>
                    </tr>
				</thead>
				<tbody>
                <#if section.itemList??>
                        <#list section.itemList as item>
                            <#if item.SKU??>
                                <#assign sku = item.SKU>
                            <#else>
                                <#assign sku = "">
                            </#if>
                            <#if item.options??>
                            <#assign rowspan = item.options?size + 1>
                            <tr>
						        <td class="name">${item.name}</td>
                                <#if section.displaySKU>
                                    <td class="id" rowspan="${rowspan}"><input type="hidden" value="0"/>${sku}<#list item.options as option><br/><#if option.sku??>${option.sku}</#if></#list></td>
                                </#if>
                                <td class="price">${item.price}</td>
						        <td class="qty" rowspan="${rowspan}">${item.quantity}</td>
						        <td class="price last" rowspan="${rowspan}">${item.subtotal}</td>
					        </tr>
                                <#list item.options as option>
                                    <tr class="nameOption">
						                <td class="nameOption">${option.name}: <b>${option.value}</b></td>
						                <td class="price">${option.price}</td>
					                </tr>
                                </#list>
                            <#else/>
                                <tr>
                                    <td class="name">${item.name}</td>
                                    <#if section.displaySKU>
                                        <td class="id"><input type="hidden" value="0"/>${sku}</td>
                                    </#if>
                                    <td class="price">${item.price}</td>
                                    <td class="qty">${item.quantity}</td>
                                    <td class="price last">${item.subtotal}</td>
					            </tr>
                            </#if>

                        </#list>
                </#if>
                </tbody>
            </table>
            <#if section.shoppingCartData??>
            <table class="totalBlock">
			    <tbody>
                    <tr>
						<td><@js.message "order.details.product.subtotal"/></td>
						<td class="price last">${section.shoppingCartData.subtotal}</td>
					</tr>
					<tr>
						<td><@js.message "order.details.shipping"/></td>
						<td class="price last">${section.shoppingCartData.shippingTotal}</td>
					</tr>
                    <#if section.shoppingCartData.discount??>
                        <tr>
                            <td>${section.shoppingCartData.discount.name}</td>
                            <td class="price last">- ${section.shoppingCartData.discount.price}</td>
                        </tr>
                    </#if>
                            <#if section.shoppingCartData.oldTaxList??>
                                <#list section.shoppingCartData.oldTaxList as tax>
                                    <tr>
                                        <td>${tax.name}</td>
                                        <td class="price last">${tax.cost}</td>
                                    </tr>
                                </#list>
                                <tr>
                                    <td><@js.message "order.details.total"/></td>
                                    <td class="price last"><#if section.shoppingCartData.total??>${section.shoppingCartData.total}</#if></td>
                                </tr>
                            <#else>
                                <#if section.shoppingCartData.taxIncluded>
                                	 <#if section.shoppingCartData.flatTaxList??>
                                     <#list section.shoppingCartData.flatTaxList as tax>
                                        <tr>
                                            <td>${tax.name}</td>
                                            <td class="price last">${tax.cost}</td>
                                        </tr>
                                     </#list>
                                     </#if>
                                     <tr>
                                        <td><@js.message "order.details.total"/></td>
                                        <td class="price last"><#if section.shoppingCartData.total??>${section.shoppingCartData.total}</#if></td>
                                     </tr>
                                <#else>
                                	<#if section.shoppingCartData.percTaxList??>
                                    <#list section.shoppingCartData.percTaxList as tax>
                                        <tr>
                                            <td>${tax.name}</td>
                                            <td class="price last">${tax.cost}</td>
                                        </tr>
                                    </#list>
                                    </#if>
                                    <#if section.shoppingCartData.flatTaxList??>
                                    <#list section.shoppingCartData.flatTaxList as tax>
                                        <tr>
                                            <td>${tax.name}</td>
                                            <td class="price last">${tax.cost}</td>
                                        </tr>
                                    </#list>
                                    </#if>
                                    <tr>
                                        <td><@js.message "order.details.total"/></td>
                                        <td class="price last"><#if section.shoppingCartData.total??>${section.shoppingCartData.total}</#if></td>
                                    </tr>
                                </#if>
                            </#if>
				</tbody>
            </table>
            </#if>
            <div class="nofloat"></div>
        </div>
<#if section.footer??>
	${section.footer}
</#if>
</body>
</html>
