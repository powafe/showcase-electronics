<pre>
<@js.messageArgs "dear.fullname" , ["${fullName.value}"] />

<@js.messageArgs "confirm.registration.thank.you" , ["${shop.name}"] />


<@js.message "confirm.registration.please.click"/>


<a href="${customer.loginLink}">${customer.loginLink}</a>

<@js.message "confirm.registration.link.doesnt.open" />

<@js.messageArgs "kind.regards" , ["${shop.name}"] />

</pre>
