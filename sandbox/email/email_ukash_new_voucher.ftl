<pre>
<@js.messageArgs "dear.fullname" , ["${fullName}"] />
<@js.messageArgs "ukash.new_voucher.update" , ["${orderNumber}", "${shopName}"] />

<@js.messageArgs "ukash.new_voucher.refund_total" , ["${currency}", "${refundTotal}"] />

<@js.messageArgs "ukash.new_voucher.voucher_info", ["${currency} ${refundTotal}", "${voucherNumber}", "${voucherExpirationDate?string('yyyy-MM-dd')}"] />

<@js.message "ukash.new_voucher.queries"/> <a href="www.ukash.com/spend">www.ukash.com/spend</a>

<@js.messageArgs "ukash.new_voucher.thankyou" , ["${shopName}"] />
<@js.messageArgs "kind.regards" , ["${shopName}"] />
</pre>