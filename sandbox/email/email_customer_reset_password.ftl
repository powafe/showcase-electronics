<pre>
<@js.messageArgs "dear.fullname" , ["${fullName.value}"] />

<@js.messageArgs "reset.password.please.find" , ["${oneTimeLink?replace('engine/shop/engine/shop', 'engine/shop')}"] />

<@js.messageArgs "kind.regards" , ["${shop.name}"] />
</pre>
