<#assign version="1.0"/>

<html>
<body id="shop" bgcolor="#ffffff">

<div class="cont">

<#if section.header??>
${section.header}
</#if>

<#if section.orderData??>
    <br/> <@js.messageArgs "order.tracking.status" , [" ${section.orderData.id}", "${section.orderData.status}"] />
    <br/><br/>
</#if>
</div>
<#if section.footer??>
${section.footer}
</#if>
</body>
</html>
<!-- END ORDER DETAILS -->
<!-- ========================================================================================================================= -->
