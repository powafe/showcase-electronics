var customizedMessages = {
    "requiredCardNumber" : "Anna kortin numero.",
    "requiredExpireMonth" : "Anna Vanhenemispvm.",
    "requiredExpireYear" : "Anna Vanhenemispvm.",
    "requiredCreditCardType": "Ilmoitathan Card Type.",
    "requiredSecurityCode": "Sy�t� turvakoodi.",

    "maxLenCardNumber": "Luottokortin numero ei kelpaa.",
    "maxlenSecurityCode": "Turvakoodi on 3 merkki� pitk� ja voi sis�lt�� vain numeroita.",

    "minLenSecurityCode": "Turvakoodi on 3 merkki� pitk� ja voi sis�lt�� vain numeroita.",

    "invalidCardNumber": "Kortin numero ei kelpaa. Vain numerot sallittu, poista kaikki v�lily�nnit, jos l�sn�.",
    "invalidSecurityCode": "Turvakoodin tulee olla 3 merkki� pitk� ja se voi sis�lt�� vain numeroita.",

    "maxLenPassword": "Salasanan pituus ei saa ylitt�� 32 merkki�. Lue <a href=\"javascript:showPasswordPolicyPopup()\">\"Salasanan vahvuusk�yt�nt�\"</a> saadaksesi lis�tietoja.",
    "minLenPassword": "Salasanan tulee olla v�hint��n 7 merkki� pitk�. Lue <a href=\"javascript:showPasswordPolicyPopup()\">\"Salasanan vahvuusk�yt�nt�\"</a> saadaksesi lis�tietoja.",
    "ivalidPassword": "Salasana voi sis�lt�� vain merkkej�: a-z, A-Z, 0-9 ja alaviiva. Lue <a href=\"javascript:showPasswordPolicyPopup()\">\Salasanan vahvuusk�yt�nt�\"</a> saadaksesi lis�tietoja.",

    "maxLenSsv": "Hakumerkkijonon pituus ei saa ylitt�� 255 merkki�.",
    "minLenSsv": "Hakumerkkijonon tulee olla v�hint��n 2 merkki� pitk�.",
    "invalidSsv": "Hakumerkkijono sis�lt�� kiellettyj� merkkej�.",

    "requiredPromoCode": "Sy�t� tarjouskoodi.",
    "maxLenPromoCode": "Tarjouskoodin pituus ei saa ylitt�� 32 merkki�.",
    "invalidPromoCode": "Tarjouskoodi on virheellinen.",

    "maxLenCardHolder": "Kortinhaltijan nimen pituus ei saa ylitt�� 255 merkki�.",
    "invalidCardHolder": "Kortinhaltijan nimi on virheellinen.",

    "isRequired":"{0} on pakollinen .",
    "shouldNotExceed":"{0} ei saa ylitt�� {1} merkki�.",
    "shouldBeAtLeast":"{0} tulee olla v�hint��n {1} merkki� pitk�.",
    "isInvalid":"{0} on virheellinen.",
    "doNotMatch": "{0} ja {1} eiv�t t�sm��.",
    "pleaseEnter": "Sy�t� '{0}'.",
    "containsIllegalCharacters": "{0}' sis�lt�� laittomia merkkej�. Tarkasta ja yrit� uudestaan."

};

function getMessageByKey(key, args)
{
    message = customizedMessages[key];
    return getMessage(message, args);
}


function getMessage(message, values)
{
    if (typeof(values) !== 'undefined')
    {

        for (var i = 0; i < values.length; i++)
        {
            var holder = '{' + i + '}';
            var value = values[i];

            message = message.replace(holder, value);
        }
    }
    return message;
}
