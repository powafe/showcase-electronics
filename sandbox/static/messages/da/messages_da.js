var customizedMessages = {
    "requiredCardNumber" : "Indtast venligst kortnummer.",
    "requiredExpireMonth" : "Indtast venligst udl�bsdato.",
    "requiredExpireYear" : "Indtast venligst udl�bsdato.",
    "requiredCreditCardType": "Angiv korttype.",
    "requiredSecurityCode": "Indtast venligst sikkerhedskode.",

    "maxLenCardNumber": "Ugyldigt kreditkortnummer.",
    "maxlenSecurityCode": "Sikkerhedskode skal v�re 3 tegn og kan kun indeholde cifre.",

    "minLenSecurityCode": "Sikkerhedskode skal v�re 3 tegn og kan kun indeholde cifre.",

    "invalidCardNumber": "Kortnummer er ugyldigt. Kun cifre er tilladt, fjern venligst mellemrum, hvis disse forefindes.",
    "invalidSecurityCode": "Sikkerhedskode skal v�re 3 tegn og kan kun indeholde cifre.",

    "maxLenPassword": "Adgangskode m� ikke overskr�de 32 tegn. L�s venligst <a href=\"javascript:showPasswordPolicyPopup()\">\"Adgangskode Styrke Politik\"</a> for flere oplysninger.",
    "minLenPassword": "Adgangskode skal v�re p� mindst 7 tegn. L�s venligst <a href=\"javascript:showPasswordPolicyPopup()\">\"Adgangskode Styrke Politik\"</a> for flere oplysninger.",
    "ivalidPassword": "Adgangskode kan kun indeholde : a-z, A-Z, 0-9 og underscore. L�s venligst <a href=\"javascript:showPasswordPolicyPopup()\">\"Adgangskode Styrke Politik\"</a> for flere oplysninger.",

    "maxLenSsv": "S�gestreng m� ikke overskride 255 tegn.",
    "minLenSsv": "S�gestreng skal v�re mindst 2 tegn l�ngere.",
    "invalidSsv": "S�gestreng indeholder ikke-tilladte tegn.",

    "requiredPromoCode": "Indtast venligst en kampagnekode.",
    "maxLenPromoCode": "Kampagnekode m� ikke overskride 32 tegn.",
    "invalidPromoCode": "Kampagnekode er ugyldig.",

    "maxLenCardHolder": "Kortindehavers navn m� ikke overskride 255 tegn.",
    "invalidCardHolder": "Kortindehavers navn er ugyldigt.",

    "isRequired":"{0} er kr�vet.",
    "shouldNotExceed":"{0} m� ikke overskride {1} tegn.",
    "shouldBeAtLeast":"{0} skal v�re mindst {1} tegn .",
    "isInvalid":"{0} er ugyldigt.",
    "doNotMatch": "{0} og {1} matcher ikke.",
    "pleaseEnter": "Indtast venligst '{0}'.",
    "containsIllegalCharacters": "{0}' indeholder ikke-tilladte tegn. Kontroller venligst og pr�v igen."

};

function getMessageByKey(key, args)
{
    message = customizedMessages[key];
    return getMessage(message, args);
}


function getMessage(message, values)
{
    if (typeof(values) !== 'undefined')
    {

        for (var i = 0; i < values.length; i++)
        {
            var holder = '{' + i + '}';
            var value = values[i];

            message = message.replace(holder, value);
        }
    }
    return message;
}
