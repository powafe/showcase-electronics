var customizedMessages = {
    "requiredCardNumber" : "Voer kaartnummer in.",
    "requiredExpireMonth" : "Voer geldigheidsdatum in.",
    "requiredExpireYear" : "Voer geldigheidsdatum in.",
    "requiredCreditCardType": "Geef kaarttype op.",
    "requiredSecurityCode": "Voer veiligheidscode in.",

    "maxLenCardNumber": "Ongeldig kredietkaartnummer.",
    "maxlenSecurityCode": "De veiligheidscode moet uit 3 karakters bestaan. Enkel cijfers zijn toegestaan.",

    "minLenSecurityCode": "De veiligheidscode moet uit 3 karakters bestaan. Enkel cijfers zijn toegestaan.",

    "invalidCardNumber": "Het kaartnummer is ongeldig. Enkel cijfers toegestaan, verwijder eventuele spaties.",
    "invalidSecurityCode": "De veiligheidscode moet uit 3 karakters bestaan. Enkel cijfers zijn toegestaan.",

    "maxLenPassword": "Het wachtwoord mag niet meer dan 32 karakters bevatten. Zie <a href=\"javascript:showPasswordPolicyPopup()\">\"wachtwoordsterkte\"</a> voor meer informatie.",
    "minLenPassword": "Het wachtwoord moet minstens 7 karakters bevatten. Zie <a href=\"javascript:showPasswordPolicyPopup()\">\"wachtwoordsterkte\"</a> voor meer informatie.",
    "ivalidPassword": "Het wachtwoord mag enkel bestaan uit: a-z, A-Z, 0-9 en liggend streepje. Zie <a href=\"javascript:showPasswordPolicyPopup()\">\"wachtwoordsterkte\"</a> voor meer informatie.",

    "maxLenSsv": "De zoekopdracht mag niet meer dan 255 karakters lang zijn.",
    "minLenSsv": "De zoekopdracht moet minstens 2 karakters bevatten.",
    "invalidSsv": "De zoekopdracht bevat ongeldige karakters.",

    "requiredPromoCode": "Voer de promotiecode in.",
    "maxLenPromoCode": "De promotiecode mag niet langer zijn dan 32 karakters.",
    "invalidPromoCode": "De promotiecode is ongeldig.",

    "maxLenCardHolder": "De naam van de kaarthouder mag niet langer zijn dan 255 karakters.",
    "invalidCardHolder": "De naam van de kaarthouder is ongeldig.",

    "isRequired":"{0} is vereist.",
    "shouldNotExceed":"{0} mag niet meer dan {1} karakters bevatten.",
    "shouldBeAtLeast":"{0} moet minstens {1} karakters bevatten.",
    "isInvalid":"{0} is ongeldig.",
    "doNotMatch": "{0} en {1} komen niet overeen.",
    "pleaseEnter": "Voer '{0}' in.",
    "containsIllegalCharacters": "{0}' bevat ongeldige karakters. Controleer en probeer opnieuw."

};

function getMessageByKey(key, args)
{
    message = customizedMessages[key];
    return getMessage(message, args);
}


function getMessage(message, values)
{
    if (typeof(values) !== 'undefined')
    {

        for (var i = 0; i < values.length; i++)
        {
            var holder = '{' + i + '}';
            var value = values[i];

            message = message.replace(holder, value);
        }
    }
    return message;
}
