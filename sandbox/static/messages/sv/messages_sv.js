var customizedMessages = {
    "requiredCardNumber" : "Ange kortnummer.",
    "requiredExpireMonth" : "Ange utg�ngsdatum.",
    "requiredExpireYear" : "Ange utg�ngsdatum.",
    "requiredCreditCardType": "Ange korttyp.",
    "requiredSecurityCode": "Ange s�kerhetskod.",

    "maxLenCardNumber": "Ogiltigt kreditkortsnummer.",
    "maxlenSecurityCode": "S�kerhetskoden m�ste vara 3 tecken l�ng och f�r endast inneh�lla siffror.",

    "minLenSecurityCode": "S�kerhetskoden m�ste vara 3 tecken l�ng och f�r endast inneh�lla siffror.",

    "invalidCardNumber": "Kortnumret �r ogiltigt. Endast siffror till�ts. Ta bort eventuella blanktecken.",
    "invalidSecurityCode": "S�kerhetskoden m�ste vara 3 tecken l�ng och f�r endast inneh�lla siffror.",

    "maxLenPassword": "L�senordet f�r inte �verstiga 32 tecken. L�s <a href=\"javascript:showPasswordPolicyPopup()\">\"Starkt l�senordpolicy\"</a> f�r n�rmare information.",
    "minLenPassword": "L�senordet m�ste minst vara 7 tecken l�ngt. L�s <a href=\"javascript:showPasswordPolicyPopup()\">\"Starkt l�senordpolicy\"</a> f�r n�rmare information.",
    "ivalidPassword": "L�senordet f�r endast inneh�lla: a-z, A-Z, 0-9 och understrykning. L�s <a href=\"javascript:showPasswordPolicyPopup()\">\"Starkt l�senordpolicy\"</a> f�r n�rmare information.",

    "maxLenSsv": "S�kstr�ngen f�r inte �verstiga 255 tecken.",
    "minLenSsv": "S�kstr�ngen m�ste vara minst 2 tecken l�ng.",
    "invalidSsv": "S�kstr�ngen inneh�ller otill�tna tecken.",

    "requiredPromoCode": "Ange promokod.",
    "maxLenPromoCode": "Promokoden f�r inte �verstiga 32 tecken.",
    "invalidPromoCode": "Promokoden �r ogiltig.",

    "maxLenCardHolder": "Kortinnehavarens namn f�r inte �verstiga 255 tecken.",
    "invalidCardHolder": "Kortinnehavarens namn �r ogiltigt.",

    "isRequired":"{0} kr�vs.",
    "shouldNotExceed":"{0} b�r inte �verstiga {1} tecken.",
    "shouldBeAtLeast":"{0} b�r vara minst {1} tecken l�ng.",
    "isInvalid":"{0} �r ogiltig.",
    "doNotMatch": "{0} och {1} �verensst�mmer inte.",
    "pleaseEnter": "Ange '{0}'.",
    "containsIllegalCharacters": "'{0}' inneh�ller ogiltiga tecken. Kontrollera och f�rs�k igen."

};

function getMessageByKey(key, args)
{
    message = customizedMessages[key];
    return getMessage(message, args);
}


function getMessage(message, values)
{
    if (typeof(values) !== 'undefined')
    {

        for (var i = 0; i < values.length; i++)
        {
            var holder = '{' + i + '}';
            var value = values[i];

            message = message.replace(holder, value);
        }
    }
    return message;
}
