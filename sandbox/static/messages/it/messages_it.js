var customizedMessages = {
    "requiredCardNumber" : "Inserire il numero di carta.",
    "requiredExpireMonth" : "Inserire data di scadenza.",
    "requiredExpireYear" : "Inserire data di scadenza.",
    "requiredCreditCardType": "Si prega di specificare il tipo di carta.",
    "requiredSecurityCode": "Inserire il codice di sicurezza.",

    "maxLenCardNumber": "Numero della carta di credito non valido.",
    "maxlenSecurityCode": "Il codice di sicurezza deve essere lungo 3 caratteri e pu� contenere solo cifre.",

    "minLenSecurityCode": "Il codice di sicurezza deve essere lungo 3 caratteri e pu� contenere solo cifre.",

    "invalidCardNumber": "Il numero della carta non � valido. Si possono inserire solo cifre, rimuovere tutti gli spazi se presenti.",
    "invalidSecurityCode": "Il codice di sicurezza deve essere lungo 3 caratteri e pu� contenere solo cifre.",

    "maxLenPassword": "La password non deve superare i 32 caratteri. Si prega di leggere <a href=\"javascript:showPasswordPolicyPopup()\"> \"Sicurezza della password \" </ a> per maggiori dettagli.",
    "minLenPassword": "La password deve essere lunga almeno 7 caratteri. Si prega di leggere <a href=\"javascript:showPasswordPolicyPopup()\">\ Sicurezza della password \" </ a> per maggiori dettagli.",
    "ivalidPassword": "La password pu� contenere solo: az, A-Z, 0-9 e underscore. Si prega di leggere <a href=\"javascript:showPasswordPolicyPopup()\"> \"Sicurezza della password \" </ a> per maggiori dettagli.",

    "maxLenSsv": "La stringa di ricerca non deve superare i 255 caratteri.",
    "minLenSsv": "La stringa di ricerca deve essere lunga almeno 2 caratteri.",
    "invalidSsv": "La stringa di ricerca contiene caratteri non consentiti.",

    "requiredPromoCode": "Inserire il Codice Promo.",
    "maxLenPromoCode": "Il Codice Promo non deve superare i 32 caratteri.",
    "invalidPromoCode": "Il Codice Promo non � valido.",

    "maxLenCardHolder": "Il nome del titolare della carta non deve superare i 255 caratteri.",
    "invalidCardHolder": "Il nome del titolare della carta non � valido.",

    "isRequired":"{0} � richiesto.",
    "shouldNotExceed":"{0} non deve superare i {1} caratteri.",
    "shouldBeAtLeast":"{0} deve essere di almeno {1} caratteri.",
    "isInvalid":"{0} non � valido.",
    "doNotMatch": "{0} e {1} non corrispondono.",
    "pleaseEnter": "Si prega di inserire '{0}'.",
    "containsIllegalCharacters": "'{0}' contiene caratteri non validi. Si prega di verificare e riprovare."

};

function getMessageByKey(key, args)
{
    message = customizedMessages[key];
    return getMessage(message, args);
}


function getMessage(message, values)
{
    if (typeof(values) !== 'undefined')
    {

        for (var i = 0; i < values.length; i++)
        {
            var holder = '{' + i + '}';
            var value = values[i];

            message = message.replace(holder, value);
        }
    }
    return message;
}
