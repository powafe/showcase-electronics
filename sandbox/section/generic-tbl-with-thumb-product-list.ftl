<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->

<#include "product-list.lib.ftl" />

<#if section.products??>
    <@showPaging style = "pTop"/>

    <div class="productListTblSection">
        <div class="block">
            <div class="corner1">
                <div class="corner2">
                    <div class="lineTop"><h2></h2></div>
                </div>
            </div>
            <div class="cont">
                <span id="product_error">
                    <#if section.error??>
                        <p class="error">
                            ${section.error}
                        </p>
                    </#if>
                </span>
                <table class="base" width="100%">
                    <thead>
                        <tr>
                            <th class="first" style="width:100px;"></th>
                            <th class="name alignLeft"><@js.message "productName"/></th>
                            <#if section.products?first.displaySKU>
                                <th class="sku alignLeft"><@js.message "sku"/></th>
                            </#if>

                            <#if section.showPrice>
                                <th class="alignRight"><@js.message "price"/></th>
                            </#if>
                            <#if section.products?first.displayStockInfo>
                                <th class="qty alignRight"><@js.message "quantity"/></th>
                            </#if>
                            <th class="last"></th>
                        </tr>
                    </thead>
                    <tbody class="cont">
                        <#assign i = 1/>
                        <#list section.products as product>
                            <#if (i % 2 == 0)>
                                <#assign className = "even"/>
                            <#else>
                                <#assign className = ""/>
                            </#if>

                            <tr class="${className}">
                                <td class="img">
                                    <span><a href="${product.link}"><img alt="${product.name} - ${product.SKU}" title="${product.name} - ${product.SKU}" src="${product.imageLink}"/></a></span>
                                    <#-- Do not change the order of these two img tags as the order of the img elements matters -->
                                    <#if section.showAlternativeImage && product.imageAlternativeLink??>
                                        <span class="skip altImage"><a href="${product.link}"><img alt="${product.name} - ${product.SKU}" title="${product.name} - ${product.SKU}" src="${product.imageAlternativeLink}"/></a></span>
                                    </#if>
                                </td>
                                <td class="name"><a href="${product.link}">${product.name}</a></td>

                                <#if product.displaySKU>
                                    <td class="sku">${product.SKU}</td>
                                </#if>

                                <#if section.showPrice>
                                    <td class="price">
                                        <#if product.promoted>
                                            <div class="priceNow">
                                                <span>${product.bestPrice}</span>
                                            </div>
                                            <div class="priceWas">
                                                <span>${product.oldPrice}</span>
                                            </div>
                                        <#else>
                                            <div class="priceNow">
                                                <span>${product.price}</span>
                                            </div>
                                        </#if>
                                    </td>
                                </#if>
                                <#if product.displayStockInfo>
                                    <td class="qty">${product.quantityValue}</td>
                                </#if>
                                <td class="last">
                                    <#if product.canBeBought>
                                        <@buyButton product>
                                            <#if product.buyButtonEnabled>
                                                <input class="smallBtn" type="submit" value="<@js.message "buy"/>"/>
                                            <#else>
                                                <input class="smallBtn opacity" disabled="true" type="submit" value="<@js.message "buy"/>"/>
                                            </#if>
                                        </@buyButton>
                                    </#if>
                                </td>
                            </tr>

                            <#assign i = i+1/>
                        </#list>
                    </tbody>
                </table>
            </div>
            <#--Alternative product image script -->
            <script type="text/javascript">
              $('div.cont td.img span:first-child').hover(
               function () {
                // if element has alt image
                if ($(this).siblings('.altImage').length != 0) {
                    $(this).hide();
                    $(this).siblings('.altImage').show();
                }
               },
               function () {

               }
             );

             $('div.cont td.img span:last-child').hover(
               function () {

               },
               function () {
                // if element has alt image
                if ($(this).prev().length != 0) {
                    $(this).prev().show();
                    $(this).hide();
                }
               }
             );
            </script>
            <div class="corner4">
                <div class="corner3">
                    <div class="lineBtm"></div>
                </div>
            </div>
        </div>
        <div class="nofloat"></div>
    </div>
    <@showPaging style="pBtm"/>
<#else>
    <#if section.search>
        <span id="no_products_found">
            <p class="error">
                <#if section.error??>
                   ${section.error}
                <#else>
                    <@js.message "product-list_noProductsFound"/>
                </#if>
            </p>
        </span>
    </#if>
</#if>