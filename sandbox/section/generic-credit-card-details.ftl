<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<script type="text/javascript"> <!--
function show3DSPaymentInProgress()
{
    $('#div3DSPaymentProcessingProgress').css('display', '');
    return true;
}

function handle3DsSubmit(form)
{
    if (validateForm(form, Validators.CreditCardDetails, document.getElementById('cc_error')))
    {
        return show3DSPaymentInProgress();
    }
    return false;
}

function handleGeneralSubmit(form)
{
    if (validateForm(form, Validators.CreditCardDetails, document.getElementById('cc_error')))
    {
        return showPaymentInProgress();
    }
    return false;
}
// --> </script>

<div id="div3DSPaymentProcessingProgress" style="display: none">
    <div class="hidden">
    </div>
    <div class="progressBar">
        <table width="100%" border="0">
            <tr><td>
                <div class="width400">
                <span class="text">
                <br /><@js.message "credit-card-details_warningText"/><br/>
                </span>
                    <img src="${common.resourcePath}/images/ajax-loader.gif" alt="" />
                <span class="text">
                    <br /><@js.message "credit-card-details_pleaseWait"/><br /><br />
                </span>
                </div></td></tr>
        </table>
    </div>
</div>

<div class="creditCardDetailsSection">
<div class="block">
    <div class="corner1"><div class="corner2"><div class="lineTop">
    <#--<h2></h2>-->
    </div></div></div>
    <div class="cont">
    <#-- Billing Details -->
    <#if (section.billingDetails?? && section.billingDetails.hasInfo)>
        <#assign bilDet = section.billingDetails.info>
        <div class="wrapperBillingDet">
            <div class="billingDet">
                <table class="base alignLeft">
                    <thead>
                    <tr>
                        <th class="first widthfix"><@js.message "billingDetails"/></th>
                        <th class="last"><a href="${section.editBillingDetailsLink}"><@js.message "credit-card-details_editBillingDetails"/></a></th>
                    </tr>
                    </thead>
                    <tbody>
                        <#assign i = 1/>
                        <#list bilDet as line>
                            <#assign className = "${line.name?replace(' ', '')}"/>
                            <#assign className = "${className?replace('/', '')}"/>
                            <#assign className = "${className?uncap_first}"/>
                            <#assign isEven = (i % 2 == 0)/>
                            <#if isEven>
                            <tr class="${className} even">
                            <#else/>
                            <tr class="${className}">
                            </#if>
                            <td class="name"><span>${line.name}</span></td>
                            <td class="last"><span>${line.value}</span></td>
                        </tr>
                            <#assign i = i + 1/>
                        </#list>
                    </tbody>
                </table>
            </div>
        </div>
    </#if>
    <#-- End Billing Details -->
        <span id="cc_error">
        <#if section.error??>
            <p class="error">
            ${section.error}
            </p>
        </#if>
        </span>
    <#-- Credit Card Details -->
    <#if (section.shopEnabledFor3DS?? && section.shopEnabledFor3DS)>
    <form action="${section.submitLink}" method="POST" onsubmit="return handle3DsSubmit(this)">
    <#else>
    <form action="${section.submitLink}" method="POST" onsubmit="return handleGeneralSubmit(this)">
    </#if>
        <div class="wrapperCreditCardDet">
            <table class="base alignLeft">
                <thead>
                <tr>
                    <th class="first widthfix"><@js.message "credit-card-details_cardDetails"/></th>
                    <th class="last"></th>
                </tr>
                </thead>
                <tbody>
                <#-- Standard Dropdown replacement (Radios)-->
                <#if section.cardTypeSelectionRadio>
                <tr class="cardListRadioType">
                    <td colspan="2">


                        <#if section.creditCardTypeMap??>
                            <#assign types = section.creditCardTypeMap>
                            <#assign CardListCounter=0>
                            <#list types?keys as key>
                            <div class="payment${CardListCounter} ${types[key]}">
                            
                            <#if section.selCardType == ''>
                                           
                            <#if CardListCounter == 0>
                            <input id="payment${CardListCounter}" class="radio" type="radio" name="creditCardType" value="${key}" checked/>
                            <#else>
                            <input id="payment${CardListCounter}" class="radio" type="radio" name="creditCardType" value="${key}" />
                            </#if>

  							<#else>
                                 
                            <#if key==section.selCardType>
                            <input id="payment${CardListCounter}" class="radio" type="radio" name="creditCardType" value="${key}" checked/>
                            <#else>        
							<input id="payment${CardListCounter}" class="radio" type="radio" name="creditCardType" value="${key}" />
							</#if>
                            
                      		</#if>
 
                                    <label class="text" for="payment${CardListCounter}"><img src="${common.resourcePath}/images/card-brand-${key?lower_case}.png" /><span class="text">${types[key]}</span></label>
                                </div>
                                <#assign CardListCounter=CardListCounter+1>
                            </#list>
                        </#if>
                    </td>
                </tr>
                </#if>
                <#--End Standard Dropdown replacement (Radios)-->
                <tr class="total">
                    <td class="name"><span><@js.message "total"/></span></td>
                    <td class="last"><span>${section.total}</span></td>
                </tr>
                <#-- Standard Dropdown replacement (Dropdown)-->
                <#if section.cardTypeSelectionRadio><#else>
                <tr class="creditCardType even">
                    <td class="name"><span><@js.message "credit-cart-details_creditCardType"/></span></td>
                    <td class="last requiredParameter"><span>
                                <select class="card_type" name="creditCardType" autocomplete="off">
                                    <#if section.creditCardTypeMap??>
                                        <#assign types = section.creditCardTypeMap>
                                        <#list types?keys as key>
                                            <#if key==section.selCardType>
                                                <option value="${key}" selected="selected">${types[key]}</option>
                                            <#else>
                                                <option value="${key}">${types[key]}</option>
                                            </#if>
                                        </#list>
                                    </#if>
                                </select></span>
                    </td>
                </tr>
                </#if>
                <#-- Standard Dropdown replacement (Dropdown)-->

                <#if section.needCardholder>
                <tr class="cardHolder">
                    <td class="name"><span><@js.message "credit-cart-details_cardHolder"/></span></td>
                    <td class="last requiredParameter"><span><label style="display:none"><@js.message "credit-cart-details_cardHolder"/></label><input class="textInput" name="cardHolder" value="${section.cardHolder}" autocomplete="off"/></span></td>
                </tr>
                </#if>
                <tr class="cardNumber">
                    <td class="name"><span><@js.message "credit-cart-details_cardNumber"/></span></td>
                    <td class="last requiredParameter"><span><label style="display:none"><@js.message "credit-cart-details_cardNumber"/></label><input class="textInput" name="cardNumber" value="${section.cardNumber}" autocomplete="off"/></span></td>
                </tr>
                <tr class="expiryDate even">
                    <td class="name"><span><@js.message "credit-cart-details_expiryDate"/></span></td>
                    <td class="last requiredParameter"><div class="date">
                        <label><span><@js.message "credit-cart-details_month"/>:</span><select class="month" name="expireMonth" autocomplete="off">
                            <option value=""> </option>
                        <#list section.monthList as month>
                            <#if month == section.selExpMonth>
                                <option value="${month}" selected="selected">${month}</option>
                            <#else>
                                <option value="${month}">${month}</option>
                            </#if>
                        </#list>
                        </select></label>
                        <label class="floatRight"><span><@js.message "credit-cart-details_year"/>:</span><select class="year" name="expireYear" autocomplete="off">
                            <option value=""> </option>
                        <#list section.yearList as year>
                            <#if year== section.selExpYear>
                                <option value="${year}" selected="selected">${year}</option>
                            <#else>
                                <option value="${year}">${year}</option>
                            </#if>
                        </#list>
                        </select></label>
                    </div></td>
                </tr>
                <tr class="securityCode">
                    <td class="name"><span><@js.message "credit-cart-details_securityCode"/></span></td>
                    <td class="last requiredParameter"><span><label style="display:none"><@js.message "credit-cart-details_securityCode"/></label><input type="password" class="textInput" name="securityCode" value="${section.securityCode}" autocomplete="off"/></span></td>
                </tr>
                <#-- Hide elements for ePDQ-->
                <#if section.cardTypeSelectionRadio>
                <#else>
                <tr class="issueNumber even">
                    <td class="name"><span><@js.message "credit-cart-details_issueNumber"/></span></td>
                    <#assign value = section.selIssueNumber>
                    <td class="last"><span><label style="display:none"><@js.message "credit-cart-details_issueNumber"/></label><input class="textInput" name="issueNumber" value="${value}" autocomplete="off"/></span></td>
                </tr>
                <tr class="cardStartDate">
                    <td class="name"><span><@js.message "credit-cart-details_cardStartDate"/></span></td>
                    <td class="last"><div class="date">
                        <label><span><@js.message "credit-cart-details_month"/>:</span><select class="month" name="startMonth" autocomplete="off">
                            <option value=""> </option>
                            <#list section.monthList as month>
                                <#if month==section.selStartMonth>
                                    <option value="${month}" selected="selected">${month}</option>
                                <#else>
                                    <option value="${month}">${month}</option>
                                </#if>
                            </#list>
                        </select></label>
                        <label class="floatRight"><span><@js.message "credit-cart-details_year"/>:</span><select class="year" name="startYear" autocomplete="off">
                            <option value=""> </option>
                            <#list section.startYearList as year>
                                <#if year == section.selStartYear>
                                    <option selected="selected" value="${year}">${year}</option>
                                <#else>
                                    <option value="${year}">${year}</option>
                                </#if>
                            </#list>
                        </select></label></div>
                    </td>
                </tr>
                </#if>
                <#--End Hide elements for ePDQ-->
                </tbody>
            </table>
        </div>
    </div>
<#-- End Credit Card Details -->
    <input type="hidden" name="prevLink" value="${section.currentLink}"/>
    <input type="hidden" name="<@csrf.name/>" value="<@csrf.value/>"/>
    <input class="middleBtn rightBtn" type="submit" value="<@js.message "credit-cart-details_placeOrder"/>" />
    </form>
    <form action="${section.backLink}" enctype="text/plain">
        <input class="middleBtn leftBtn" type="submit" value="<@js.message "back"/>" />
    </form>
    <div class="nofloat"></div>
</div>
<div class="corner4"><div class="corner3"><div class="lineBtm"></div></div></div>
</div>
<div class="nofloat"></div>
</div>
