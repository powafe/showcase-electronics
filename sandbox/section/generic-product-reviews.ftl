<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->

<div id="BVRRContainer"></div>
<script type="text/javascript">
    $BV.ui("rr", "show_reviews", {
        productId: "${section.product.SKU}"
    });
</script>
