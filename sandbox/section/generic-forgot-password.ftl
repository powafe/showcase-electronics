<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<div class="accountSection">
<div class="form">
    <div class="block">
        <div class="corner1">
            <div class="corner2">
                <div class="lineTop"></div>
            </div>
        </div>
        <div class="cont">
            <span id="forgot_password_error">
                <#if section.error??>
                <p class="error">
                   ${section.error}
                </p>
                </#if>
            </span>
            <form action="${section.submitLink}" method="POST" onsubmit="return validateForm(this, Validators.CustomerRemindPassword, document.getElementById('forgot_password_error'));">
                <div class="email">
                    <label class="requiredParameter"><@js.message "email"/></label><input class="textInput" <#if section.email??>value="${section.email}"</#if> name="email" type="text"/>
                </div>
                <input type="hidden" name="<@csrf.name/>" value="<@csrf.value/>"/>
                <input class="middleBtn rightBtn" value="<@js.message "forgot-password_send"/>" type="submit"/>
                <div class="nofloat"></div>
            </form>
        </div>
        <div class="corner4">
            <div class="corner3">
                <div class="lineBtm"></div>
            </div>
        </div>
    </div>
</div>
</div>