<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<#if section.brands??>
<#assign brandsCount = section.brands?size/>
<#if ( brandsCount > 0)>
<div class="panel panel-default">
    <div class="brandsMenu section-refine-search">
        <div class="component component-brand-menu">
            <div class="panel-heading">
                <h4 class="panel-title"><#if section.title?? && section.title != ""><a data-toggle="collapse" class="accordion-toggle" data-parent="#left-accordion" href="#collapseLeftTwo">${section.title}</a><#else/>&nbsp;</#if></h4>
            </div>
            <div id="collapseLeftTwo" class="panel-collapse collapse in">
                <div class="panel-body">
                    <ul>
                        <#assign i = 0/>
                        <#assign className = ""/>
                        <#list section.brands as brand>
                            <#if (i == brandsCount - 1)>
                                <#assign className = "last" />
                            </#if>
                            <li class="${className}"><input type="checkbox"><a href="${brand.link}">${brand.name}</a></li>
                            <#assign i = i + 1 />
                        </#list>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
</#if>
</#if>
<script>
$(document).ready(accordionResize);
$(window).on('resize', accordionResize);
function accordionResize() {
    var leftAccordionPanels = $('#left-accordion .panel-collapse'),
        leftContainer = $('.leftContainer');
    if($(window).width() < 1200) {
        leftAccordionPanels.removeClass('in');
        $('#left-accordion .accordion-toggle').addClass('collapsed');        
        leftContainer.css('padding', '0');
    } else {
        leftAccordionPanels.addClass('in');
        $('#left-accordion .accordion-toggle').removeClass('collapsed');
        $('#left-accordion .panel-collapse').css('height', 'auto'); 
        leftContainer.css('padding', '0px 15px');
    }
}
</script>