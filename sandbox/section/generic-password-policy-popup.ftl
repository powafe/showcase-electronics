<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<!-- Password Policy Popup Content -->
<div id="passwordPolicyContent" style="display: none">
<b><@js.message "password-policy-popup_passwordConditions"/>:</b>
<ol>
    <li><@js.message "password-policy-popup_lengthMustBe"/></li>
    <li><@js.message "password-policy-popup_canCanContain"/></li>
    <li><@js.message "password-policy-popup_mustNotBeTheSameOrContain"/></li>
    <li><@js.message "password-policy-popup_mustContainLettersAndNumbers"/></li>
    <li><@js.message "password-policy-popup_mustNotBeBasedOnSimpleWord"/></li>
</ol>
<br/>
<@js.message "password-policy-popup_pleaseDoNotDisclose"/>
</div>

<script type="text/javascript">
<!--

// Function to show password policy popup
function showPasswordPolicyPopup()
{
    $("#divPasswordPolicy .text").append($("#passwordPolicyContent").html());
    $("#divPasswordPolicy").css("display","block");
}

// Register handler to close popup
$("#divPasswordPolicy *").live("click", function()    {
    $("#divPasswordPolicy").css("display","none");
    $("#divPasswordPolicy .text").html(null);
});

// --> </script>
