<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<#if totals??>
<table id="total" class="totalBlock">
    <thead>
        <tr>
            <th><@js.message "subtotal"/></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        
        <tr class="productSubtotal">
            <td><span><@js.message "productSubtotal"/></span></td>
            <td class="price last"><span>${totals.subtotal}</span></td>
        </tr>
        <tr class="shipping">
            <td><span><@js.message "shipping"/></span></td>
            <td class="price last"><span>${totals.shippingTotal}</span></td>
        </tr>

        <#if totals.discount??>
            <tr class="discount">
                <td><span>${totals.discount.name}</span></td>
                <td class="price last"><span>- ${totals.discount.price}</span></td>
            </tr>
        </#if>

        <#if totals.oldTaxList??>
            <#list totals.oldTaxList as tax>
                <#assign className = "${tax.name?replace(' ', '')}"/>
                <#assign className = "${className?uncap_first}"/>
                <tr class="${className}">
                    <td><span>${tax.name}</span></td>
                    <td class="price last"><span>${tax.cost}</span></td>
                </tr>
            </#list>
            <tr class="total">
                <td><span><@js.message "total"/></span></td>
                <td class="price last"><span>${totals.total}</span></td>
            </tr>
            <#else>
                <#if totals.taxIncluded>
                    <#list totals.flatTaxList as tax>
                         <#assign className = "${tax.name?replace(' ', '')}"/>
                         <#assign className = "${className?uncap_first}"/>
                         <tr class="${className}">
                            <td><span>${tax.name}</span></td>
                            <td class="price last"><span>${tax.cost}</span></td>
                         </tr>
                    </#list>
                    <tr class="total">
                        <td><span><@js.message "total"/></span></td>
                        <td class="price last"><span>${totals.total}</span></td>
                    </tr>
                    <#else>
                        <#list totals.percTaxList as tax>
                         <#assign className = "${tax.name?replace(' ', '')}"/>
                         <#assign className = "${className?uncap_first}"/>
                         <tr class="${className}">
                                <td><span>${tax.name}</span></td>
                                <td class="price last"><span>${tax.cost}</span></td>
                            </tr>
                        </#list>
                        <#list totals.flatTaxList as tax>
                         <#assign className = "${tax.name?replace(' ', '')}"/>
                         <#assign className = "${className?uncap_first}"/>
                         <tr class="${className}">
                                <td><span>${tax.name}</span></td>
                                <td class="price last"><span>${tax.cost}</span></td>
                            </tr>
                        </#list>
                        <tr class="total">
                            <td><span><@js.message "total"/></span></td>
                            <td class="price last"><span>${totals.total}</span></td>
                        </tr>
                </#if>
        </#if>
    </tbody>
</table>
<input type="hidden" id="isZeroValueCart" value="${totals.zeroValueCart?string}" />
</#if>