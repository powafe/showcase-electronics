<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<div class="textSection">
       <div class="block">
               <div class="corner1"><div class="corner2"><div class="lineTop"></div></div></div>
               <div class="cont">
            ${section.html}
               </div>
               <div class="corner4"><div class="corner3"><div class="lineBtm"></div></div></div>
       </div>
<div class="nofloat"></div>
</div>
