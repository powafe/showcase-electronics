<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<#if section.products??>
    <div class="featuredProductMenu">
        <div class="block floatLeft">
            <div class="corner1">
                <div class="corner2">
                    <div class="lineTop"><h3><#if section.title?? && section.title != "">${section.title}<#else>&nbsp;</#if></h3></div>
                </div>
            </div>
            <div class="cont">
                <ul>
                    <#assign productsCount = section.products?size/>
                    <#assign i = 0/>
                    <#assign className = ""/>
                    <#list section.products as product>
                        <#if (i == productsCount - 1)>
                            <#assign className = "last" />
                        </#if>
                        <li class="${className}"><a href="${product.link}"><#if section.showPrice><span class="price">${product.priceWithoutSuffix}</span></#if><span class="name">${product.name}</span></a></li>
                        <#assign i = i + 1 />
                    </#list>

                </ul>
                    <div class="nofloat"></div>
            </div>
                    <div class="corner4"><div class="corner3"><div class="lineBtm"></div></div></div>
                </div>
                <div class="nofloat"></div>
                </div>
</#if>