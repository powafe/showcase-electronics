<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<#macro showList pages level>
    <#assign styleName = ""/>
    <#if level = 2>
        <#assign styleName = "subMenu"/>
        <#else/>
        <#if level = 1>
            <#assign styleName = "subSubMenu"/>
        </#if>
    </#if>
    <#if (level != 0) && (pages??) && (pages?size > 0)>
        <ul class="${styleName}">
            <#assign pageCount = pages?size/>
            <#assign i = 0/>
            <#assign className = ""/>
            <#list pages as page>
                <#if (i == pageCount - 1)>
                    <#assign className = "last" />
                </#if>

                <#if page.link??>
                    <#if (page.children??) && (page.children?size > 0)>
                        <li class="sel ${className}">
                            <a href="${page.link}">${page.name}</a>
                            <@showList pages = page.children level = level-1/>
                        </li>
                        <#else/>
                        <li class="${className}">
                            <a href="${page.link}">${page.name}</a>
                            <@showList pages = page.children level = level-1/>
                        </li>
                    </#if>
                    <#else/>
                    <#if (page.children??) && (page.children?size > 0)>
                        <li class="sel ${className}">
                            <a>${page.name}</a>
                            <@showList pages = page.children level = level-1/>
                        </li>
                        <#else/>
                        <li class="${className}">
                            <a>${page.name}</a>
                            <@showList pages = page.children level = level-1/>
                        </li>
                    </#if>
                </#if>
                <#assign i = i+1 />
            </#list>
        </ul>
    </#if>
</#macro>


<div class="webSiteMapSection">
    <div class="block">
        <div class="corner1">
            <div class="corner2">
                <div class="lineTop"></div>
            </div>
        </div>
        <div class="cont">
            <@showList pages = section.pages level = 3/>
        </div>
        <div class="corner4">
            <div class="corner3">
                <div class="lineBtm"></div>
            </div>
        </div>
    </div>
    <div class="nofloat"></div>
</div>
