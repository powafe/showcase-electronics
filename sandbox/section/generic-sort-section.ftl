<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<#if common.productDisplayCount?? && (common.productDisplayCount > 0)>

<#assign sortChoices = section.viewSettings.sortChoices/>
<#if section.viewSettings.selectedChoiceIndex??>
    <#assign selectedChoiceIndex = section.viewSettings.selectedChoiceIndex/>
</#if>
<#assign settings = section.viewSettings.viewSettings/>
<script type="text/javascript"> <!--
function onChangeSort(el, formId)
{
    var formToSubmit = this.document.getElementById(formId);
    var inputs = formToSubmit.getElementsByTagName("input");
    var psf, psfo;

    <#list sortChoices as sortChoice>
            if(el.value == '${sortChoice_index}')
            {
                psf = '${sortChoice.productSortingFieldId}';
                psfo = '${sortChoice.productSortingOrder}';
            }
    </#list>

    for (var i = 0; i < inputs.length; i++)
    {
        if (inputs[i].name == 'psf')t
        {
            inputs[i].value = psf;
        }
        if (inputs[i].name == 'psfo')
        {
            inputs[i].value = psfo;
        }

        clearPaging(inputs[i]);
    }

    formToSubmit.submit();
}

// --></script>
<div class="col-lg-3 col-xs-6 categoryFilterSortSection">
    <div class="component component-sort-section">
        <div class="cont">
            <div class="orderBlock">
                <span class="sort">
                    <label class="visible-lg-inline">Sort:</label>
                    <select onchange="onChangeSort(this, 'filterForm')" name="psf" autocomplete="off">
                            <#list sortChoices as sortChoice>
                                <option value="${sortChoice_index}"<#if selectedChoiceIndex?? && selectedChoiceIndex == sortChoice_index> selected="selected"</#if>>${sortChoice.name}</option>
                            </#list>
                    </select>
                </span>
            </div>
        </div>
    </div>
</div>
</#if>