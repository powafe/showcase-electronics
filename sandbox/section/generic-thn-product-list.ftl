<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<#include "product-list.lib.ftl" />

<#if section.products??>
    <span id="product_warning">
        <#if section.warning??>
            <p class="warning">
                ${section.warning}
            </p>
        </#if>
    </span>
    <@showPaging style = "pTop"/>
    <div class="col-lg-9 col-xs-12 productListThbSection section-product-list">
       <div class="corner1"><div class="corner2"><div class="lineTop"></div></div></div>
        <div class="cont">
            <span id="product_error">
                <#if section.error??>
                    <p class="error">
                        ${section.error}
                    </p>
                </#if>
            </span>
            <#assign i = 1/>
            <#list section.products as product>
                <#assign isLast = (i % template.productCountPerRow == 0)/>
                <#if isLast>
                    <#assign blockclass = " n${i} last"/>
                <#else>
                    <#assign blockclass = " n${i}"/>
                </#if>
                <div class="productListThbBlock block ${blockclass} component component-product-list">
                    <#-- <div class="corner1">
                        <div class="corner2">
                            <div class="lineTop">
                                <h4><a href="${product.link}">${product.name}</a></h4>
                            </div>
                        </div>
                    </div> -->
                    <div class="cont">
                        <div class="imgBlock">
                            <a href="${product.link}">
                                <span>
                                    <img alt="${product.name} - ${product.SKU}" title="${product.name} - ${product.SKU}" src="${product.imageLink}"/></span>
                                <#-- Do not change the order of these two img tags as the order of the img elements matters -->
                                <#if section.showAlternativeImage && product.imageAlternativeLink??>
                                    <span class="skip altImage">
                                        <img alt="${product.name} - ${product.SKU}" title="${product.name} - ${product.SKU}" src="${product.imageAlternativeLink}"/></span>
                                </#if>
                            </a>
                            
                        </div>
                        <div class="descBlock">
                            <#-- <a class="moreInfo" href="${product.link}">More Info</a> -->
                            <a href="${product.link}">${product.name}</a>
                            <#if section.showPrice>
                                <div class="wrapperPriceBlock">
                                    <div class="priceBlock">
                                        <#if product.promoted>
                                            <div class="priceNow">
                                                <h4>${product.bestPrice}</h4>
                                            </div>
                                            <div class="priceWas">
                                                <h4>${product.oldPrice}</h4>
                                            </div>
                                        <#else>
                                            <div class="priceNow">
                                                <h4>${product.price}</h4>
                                            </div>
                                        </#if>
                                    </div>

                                </div>
                            </#if>
                            <#-- <#if product.displaySKU>
                                <div class="sku"><label><@js.message "sku"/>: </label><b>${product.SKU}</b></div>
                            </#if>
                            <#if product.displayStockInfo>
                                <div class="qty"><label>${product.quantityLabel}</label><b>${product.quantityValue}</b></div>
                            </#if>

                            <#if product.displayShortDescription>
                                <div class="text">${product.description}</div>
                            </#if> -->
                        </div>

                        <#-- <#if product.canBeBought>
                            <div class="buyBlock">
                                <@buyButton product>
                                    <#if product.buyButtonEnabled>
                                        <input class="smallBtn" type="submit" value="<@js.message "buy"/>"/>
                                    <#else>
                                        <input class="smallBtn opacity" disabled="true" type="submit" value="<@js.message "buy"/>" />
                                    </#if>
                                </@buyButton>
                            </div>
                        </#if> -->
                        <div class="nofloat"></div>
                    </div>

                    <div class="corner4">
                        <div class="corner3">
                            <div class="lineBtm"></div>
                        </div>
                    </div>

                </div>
                <#-- <#if isLast>
                    <div class="nofloat rowSeparator"></div>
                </#if> -->
                <#assign i = i + 1/>
            </#list>
            <#-- Alternative product image script -->
            <script type="text/javascript">
             $('div.imgBlock span:first-child').hover(
               function () {
                // if element has alt image
                if ($(this).siblings('.altImage').length != 0) {
                    $(this).hide();
                    $(this).siblings('.altImage').show();
                }
               },
               function () {

               }
             );

             $('div.imgBlock span:last-child').hover(
               function () {

               },
               function () {
                // if element has alt image
                if ($(this).prev().length != 0) {
                    $(this).prev().show();
                    $(this).hide();
                }
               }
             );
            </script>
            <div class="nofloat"></div>
        </div>
        <div class="corner4"><div class="corner3"><div class="lineBtm"></div></div></div>
    </div>
    <@showPaging style = "pBtm"/>
<#else>
    <#if section.search>
    <script type="text/javascript">
        $('.leftContainer').hide();
        $('.page-title').hide();
        $('#middleContainer').addClass('background-full background-system-message');
    </script>
    <div class="col-md-12 component component-system-message">
        <div class="system-message">
            <#if section.error??>
                <h1>SEARCH NOT FOUND</h1>
                <p>Sorry, we couldn't find any results matching your criteria. Please try another keyword search or select from the categories above.</p>
                <a href="javascript:history.back()"><button class="button button-green" type="submit">&lt; BACK</button></a>
                <a href="/index.html"><button class="button button-green" type="submit">CONTINUE SHOPPING &gt;</button></a>
            <#else>
                <h1>SEARCH NOT FOUND</h1>
                <p>Sorry, we couldn't find any results matching your criteria. Please try another keyword search or select from the categories above.</p>
                <a href="javascript:history.back()"><button class="button button-green" type="submit">&lt; BACK</button></a>
                <a href="/index.html"><button class="button button-green" type="submit">CONTINUE SHOPPING &gt;</button></a>
            </#if>
        </div>
    </div>
    </#if>
</#if>