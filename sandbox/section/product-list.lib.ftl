<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<script type="text/javascript"> <!--
function updateAndSubmit(el, formId)
{
    var formToSubmit = this.document.getElementById(formId);
    var inputs = formToSubmit.getElementsByTagName("input");
    for (var i = 0; i < inputs.length; i++)
    {
        if (inputs[i].name == el.name)
        {
            inputs[i].value = el.value;
        }
        clearPaging(inputs[i]);
    }
    formToSubmit.submit();
}
// -->
$(document).ready(function(){
    var pTop = $('.pTop').find('.pagination'),
        pBtm = $('.pBtm').find('.pagination');
    pBtm.hide();
    $(window).on('resize load', function () {
        if ($(window).width() > 768) {
            pTop.prependTo('.centerContainer');
        } else if ($(window).width() <= 768) {
            pTop.prependTo('.pTop .pagination-block');
        }
    });
})
</script>

<form id="filterForm" action="${section.submitLink}">
    <input name="prpp" type="hidden"<#if section.viewSettings.viewSettings.prpp??> value="${section.viewSettings.viewSettings.prpp}"</#if>/>
    <input name="ssv" type="hidden"<#if section.viewSettings.viewSettings.ssv??> value="${section.viewSettings.viewSettings.ssv}"</#if>/>
    <input name="ppn" type="hidden"<#if section.viewSettings.viewSettings.ppn??> value="${section.viewSettings.viewSettings.ppn}"</#if>/>
    <input name="psfo" type="hidden"<#if section.viewSettings.viewSettings.psfo??> value="${section.viewSettings.viewSettings.psfo}"</#if>/>
    <input name="psf" type="hidden"<#if section.viewSettings.viewSettings.psf??> value="${section.viewSettings.viewSettings.psf}"</#if>/>
    <#list section.viewSettings.locaytaFilters as filter>
        <input name="filter" type="hidden" value="${filter}"/>
    </#list>
</form>

<#assign showPagination = ((section.pagination??) && (section.pagination?size > 0))/>
<#if showPagination>
    <#assign pages = section.pagination.pages/>
    <#assign prev = section.pagination.prev/>
    <#assign next = section.pagination.next/>
</#if>

<#macro showPaging style>
    <div class="paginationBlock ${style}">
            <div class="pagination-block">
                <#if showPagination>
                <div class="pagination col-sm-4 col-lg-4">
                    <ul>
                        <li class="prev">
                            <a href="${prev.link}">
                                <#if prev.name?length!=0>
                                    <@js.message "product-list_prev"/>
                                </#if>
                            </a>
                        </li>
                        <#list pages as page>
                            <#if (page.name==section.viewSettings.viewSettings.ppn)>
                                <li class="sel">${page.name} &nbsp;|&nbsp;</li>
                            <#else>
                                <li><a href="${page.link}">${page.name}</a></li>
                            </#if>
                        </#list>
                        <li class="next">
                            <a href="${next.link}">
                                <#if next.name?length!=0>
                                    <@js.message "product-list_next"/>
                                </#if>
                            </a>
                        </li>
                    </ul>
                </div>
                </#if>
                <#if section.viewSettings.viewSettings.prpp??>
                <div class="component component-page-view">
                    <label class="perPage visible-lg-inline"><@js.message "product-list_productsPerPage"/>:</label>
                    <select name="prpp" onchange="updateAndSubmit(this, 'filterForm')" autocomplete="off">
                        <#list section.viewSettings.allowedPPPValues as number>
                            <#if number==section.viewSettings.viewSettings.prpp>
                                <option selected="selected" value="${number}">${number}</option>
                            <#else>
                                <option value="${number}">${number}</option>
                            </#if>
                        </#list>
                    </select>
                </div>
                </#if>
            </div>
        </div>
    </div>
</#macro>

<#macro buyButton product>
    <form id = "buyForm" action="${product.buyLink}" method="POST">
        <input name= "pid" type="hidden" value="${product.pid}" />
        <input name="${product.pid}.qty" type="hidden" value="1"/>
        <input type="hidden" name="<@csrf.name/>" value="<@csrf.value/>"/>

        <#nested />
    </form>
</#macro>