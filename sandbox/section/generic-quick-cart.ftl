<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->

<#include "/${common.templatePath}cart-js.ftl"/>
<div class="quickCartSection">
    <div class="block floatLeft">
        <div class="corner1">
            <div class="corner2">
                <div class="lineTop"><h3><@js.message "quick-cart_quickCart"/></h3></div>
            </div>
        </div>
        <div class="cont">
            <table>
                <thead>
                    <tr>
                        <th class="name"><@js.message "cart_items"/></th>
                        <th class="qty"><@js.message "quick-cart_qty"/></th>
                        <th class="dlt"></th>
                    </tr>
                </thead>
                <tbody>
                    <#if section.items??>
                        <#assign items = section.items/>

                        <#list items as item>
                            <#if item == items?last>
                                <#assign trclass = "even"/>
                                <#else/>
                                <#assign trclass = ""/>
                            </#if>

                            <tr class="${trclass}">
                                <td class="name"><a href="${item.link}">${item.name}</a><br />&nbsp;</td>
                                <td class="qty">${item.quantity}</td>
                                <td class="dlt">
                                    <button class="dlt" onclick="dlt(${item.id})"></button>
                                </td>
                            </tr>
                        </#list>
                    </#if>
                <tr class="total">
                    <td class="name"><@js.message "subtotal"/></td>
                    <td colspan="2" class="qty">${section.totalsBlock.subtotal}</td>
                </tr>
                </tbody>
            </table>
    <#if section.items??>
        <form id="qcActionForm" action="" method="POST">
            <input name="shippingMethodId" type="hidden" value="${section.selectedShippingId}"/>
            <input name="paymentMethodId" type="hidden"/>
            <input type="hidden" name="<@csrf.name/>" value="<@csrf.value/>"/>
            <div class="buttonLine">
                <input class="middleBtn leftBtn" value="<@js.message "cart_viewCart"/>" type="submit"
                       onclick="cartAction('${section.cartLink}', null, 'qcActionForm')">
                <input class="middleBtn rightBtn" value="<@js.message "cart_checkout"/>" type="submit"
                       onclick="cartAction('${section.checkoutLink}', null, 'qcActionForm')">
            </div>
            <div class="nofloat"></div>
            <#if section.expressPayEnabled>
                <#list section.expressMethods as method>
                    <div class="sep">--- Or Use ---</div>
                    <div class="nofloat"></div>
                    <div class="checkoutBtn">
                        <input src="${method.imageLink}" alt="<@js.message "quick-cart_fastCheckout"/>" type="image"
                               onclick="cartAction('${section.expressLink}', ${method.methodId}, 'qcActionForm')"/>
                    </div>
                    <div class="nofloat"></div>
                </#list>
            </#if>
        </form>
    <#else>
    <div class="buttonLine"></div>
        </#if>
        </div>
        <div class="corner4">
            <div class="corner3">
                <div class="lineBtm"></div>
            </div>
        </div>
    </div>
    <div class="nofloat"></div>
</div>