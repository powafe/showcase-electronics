<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<#if section.items??>

<form id="delForm" action="${section.deleteLink}" method="POST">
        <input name="pid" type="hidden"/>
        <input type="hidden" name="<@csrf.name/>" value="<@csrf.value/>"/>
        <input name="prevLink" type="hidden" <#if common.requestURI??>value="${common.requestURI}"</#if>/>
</form>

<script type="text/javascript"> <!--
function dlt(id)
{
     var form = this.document.getElementById('delForm');
     var element = form.pid;
     element.value = id;

     form.submit();
}

function cartAction(url, paymentMethod, formId)
{
        var form = this.document.getElementById(formId);
        form.action = url
        var shippings = document.getElementsByName('shippingMethod');
            <#if section.payEnabled>
                 if (paymentMethod != null)
                 {
                    form.paymentMethodId.value = paymentMethod;
                 }
                if (shippings != null)
                {
                    for (var i = 0; i < shippings.length; i++)
                    {
                        if (shippings[i].checked)
                        {
                            form.shippingMethodId.value = shippings[i].value;
                            break;
                        }
                    }
                }
                form.submit();
            <#else>
                alert(<@js.message "cart-js_alert"/>);
                return false;
            </#if>
}
// --> </script>
</#if>