<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<div class="paymentDetailsSection">
    <div class="block">
        <div class="corner1">
            <div class="corner2">
                <div class="lineTop">
                    <#--<h2></h2>-->
                </div>
            </div>
        </div>
        <span id="error">
            <#if section.error??>
                <p class="error">
                   ${section.error}
                </p>
            </#if>
        </span>
        
        <div style="width: 85%; height: 320px;">
            <div style="float: left;">
                <img src="${common.resourcePath}/images/card-brand-${section.cardBrandName?lower_case}.jpg" />
                ${section.cardBrandDisplayName}
            </div>
            <div style="float: right;">
                <a href="${section.backLink}"><@js.message "changePayment" /></a>
            </div>
            <div style="clear: both;" />
        </div>
        
        <#if section.showIFrame>
            <!-- CARD BRAND NAME: ${section.cardBrandName} (USED TO PICK A CSS CLASS FOR I-FRAME) -->
            <#switch section.cardBrandName>
                <#case "GLOBAL_COLLECT_DIRECT_DEBIT_ELV">
                <#case "GLOBAL_COLLECT_DIRECT_DEBIT_DOMICILIACION_BANCARIA">
                    <#assign iframeCssClass="paymentDetailsLarge" />
                    <#break>
                <#default>
                    <#assign iframeCssClass="paymentDetailsSmall" />
            </#switch>
            <iframe src="${section.pspPaymentUrl}" class="${iframeCssClass}" ></iframe>
        <#else>
		    <img src="${common.resourcePath}/images/global_collect_iframe_preview.png" />
		</#if>
		
        <div class="nofloat"></div>
        
		<form action="${section.backLink}" enctype="text/plain">
			<input class="middleBtn leftBtn floatLeft" type="submit" value="<@js.message "back"/>" />
		</form>
        <div class="nofloat"></div>
    </div>
    <div class="corner4"><div class="corner3"><div class="lineBtm"></div></div></div>
</div>
<div class="nofloat"></div>
</div>
