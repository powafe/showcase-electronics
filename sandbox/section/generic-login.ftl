<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<div class="accountSection">
<div class="form">
    <div class="block">
        <div class="corner1">
            <div class="corner2">
                <div class="lineTop"></div>
            </div>
        </div>
        <div class="cont">
            <span id="login_error">
                <#if section.error??>
                <p class="error">
                   ${section.error}
                </p>
                </#if>
            </span>
            <form method="post" action="${section.loginLink}" autocomplete="off" onsubmit="return validateForm(this, Validators.CustomerLogin, document.getElementById('login_error'));">
                <div class="username"><label class="requiredParameter"><@js.message "username"/></label><input class="textInput"
                <#if section.userName??> value="${section.userName}"</#if>
                name="userName" type="text" /></div>
                <div class="password"><label class="requiredParameter"><@js.message "password"/></label><input class="textInput" value="" name="password" type="password"/></div>
                <div class="forgotPassword"><label>&nbsp;</label><a class="link" href="${section.forgotLink}"><@js.message "login_forgotPassword"/></a><div class="nofloat"></div></div>
                <input type="hidden" name="<@csrf.name/>" value="<@csrf.value/>"/>
                <input class="middleBtn rightBtn" value="<@js.message "login_login"/>" type="submit" />
            </form>

            <input class="newCustomerBtn leftBtn" type="submit" value="<@js.message "login_registerNewCustomer"/>" onclick="javascript: window.location='${section.registerLink}'"/>
        </div>
        <div class="corner4">
            <div class="corner3">
                <div class="lineBtm"></div>
            </div>
        </div>
    </div>
</div>
</div>