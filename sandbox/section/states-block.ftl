<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<#if section.selectedCountry??>
<#if section.selectedCountry.haveStates>
<label class="requiredParameter"><@js.message "${section.selectedCountry.countyLabelKey}"/></label>
    <#else>
<label><@js.message "${section.selectedCountry.countyLabelKey}"/></label>
</#if>
    <#else>
<label><@js.message "billing-shipping-details_countryState"/></label>
</#if>
<#if section.selectedCountry?? && section.selectedCountry.states?? && section.selectedCountry.states?size!=0>
<select name="county" class="requiredParameter">
    <option value=""><@js.message "pleaseSelect"/></option>
    <#list section.selectedCountry.states as state>
        <#if section.county?? && section.county == state>
            <option selected="selected" value="${state}">${state}</option>
            <#else>
                <option value="${state}">${state}</option>
        </#if>
    </#list>
</select>
    <#else>
<input class="textInput" <#if  section.county??>value="${section.county}" </#if> type="text" name="county"/>
</#if>
