<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<div class="panel panel-default">
    <div class="qcategoriesMenu section-refine-search">
        <div class="component component-category-menu">
            <div class="panel-heading">
                <h4 class="panel-title"><#if section.title?? && section.title != ""><a data-toggle="collapse" class="accordion-toggle" data-parent="#left-accordion" href="#collapseLeftOne">${section.title}</a><#else/>&nbsp;</#if></h4>
            </div>
            <div id="collapseLeftOne" class="panel-collapse collapse in">
                <div class="panel-body">
                    <ul>
                        <#assign i = 0/>
                        <#assign pageCount = section.pages?size/>
                        <#assign className = ""/>
                        <#list section.pages as page>
                            <#if i = pageCount - 1>
                                <#assign className = "last"/>
                            </#if>
                            <#if (page.children??) && (page.children?size > 0)>
                                <li class="${className} sel">

                                    <a href="${page.link}"><input type="checkbox">${page.name}</a>


                                    <ul class="subCategory">
                                        <#assign j = 0/>
                                        <#assign childCount = page.children?size/>
                                        <#assign childClassName = ""/>
                                        <#list page.children as child>
                                            <#if j = childCount - 1>
                                                <#assign childClassName = "last"/>
                                            </#if>
                                            <li class="${childClassName}">
                                                <a href="${child.link}"><input type="checkbox">
                                                    <#if section.textMode?? && section.textMode != "false"><b></#if>
                                                        ${child.name}
                                                    <#if section.textMode?? && section.textMode != "false"></b></#if>
                                                </a>
                                            </li>
                                            <#assign j = j + 1/>
                                        </#list>
                                    </ul>
                                </li>
                                <#else/>
                                <li class="${className}">
                                    <a href="${page.link}"><input type="checkbox">${page.name}</a>
                                </li>
                            </#if>
                            <#assign i = i +1/>
                        </#list>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>