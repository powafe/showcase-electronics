<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
 <div class="page-title background-full background-page-title">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12 component component-page-title">
 				<div id="pageTitle" class="pageTitleSection">
	    			<h2><span>${section.pageName}</span></h2>
				</div>
 			</div>
 		</div>
 	</div>
</div>
<script type="text/javascript">
$(document).ready(pageResize);
$(window).on("resize", pageResize);
function pageResize() {
	if ($(window).width() > 768 & $("body").is(".pageCategory,.pageBrand")) {
		$('.page-title').removeClass('background-page-title');
		$('.page-title').addClass('background-header-woman');
		$('.page-title').css("box-shadow", "none");
	} else if ($(window).width() < 768 & $("body").is(".pageCategory,.pageBrand")) {
		$('.page-title').addClass('background-page-title');
		$('.page-title').removeClass('background-header-woman');
		$('.page-title').removeAttr('style');
	}
	if ($("body").hasClass("pageInformation")) {
		$('.page-title').removeClass('background-page-title');
		$('.page-title').addClass('background-header-bicycle');
		$('.page-title').css("box-shadow", "none");
	}
}
</script>
