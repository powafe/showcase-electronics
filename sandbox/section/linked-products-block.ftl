<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<#if linkedProducts.tabs??>
    <div class="productTabBlock">
        <ul>
            <#list linkedProducts.tabs as tab>
                <#if tab == linkedProducts.tabs?last>
                    <#assign liclass="last">
                </#if>
                <#if tab == linkedProducts.tabs?first>
                    <#assign liclass="first">
                </#if>
                <#if tab == linkedProducts.currentlySelected>
                    <#assign liclass=liclass+ " sel">
                    <li class="${liclass}">
                        <span class="l">&nbsp;</span><span class="name"><@js.message "${linkedProducts.keysForTypes[tab]}"/></span>
                    </li>
                <#else>
                    <li class="${liclass}">
                        <a onclick="switchLinkedProducts('${linkedProducts.switchLinkedProductsLink}', '${tab}')">
                            <span class="l">&nbsp;</span><span class="name"><@js.message "${linkedProducts.keysForTypes[tab]}"/></span></a>
                    </li>
                </#if>
            </#list>
        </ul>
    </div>
    <div class="relatedProductListThbSection">
        <#assign i = 1/>
        <#list linkedProducts.products as lproduct>
            <#assign isLast = (i % 3 == 0)/>

            <#if isLast>
                <#assign blockclass = " n${i} last "/>
            <#else>
                <#assign blockclass = " n${i}"/>
            </#if>
            <div class="relatedProductListThbBlock block ${blockclass}">
                <div class="corner1">
                    <div class="corner2">
                        <div class="lineTop">
                            <h4><a href="${lproduct.link}">${lproduct.name}</a></h4>
                        </div>
                    </div>
                </div>
                <div class="cont">
                    <div class="imgBlock">
                        <a href="${lproduct.link}">
                            <img alt="" title="" src="${lproduct.imageLink}"/>
                        </a>
                        <h4><a href="${lproduct.link}">${lproduct.name}</a></h4>

                        <div class="wrapperPriceBlock">
                            <div class="priceBlock">
                                <#if lproduct.promoted>
                                    <div class="priceNow">
                                        <span>${lproduct.price}</span>
                                    </div>
                                    <div class="priceWas">
                                        <span>${lproduct.oldPrice}</span>
                                    </div>
                                <#else>
                                    <div class="priceNow">
                                        <span>${lproduct.price}</span>
                                    </div>
                                </#if>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="corner4">
                    <div class="corner3">
                        <div class="lineBtm"></div>
                    </div>
                </div>
            </div>
            <#if isLast>
                <#if i==linkedProducts.products?size>
                <#else>
                    <div class="rowSeparator"></div>
                </#if>
            </#if>
            <#assign i = i + 1/>
        </#list>
        <div class="nofloat"></div>
    </div>
</#if>