<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Showcase Electronics</title>
    <link rel="shortcut icon" href="${resourcePath}/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="${resourcePath}/styles/styles.css" type="text/css"/>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
</head>
<body class="page-system-message">
<!--
HEADER
-->
<!--container-->
<div class="container">

    <!--logo-->
    <div class="row logo-search-section">
        <div class="col-md-8 col-xs-12 logo">
            <a href="/index.html" title="" data-no-instant><img alt="" src="/file/ItEixpG/aa009ee9-1606-41c1-804e-485c28dabb50.png" /></a>
        </div>

    <!--top-nav-->
    </div><!--//container -->

    <div class="row">
        <div class="col-lg-12 visible-lg nav top-nav">
            <ul class="nav-set top-nav-set pull-right">
                <li class="nav-item top-nav-item">
                    <a href="#" title="Products" class="nav-link top-nav-link">Products</a>
                    <ul class="nav-set sub-nav-set">
                        <div class="col-lg-3 component component-sub-nav">
                            <div class="sub-nav-row-1">
                                <h5>Laptops</h5>
                                <li><a href="/category/Laptops">Latest Laptops</a></li>
                                <li><a href="/category/Laptops">Ultrabooks</a></li>
                                <li><a href="/category/Laptops">Touchscreen</a></li>
                            </div>

                            <div class="sub-nav-row-2">
                                <h5>Tablets</h5>
                                <li><a href="/category/Tablets">2-in-1</a></li>
                                <li><a href="/category/Tablets">eReaders</a></li>
                                <li><a href="/category/Tablets">Accessories</a></li>
                            </div>
                        </div>
                        <div class="col-lg-3 component component-sub-nav">
                            <div class="sub-nav-row-1">
                                <h5>Televisions</h5>
                                <li><a href="/category/Televisions">Under 32"</a></li>
                                <li><a href="/category/Tablets">32" to 55"</a></li>
                                <li><a href="/category/Tablets">56" and more</a></li>
                            </div>

                            <div class="sub-nav-row-2">
                                <h5>Cameras</h5>
                                <li><a href="/category/Cameras">DSLR</a></li>
                                <li><a href="/category/Cameras">Compact Cameras</a></li>
                                <li><a href="/category/Cameras">Camcorders</a></li>
                                <li><a href="/category/Cameras">Bags & Cases</a></li>
                                <li><a href="/category/Cameras">Memory Cards</a></li>
                            </div>
                        </div>
                        <div class="col-lg-3 component component-sub-nav">
                            <div class="sub-nav-row-1">
                                <h5>Phones</h5>
                                <li><a href="/category/Phones">Smartphones</a></li>
                                <li><a href="/category/Phones">Mobile Phones</a></li>
                                <li><a href="/category/Phones">Home Phones</a></li>
                            </div>

                            <div class="sub-nav-row-2">
                                <h5>Headphones</h5>
                                <li><a href="/category/Headphones">On-ear</a></li>
                                <li><a href="/category/Headphones">In-ear</a></li>
                                <li><a href="/category/Headphones">Wireless</a></li>
                            </div>

                        </div>
                        <div class="col-lg-3 component component-sub-nav">
                            <div class="sub-nav-row-1">
                                <h5>Speakers</h5>
                                <li><a href="/category/Speakers">PC Speakers</a></li>
                                <li><a href="/category/Headphones">Portable Speakers</a></li>
                                <li><a href="/category/Headphones">Hi-Fi Systems</a></li>
                            </div>

                            <div class="sub-nav-row-2">
                                <h5>MP3 Players</h5>
                                <li><a href="/category/MP3+Players">MP3 & Multimedia Players</a></li>
                                <li><a href="/category/MP3+Players">Accesories</a></li>
                            </div>
                        </div>
                    </ul>
                </li>
                <li class="nav-item top-nav-item">
                    <a href="#" title="Deals & Gifts" class="nav-link top-nav-link">Deals & Gifts</a>
                </li>
                <li class="nav-item top-nav-item">
                    <a href="#" title="Services" class="nav-link top-nav-link">Services</a>
                </li>
                <li class="nav-item top-nav-item">
                    <a href="#" title="Contact Us" class="nav-link top-nav-link">Contact Us</a>
                </li>
            </ul>
        </div>
        <div class="col-xs-4 hidden-lg mobile-nav top-nav">
            <ul class="nav-set mobile-nav-set top-nav-set">
                <li class="nav-item mobile-nav-item top-nav-item">
                    <a href="#" title=""><i class="fa fa-bars"></i></a>
                    <div class="panel-group" id="accordion">
                        <ul class="nav-set sub-nav-set">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <li><h5 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" class="accordion-toggle collapsed" href="#collapseOne">Laptops</a></h5>
                                        <div id="collapseOne" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <a href="/category/Laptops">Latest Laptops</a>
                                                <a href="/category/Laptops">Ultrabooks</a>
                                                <a href="/category/Laptops">Touchscreen</a>
                                            </div>
                                        </div>
                                    </li>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <li><h5 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" class="accordion-toggle collapsed" href="#collapseTwo">Tablets</a></h5>
                                        <div id="collapseTwo" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <a href="/category/Tablets">2-in-1</a>
                                                <a href="/category/Tablets">eReaders</a>
                                                <a href="/category/Tablets">Accesories</a>
                                            </div>
                                        </div>
                                    </li>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <li><h5 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" class="accordion-toggle collapsed" href="#collapseThree">Televisions</a></h5>
                                        <div id="collapseThree" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <a href="/category/Televisions">Under 32"</a>
                                                <a href="/category/Televisions">32" to 55"</a>
                                                <a href="/category/Televisions">56" and more</a>
                                            </div>
                                        </div>
                                    </li>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <li><h5 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" class="accordion-toggle collapsed" href="#collapseFour">Cameras</a></h5>
                                        <div id="collapseFour" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <a href="/category/Cameras">DSLR</a>
                                                <a href="/category/Cameras">Compact Cameras</a>
                                                <a href="/category/Cameras">Camcorders</a>
                                                <a href="/category/Cameras">Bags & Cases</a>
                                                <a href="/category/Cameras">Memory Cards</a>
                                            </div>
                                        </div>
                                    </li>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <li><h5 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" class="accordion-toggle collapsed" href="#collapseFive">Phones</a></h5>
                                        <div id="collapseFive" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <a href="/category/Phones">Smartphones</a>
                                                <a href="/category/Phones">Mobile Phones</a>
                                                <a href="/category/Phones">Home Phones</a>
                                            </div>
                                        </div>
                                    </li>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <li><h5 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" class="accordion-toggle collapsed" href="#collapseSix">Headphones</a></h5>
                                        <div id="collapseSix" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <a href="/category/Headphones">On-ear</a>
                                                <a href="/category/Headphones">In-ear</a>
                                                <a href="/category/Headphones">Wireless</a>
                                            </div>
                                        </div>
                                    </li>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <li><h5 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" class="accordion-toggle collapsed" href="#collapseSeven">Speakers</a></h5>
                                        <div id="collapseSeven" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <a href="/category/Speakers">PC Speakers</a>
                                                <a href="/category/Speakers">Portable Speakers</a>
                                                <a href="/category/Speakers">Hi-Fi Systems</a>
                                            </div>
                                        </div>
                                    </li>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <li><h5 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" class="accordion-toggle collapsed" href="#collapseEight">MP3 Players</a></h5>
                                        <div id="collapseEight" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <a href="/category/MP3+Players">MP3 & Multipedia Players</a>
                                                <a href="/category/MP3+Players">Accesories</a>
                                            </div>
                                        </div>
                                    </li>
                                </div>
                            </div>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
        <div class="col-xs-8 hidden-lg cart-search-nav top-nav">
            <ul class="nav-set cart-search-set top-nav-set pull-right">
                <li class="nav-item cart-search-item top-nav-item">
                    <a href="#" title="Basket" class="nav-link top-nav-link"><img src="${resourcePath}/images/basket.png" class="icon-mobile-nav" alt="Shopping basket"></a>
                    <ul class="nav-set sub-nav-set">
                        <li class="basket-item text-center">
                            <div class="quickCartSection empty">
                                <div class="row">
                                    <div class="col-xs-12 quick-cart-header">
                                        <ul>
                                            <li class="items"><img src="/file/ItEixpG/8a7b9027-606d-46e0-ad9e-4ab4efd2b85d.png" class="icon-shopping-basket" alt="Shopping basket">&nbsp;&nbsp;&nbsp;&nbsp;ITEMS: &nbsp;<span>0</span>&nbsp;&nbsp;&nbsp;&nbsp;|</li>
                                            <li class="total">SUBTOTAL: &nbsp;<span>0.00</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="nav-item cart-search-item top-nav-item">
                    <a href="#" title="Search" class="nav-link top-nav-link"><i class="fa fa-search"></i></a>
                    <ul class="nav-set sub-nav-set">
                        <li class="search-item">
                            <div class="searchSection">
                                <div class="block col-xs-12 search-section">
                                    <form action="/search" onsubmit="return validateForm(this, Validators.SearchProducts, document.getElementById('serch_error'));">
                                        <input class="textInput" name="ssv" value="" placeholder="Search Showcase Electronics..."/>
                                        <button type="submit"><i class="fa fa-search"></i></button> 
                                    </form>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(headerResize);
$(window).on("resize", headerResize);
function headerResize() {
    var searchBox = $('.searchSection'),
        quickCartBox = $('.quickCartSection');
    if ($(window).width() < 1199) {
        quickCartBox.appendTo($('.basket-item')).show();
        searchBox.appendTo($('.search-item')).show();
    } else {
        quickCartBox.hide();
        searchBox.hide();
    }
}
</script>
<!--
CONTENT
-->
<#if body??>
    <div class="background-full background-system-message">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 component component-system-message">
                    <div class="system-message">
                        <h1>PAGE NOT FOUND</h1>
                        <p>Sorry, the page you are looking for cannot be found, please click here to return to the Showcase Electronics homepage to browse the site.</p>
                        <a href="/index.html">
                            <button class="button button-green">GO TO HOME PAGE &gt;</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</#if>
<!--
FOOTER
-->
<!--manufacturers-->
<div class="section section-footer section-manufacturers hidden-xs">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 component component-manufacturers">
                <h4>Manufacturers</h4>
            
                <div class="content content-manufacturers">
                    <ul>
                        <li>Ableton</li>
                        <li>Adam Hall</li>
                        <li>Akai</li>
                        <li>AKG</li>
                        <li>Alesis</li>
                        <li>Alien & Heath</li>
                        <li>Alto</li>
                        <li>ALVA</li>
                        <li>ART</li>
                        <li>Arturia</li>
                    </ul>
                </div>
                <div class="content content-manufacturers">
                    <ul>
                        <li>Audient</li>
                        <li>Audio Technica</li>
                        <li>Audix</li>
                        <li>Avid</li>
                        <li>Beyerdynamic</li>
                        <li>Blue</li>
                        <li>Chord</li>
                        <li>Citronic</li>
                        <li>Digidesign</li>
                        <li>ESI</li>
                    </ul>
                </div>
                <div class="content content-manufacturers">
                    <ul>
                        <li>FBT</li>
                        <li>Focusirte</li>
                        <li>Fostex</li>
                        <li>FXpansion</li>
                        <li>Genelec</li>
                        <li>IK Multimedia</li>
                        <li>JBL</li>
                        <li>Kinsman</li>
                        <li>Korg</li>
                        <li>KRK</li>
                    </ul>
                </div>
                <div class="content content-manufacturers">
                    <ul>
                        <li>LD Systems</li>
                        <li>Lexicon</li>
                        <li>M Audio</li>
                        <li>Mackie</li>
                        <li>Miditech</li>
                        <li>Neumann</li>
                        <li>Novation</li>
                        <li>Numark</li>
                        <li>Olympus</li>
                        <li>Orange</li>
                    </ul>
                </div>
                <div class="content content-manufacturers">
                    <ul>
                        <li>Pace</li>
                        <li>Peavey</li>
                        <li>Phonic</li>
                        <li>PreSonus</li>
                        <li>Prodipe</li>
                        <li>Proel</li>
                        <li>Propellerhead</li>
                        <li>Quiklok</li>
                        <li>RCF</li>
                        <li>RME</li>
                    </ul>
                </div>
                <div class="content content-manufacturers">
                    <ul>
                        <li>Rode</li>
                        <li>Samsung</li>
                        <li>sE Electronics</li>
                        <li>Sennheiser</li>
                        <li>Shure</li>
                        <li>Skytronic</li>
                        <li>Sony</li>
                        <li>Soundcraft</li>
                        <li>Spectrasonics</li>
                        <li>Stagg</li>
                    </ul>
                </div>
                <div class="content content-manufacturers">
                    <ul>
                        <li>Superlux</li>
                        <li>Tannoy</li>
                        <li>Tascam</li>
                        <li>TC Electronic</li>
                        <li>TC Helicon</li>
                        <li>Tin Pan Alley</li>
                        <li>Toontrack</li>
                        <li>Unbranded</li>
                        <li>Universal Acoustics</li>
                        <li>Universal Audio</li>
                    </ul>
                </div>
                <div class="content content-manufacturers">
                    <ul>
                        <li>Waves</li>
                        <li>Yamaha</li>
                        <li>Zoom</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!--categories-and-social-->
<div class="section section-footer section-categories-and-social">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 hidden-xs component component-categories">
                <h4>Categories</h4>

                <div class="content content-categories">
                    <ul>
                        <li><a href="/category/Laptops" title="Laptops">Laptops</a></li>
                        <li><a href="/category/Tablets" title="Tablets">Tablets</a></li>
                        <li><a href="/category/Televisions" title="Televisions">Televisions</a></li>
                        <li><a href="/category/Cameras" title="Cameras">Cameras</a></li>
                    </ul>
                </div>
                <div class="content content-categories">
                    <ul>
                        <li><a href="/category/Phones" title="Phones">Phones</a></li>
                        <li><a href="/category/Headphones" title="Headphones">Headphones</a></li>
                        <li><a href="/category/Speakers" title="Speakers">Speakers</a></li>
                        <li><a href="/category/MP3+Players" title="MP3 Players">MP3 Players</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-sm-6 col-xs-12 component component-social">
                <h4 class="hidden-xs">Follow Us On</h4>

                <div class="content content-social">
                    <ul>
                        <li class="social-icon social-icon-facebook">
                            <i class="fa fa-facebook"></i><span class="hidden-xs">Facebook</span>
                        </li>
                        <li class="social-icon social-icon-twitter">
                            <i class="fa fa-twitter"></i><span class="hidden-xs">Twitter</span>
                        </li>
                        <li class="social-icon social-icon-youtube">
                            <i class="fa fa-youtube"></i><span class="hidden-xs">Youtube</span>
                        </li>
                        <li class="social-icon social-icon-googleplus">
                            <i class="fa fa-google-plus"></i><span class="hidden-xs">Google+</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!--static-links-->
<div class="section section-footer section-static-links">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 hidden-xs component component-static-links">
                <div class="content content-static-links">
                    <ul>
                        <li><a href="/page/About+Us" title="About Us">About Us</a>|</li>
                        <li><a href="#" title="Shipping & Returns">Shipping & Returns</a>|</li>
                        <li><a href="#" title="Privacy Notice">Privacy Notice</a>|</li>
                        <li><a href="/page/Conditions+of+Use" title="Conditions of Use">Conditions of Use</a>|</li>
                        <li><a href="#" title="Contact Us">Contact Us</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-sm-4 col-xs-12 col-sm-pull-left component component-payment-methods">
                <div class="content content-payment-methods">
                    <ul>
                        <li class="payment-icon payment-icon-mastercard"></li>
                        <li class="payment-icon payment-icon-visa"></li>
                        <li class="payment-icon payment-icon-visa-electron"></li>
                        <li class="payment-icon payment-icon-maestro"></li>
                        <li class="payment-icon payment-icon-paypal"></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section section-footer section-static-links section-static-links-mobile visible-xs">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 component component-static-links">
                <div class="content content-static-links">
                    <ul>
                        <li><a href="/page/About+Us" title="About Us">About Us</a>|</li>
                        <li><a href="#" title="Shipping & Returns">Shipping & Returns</a>|</li>
                        <li><a href="#" title="Privacy Notice">Privacy Notice</a>|</li>
                        <li><a href="/page/Conditions+of+Use" title="Conditions of Use">Conditions of Use</a>|</li>
                        <li><a href="#" title="Contact Us">Contact Us</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="PoweredBy">
    <div class="section section-footer section-powered-by">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-xs-12 content content-pci">
                    <div class="security">
                        <div class="pci">
                            <#-- <a onclick="window.open('/page/PCI+Compliant?referrer=${section.domain}', '', 'toolbar=no, status=no, scrollbars=no, resizable=no, width=565, height=665')"> -->
                                <img src="/template/resource/runtime/542/g16/static/images/pciLogo.png" title="PCI Compliant" alt="PCI Compliant"/>
                            <#-- </a> -->
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12 content content-copyright">
                    <div class="copyright text-center">
                        <p>&copy;2014 PowaTag Showcase Demo Site</p>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12 content content-company">
                    <div class="company text-right">
                        <img src="/template/resource/runtime/542/g16/static/images/LocaytaLogo.png" title="Search technology provided by Locayta" alt="Search technology provided by Locayta"/>
                        <#-- <a href="${section.outerLink}" target="_blank"> -->
                            <img src="/template/resource/runtime/542/g16/static/images/poweredBy.png" title="Powa Website and Shopping Cart Solutions" alt="Powa Website and Shopping Cart Solutions"/>
                       <#--  </a> -->
                    </div>
                </div>
            </div>      
        </div>          
    </div>
</div>
<script type="text/javascript" src="${resourcePath}/js/combined.js"></script>
<script type="text/javascript" src="${resourcePath}/js/common.js"></script>
<script type="text/javascript" src="${resourcePath}/messages/${common.localeMessages.locale}/messages_${common.localeMessages.locale}.js"></script>
</body>
</html>