<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<script type="text/javascript"> <!--

function handleGeneralSubmit(form)
{
    if (validateForm(form, Validators.CreditCardDetails, document.getElementById('cc_error')))
    {
        return showPaymentInProgress();
    }
    return false;
}
// --> </script>

<div class="creditCardDetailsSection">
    <div class="block">
        <div class="corner1"><div class="corner2"><div class="lineTop">
            <#--<h2></h2>-->
        </div></div></div>
        <div class="cont">
<#-- Billing Details -->
            <#if (section.billingDetails?? && section.billingDetails.hasInfo)>
                <#assign bilDet = section.billingDetails.info>
    <div class="wrapperBillingDet">
        <div class="billingDet">
                <table class="base alignLeft">
                    <thead>
                        <tr>
                            <th class="first widthfix"><@js.message "billingDetails"/></th>
                            <th class="last"></th>
                        </tr>
                    </thead>
                    <tbody>
        <#assign i = 1/>
                        <#list bilDet as line>
            <#assign isEven = (i % 2 == 0)/>
                        <#if isEven>
                            <tr class="even">
                        <#else/>
                            <tr>
                        </#if>
                                <td class="name"><span>${line.name}</span></td>
                                <td class="last"><span>${line.value}</span></td>
                            </tr>
            <#assign i = i + 1/>
                        </#list>
                    </tbody>
                </table>
        </div>
    </div>
            </#if>
<#-- End Billing Details -->
            <span id="cc_error">
                <#if section.error??>
                <p class="error">
                   ${section.error}
                </p>
                </#if>
            </span>
<#-- Credit Card Details -->
            <form action="${section.submitLink}" method="POST" onsubmit="return handleGeneralSubmit(this)">
    <div class="wrapperCreditCardDet">
        <div class="creditCardDet">
                <table class="base alignLeft">
                <thead>
                    <tr>
                        <th class="first widthfix"><@js.message "change-credit-cart-details_newCardDetails"/></th>
                        <th class="last"></th>
                    </tr>
                </thead>
                        <tr class="even">
                            <td class="name"><span><@js.message "credit-cart-details_creditCardType"/></span></td>
                            <td class="last requiredParameter"><span>
                                <select class="card_type" name="creditCardType" autocomplete="off">
                                    <#if section.creditCardTypeMap??>
                                        <#assign types = section.creditCardTypeMap>
                                        <#list types?keys as key>
                                            <#if key==section.selCardType>
                                                <option value="${key}" selected="selected">${types[key]}</option>
                                            <#else>
                                                <option value="${key}">${types[key]}</option>
                                            </#if>
                                        </#list>
                                    </#if>
                                </select></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="name"><span><@js.message "credit-cart-details_cardNumber"/></span></td>
                            <td class="last requiredParameter"><span><label style="display:none"><@js.message "credit-cart-details_cardNumber"/></label><input class="textInput" name="cardNumber" value="${section.cardNumber}" autocomplete="off"/></span></td>
                        </tr>
                        <tr class="even">
                            <td class="name"><span><@js.message "credit-cart-details_expiryDate"/></span></td>
                            <td class="last requiredParameter"><div class="date">
                                <label><span><@js.message "credit-cart-details_month"/>:</span><select class="month" name="expireMonth" autocomplete="off">
                                            <option value=""> </option>
                                    <#list section.monthList as month>
                                        <#if month == section.selExpMonth>
                                            <option value="${month}" selected="selected">${month}</option>
                                        <#else>
                                            <option value="${month}">${month}</option>
                                        </#if>
                                    </#list>
                                </select></label>
                                <label class="floatRight"><span><@js.message "credit-cart-details_year"/>:</span><select class="year" name="expireYear" autocomplete="off">
                                            <option value=""> </option>
                                    <#list section.yearList as year>
                                        <#if year== section.selExpYear>
                                            <option value="${year}" selected="selected">${year}</option>
                                        <#else>
                                            <option value="${year}">${year}</option>
                                        </#if>
                                    </#list>
                                </select></label>
                            </div></td>
                        </tr>
                        <tr>
                            <td class="name"><span><@js.message "credit-cart-details_securityCode"/></span></td>
                            <td class="last requiredParameter"><span><label style="display:none"><@js.message "credit-cart-details_securityCode"/></label><input type="password" class="textInput" name="securityCode" value="${section.securityCode}" autocomplete="off"/></span></td>
                        </tr>
                        <tr class="even">
                        <td class="name"><span><@js.message "credit-cart-details_issueNumber"/></span></td>
                            <#assign value = section.selIssueNumber>
                            <td class="last"><span><label style="display:none"><@js.message "credit-cart-details_issueNumber"/></label><input class="textInput" name="issueNumber" value="${value}" autocomplete="off"/></span></td>
                        </tr>
                        <tr>
                            <td class="name"><span><@js.message "credit-cart-details_cardStartDate"/></span></td>
                            <td class="last"><div class="date">
                                <label><span><@js.message "credit-cart-details_month"/>:</span><select class="month" name="startMonth" autocomplete="off">
                                        <option value=""> </option>
                                    <#list section.monthList as month>
                                        <#if month==section.selStartMonth>
                                            <option value="${month}" selected="selected">${month}</option>
                                        <#else>
                                            <option value="${month}">${month}</option>
                                        </#if>
                                    </#list>
                                </select></label>
                                <label class="floatRight"><span><@js.message "credit-cart-details_year"/>:</span><select class="year" name="startYear" autocomplete="off">
                                        <option value=""> </option>
                                    <#list section.startYearList as year>
                                        <#if year == section.selStartYear>
                                            <option selected="selected" value="${year}">${year}</option>
                                        <#else>
                                            <option value="${year}">${year}</option>
                                        </#if>
                                    </#list>
                                </select></label></div>
                            </td>
                        </tr>
                    </table>
        </div>
    </div>
<#-- End Credit Card Details -->
                <input type="hidden" name="<@csrf.name/>" value="<@csrf.value/>"/>
                    <input class="bigBtn rightBtn" type="submit" value="<@js.message "change-credit-cart-details_updatePaymentCard"/>" />
                </form>
 <div class="nofloat"></div>
               </div>
               <div class="corner4"><div class="corner3"><div class="lineBtm"></div></div></div>
    </div>
    <div class="nofloat"></div>
</div>
