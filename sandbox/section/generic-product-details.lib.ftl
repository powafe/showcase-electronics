<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<#macro checkBox option pid updateLink >
    <span>
        <br/><label></label>
        <#assign chkd = "">

        <#if option.selected??>
            <#if option.selected == "on">
                <#assign chkd = "checked = true">
            </#if>
        </#if>
        <input id="${option.id}" name="${pid}.opt.${option.id}" class="checkbox" type="checkbox" onclick="updateProductPrice('${updateLink}')" ${chkd}></input>
        <label for="${option.id}" class="text">${option.name} <b>${option.price}</b></label>
    </span>
</#macro>

<#macro textField option pid>
    <#if option.selected??>
        <#assign text = option.selected>
    <#else>
        <#if option.values?first.name??>
            <#assign text = option.values?first.name>
        <#else>
            <#assign text = "">
        </#if>
    </#if>
    <span>
        <br/><label>${option.name}</label>
        <input name="${pid}.opt.${option.id}" class="textInput" value="${text}"></input>
    </span>
</#macro>

<#macro comboBox option pid updateLink>
    <#if option.selected??>
        <#assign sel = option.selected>
    <#else>
        <#assign sel = "">
    </#if>
    <span>
        <h5>${option.name}:&nbsp;</h5>
        <div class="styled-select">
            <select name="${pid}.opt.${option.id}" onchange="updateProductPrice('${updateLink}')">
                <option value=""><@js.message "pleaseSelect"/></option>
                <#assign choices = option.values>
                <#list choices as choice>
                    <#if (choice.price?length > 0)>
                        <#assign option_value = choice.name + ", " + choice.price>
                    <#else>
                        <#assign option_value = choice.name>
                    </#if>
                    <#if sel == choice.id>
                        <option selected="selected" value="${choice.id}">${option_value}</option>
                    <#else>
                        <option value="${choice.id}">${option_value}</option>
                    </#if>
                </#list>
            </select>
        </div>
    </span>
</#macro>

<#assign thumbnailSize = template.productFullSizeWidth/5-10 />
<#assign largeImageSize = template.productLargeWidth+20 />
<#assign imageSize = template.productFullSizeWidth />

<#macro optForm>
    <form id="optForm" action="${section.product.buyLink}" method="post">
        <input id="tabUID" name="tabUID" type="hidden" value="${section.tabUID}" />
        <input id="pid" name="pid" type="hidden" value="${section.product.pid}"></input>
        <#if section.linkedProducts.currentlySelected != ' '>
            <input name="cs" type="hidden" value="${section.linkedProducts.currentlySelected}"></input>
        </#if>
        <#if section.product.canBeBought>
            <input type="hidden" name="<@csrf.name/>" value="<@csrf.value/>"/>
        </#if>

        <#nested />
    </form>
</#macro>
