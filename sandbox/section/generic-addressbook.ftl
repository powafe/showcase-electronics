<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->

<span id="address_result">
    <#if section.result??>
        <p class="warning">
            ${section.result}
        </p>
    </#if>
</span>

<#if section.addressBook?has_content>
<table class="base alignLeft">
    <tr>
        <td>Address</td>
        <td>Edit</td>
        <td>Delete</td>
    </tr>

    <#list section.addressBook as address>
        <tr>
            <td>${address.firstName} ${address.lastName} ${address.addressLine1} ${address.addressLine2} ${address.town}</td>
            <td><a href="edit.html">Edit</a></td>
            <td><a href="delete.html">Delete</a></td>
        </tr>
    </#list>

</table>
</#if>

<br/>
<br/>

<a href="#" onclick="$('#addressEntry').show()">Add an address manually</a>

<div id="addressEntry" hidden="true">
    <form id="enterAddress" method="POST" action="${section.submitLink}">
        firstName <input type="text" name="firstName" value="${(section.customerDetails.firstName)!}"/><br/>
        lastName <input type="text" name="lastName" value="${(section.customerDetails.lastName)!}"/><br/>
        <input type="submit" value="Submit">
        <input type="button" value="Cancel" onclick="$('#addressEntry').hide()">
    </form>
</div>