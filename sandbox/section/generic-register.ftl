<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<#include "/${common.templatePath}password-policy-popup.ftl"/>
<div class="accountSection">
<div class="form">
    <div class="block">
        <div class="corner1">
            <div class="corner2">
                <div class="lineTop"></div>
            </div>
        </div>
        <div class="cont">
            <span id="register_error">
                <#if section.error??>
                <p class="error">
                   ${section.error}
                </p>
                </#if>
            </span>
            <form action="${section.submitLink}" method="POST" onsubmit="return validateForm(this, Validators.CustomerRegistration, document.getElementById('register_error'));">
                <input type="hidden" name="<@csrf.name/>" value="<@csrf.value/>"/>
                <div class="loginDetails">
                    <h4><@js.message "edit-profile-register_loginDetails"/></h4>
                    <div class="username">
                        <label class="requiredParameter"><@js.message "username"/></label><input class="textInput"
                        <#if section.userName??>
                            value="${section.userName}"
                        </#if>
                        name="userName" type="text" autocomplete="off"/>
                    </div>
                    <div class="password">
                        <label class="requiredParameter"><@js.message "password"/></label>
                        <#if section.password??>
                            <#assign pass = section.password>
                        <#else>
                            <#assign pass = "">
                        </#if>
                        <input class="textInput" value="${pass}" name="password" type="password"/>
                    </div>
                    <div class="confirmPassword">
                        <label class="requiredParameter"><@js.message "register_confirmPassword"/></label>
                        <#if section.password?? && section.confirmPassword??>
                            <#assign confPass = section.confirmPassword>
                        <#else>
                            <#assign confPass = "">
                        </#if>
                        <input class="textInput" value="${confPass}" name="confirmPassword" type="password"/>
                    </div>
                </div>
                <div class="horLine"></div>
                <div class="personalDetails">
                    <h4><@js.message "edit-profile-register_personalDetails"/></h4>
                    <div class="title">
                        <label><@js.message "title"/></label><input class="textInput"
                        <#if section.title??>value="${section.title}"</#if>
                        name="title" type="text"/>
                    </div>
                    <div class="firstName">
                        <label class="requiredParameter"><@js.message "firstName"/></label><input class="textInput"
                        <#if section.firstName??>value="${section.firstName}"</#if>
                        name="firstName" type="text"/>
                    </div>
                    <div class="lastName">
                        <label class="requiredParameter"><@js.message "lastName"/></label><input class="textInput"
                        <#if section.lastName??>value="${section.lastName}"</#if>
                        name="lastName" type="text"/>
                    </div>
                    <div class="company">
                        <label><@js.message "company"/></label><input class="textInput"
                        <#if section.company??>value="${section.company}"</#if>
                        name="company" type="text"/>
                    </div>
                </div>
                <div class="horLine"></div>
                <div class="contactDetails">
                    <h4><@js.message "edit-profile-register_contactDetails"/></h4>
                    <div class="email">
                        <label class="requiredParameter"><@js.message "email"/></label><input class="textInput"
                        <#if section.email??>value="${section.email}"</#if>
                        name="email" type="text"/>
                    </div>
                    <div class="confirmEmail">
                        <label class="requiredParameter"><@js.message "register_confirmEmail"/></label><input class="textInput"
                        <#if section.confirmEmail??>value="${section.confirmEmail}"</#if>
                        name="confirmEmail" type="text"/>
                    </div>
                    <div class="phone">
                        <label class="requiredParameter"><@js.message "phone"/></label><input class="textInput"
                        <#if section.phone??>value="${section.phone}"</#if>
                        name="phone" type="text"/>
                    </div>
                </div>
                <div class="horLine"></div>
                <div class="addressDetails">
                    <h4><@js.message "edit-profile-register_addressDetails"/></h4>
                    <div class="addressLine1">
                        <label class="requiredParameter"><@js.message "addressLine"/> 1</label><input class="textInput"
                        <#if section.address1??>value="${section.address1}"</#if>
                        name="address1" type="text"/>
                    </div>
                    <div class="addressLine2">
                        <label><@js.message "addressLine"/> 2</label><input class="textInput"
                        <#if section.address2??>value="${section.address2}"</#if>
                        name="address2" type="text"/>
                    </div>
                    <div class="townCity">
                        <label class="requiredParameter"><@js.message "edit-profile-register_town"/></label><input class="textInput"
                        <#if section.town??>value="${section.town}"</#if>
                        name="town" type="text"/>
                    </div>
                    <div class="countyState" id="states">
                        <#include "/${common.templatePath}states-block.ftl"/>
                    </div>
                    <div class="postcode">
                        <label class="requiredParameter"><@js.message "postcode"/></label><input class="textInput"
                        <#if section.postcode??>value="${section.postcode}"</#if>
                        name="postcode" type="text"/>
                    </div>
                    <div class="country">
                        <label class="requiredParameter"><@js.message "country"/></label>
                        <select name="countryCode" onchange="countryChange(this);">
                        <#list section.countryList as country>
                            <option value="${country.code}"
                            <#if section.countryCode?? && country.code == section.countryCode>selected</#if>
                            >${country.name}</option>
                        </#list>
                        </select>
                    </div>
                    <div class="subscribedNews">
                        <label>&nbsp;</label><div class="checkboxBlock"><input id="checkbox" class="checkbox" name="subscribed2News" <#if section.subscribed2News>checked </#if>type="checkbox"/><label for="checkbox" class="text"><@js.message "edit-profile-register_subscribeToNewsletter"/></label></div>
                        <div class="nofloat"></div>
                    </div>
                </div>
                <input class="middleBtn rightBtn" value="<@js.message "register_register"/>" type="submit"/>

                <div class="nofloat"></div>
            </form>
        </div>
        <div class="corner4">
            <div class="corner3">
                <div class="lineBtm"></div>
            </div>
        </div>
    </div>
</div>
</div>
