<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<script type="text/javascript"> <!--
function closeAllSubMenu(){
    if (document.getElementById('mainMenu') == null) return;
    var arrayDiv = document.getElementById('mainMenu').getElementsByTagName('div');
    arrayLI = document.getElementById('mainMenu').getElementsByTagName('li');
    for (i = 0; i < arrayDiv.length; i++)
        arrayDiv[i].style.visibility = 'hidden';
    for (i = 0; i < arrayLI.length; i++)
        arrayLI[i].className = 'none';
}

function searchDIV(nameDiv){
    var arrayDiv = document.getElementsByTagName('div');
    i = 0;
    while (( i < arrayDiv.length ) && ( arrayDiv[i].className != nameDiv )) i++;
    return arrayDiv[i];
}

function widthDIV(nameDiv){
    return searchDIV(nameDiv).offsetWidth;
}


function searchCSSName(nameCSS){
    rul = document.styleSheets[0].rules;
    i = 0;
    if (rul == null)
        return (null);
    while (( i < rul.length ) && ( rul[i].selectorText != nameCSS ))
        i++;
    if (( i != rul.length ) && ( rul[i].selectorText == nameCSS ))
        return( rul[i] );
    return( null );
}

function displaySubMenu(itemName, subName){
    closeAllSubMenu();
    $('#'+subName+'').css('visibility','visible');
    $('#'+itemName+'').addClass('chosen');

    if (document.getElementById(subName).style.left == '')
    {
        if (document.getElementById(searchDIV('mainMenu').className) != null)
        {
            leftPosition_Submenu = ( document.getElementById(itemName).offsetLeft + document.getElementById(itemName).offsetWidth / 2 ) - document.getElementById(subName).offsetWidth / 2;
            widthMenu = widthDIV('mainMenu');
            if ((leftPosition_Submenu + document.getElementById(subName).offsetWidth) > widthMenu)
                document.getElementById(subName).style.left = widthMenu - document.getElementById(subName).offsetWidth + 'px';
            else if (leftPosition_Submenu < 0)
                document.getElementById(subName).style.left = 0 + 'px'
            else document.getElementById(subName).style.left = leftPosition_Submenu + 'px';
        }
    }
    if (( document.styleSheets ) && ( (css = searchCSSName('.mainSubMenu_exp') ) != null)) document.getElementById(subName).style.marginTop = css.style.marginTop;
}

function hideSubMenu(itemName, subName){
    $('#'+subName+'').css('visibility','hidden');
    if ($('#'+itemName+'').hasClass('chosen')) $('#'+itemName+'').removeClass('chosen');
}
// --> </script>
<div class="nofloat"></div>
<div class="wrapperMenu">
    <div class="mainMenu">
        <div class="block">
            <div class="corner1">
                <div class="corner2">
                    <div class="lineTop"><h3 class="skip"><@js.message "main-menu_mainMenu"/></h3></div>
                </div>
            </div>
            <div class="cont">
                <ul>
                    <#assign pageCount = section.pages?size/>
                    <#assign i = 0/>
                    <#assign className = ""/>
                    <#list section.pages as page>
                    <#if (page.type == 1)>
                        <#assign className = "home" />
                    </#if>
                    <#if (page.type == 2)>
                        <#assign className = "category" />
                    </#if>
                    <#if (page.type != 1) && (page.type != 2)>
                        <#assign className = "info" />
                    </#if>
                        <#if (i == pageCount - 1)>
                            <#assign className = className + " last" />
                        </#if>
                        <li class="${className}" id="item_${i}">
                            <#assign eventHandlers = ""/>
                            <#if (page.children??) && (page.children?size > 0)>
                                <div class="mainSubMenu" id="sub_${i}" onmouseout="javascript:hideSubMenu('item_${i}','sub_${i}');" onmouseover="javascript:displaySubMenu('item_${i}','sub_${i}');">
                                <div class="c1">
                                    <div class="c2">
                                        <div class="lTop"></div>
                                    </div>
                                </div>
                                <div class="cont">
                                    <ul>
                                    <#assign childCount = page.children?size/>
                                    <#assign j = 0/>
                                    <#assign childClassName = ""/>
                                    <#list page.children as child>
                                        <#if (j == childCount - 1)>
                                            <#assign childClassName = "last" />
                                        </#if>
                                        <li class="${childClassName}"><a href="${child.link}">${child.name}</a></li>
                                        <#assign j = j+1/>
                                    </#list>
                                    </ul>
                                </div>
                                <div class="c4">
                                    <div class="c3">
                                        <div class="lBtm"></div>
                                    </div>
                                </div>
                                </div>
                                <a href="${page.link}" onmouseout="javascript:hideSubMenu('item_${i}','sub_${i}');" onmouseover="javascript:displaySubMenu('item_${i}','sub_${i}');"><span class="l">&nbsp;</span><span class="name">${page.name}</span></a>
                                <#else/>
                                <a href="${page.link}"><span class="l">&nbsp;</span><span class="name">${page.name}</span></a>
                            </#if>

                        </li>
                        <#assign i = i+1 />
                    </#list>
                </ul>
            </div>
            <div class="corner4">
                <div class="corner3">
                    <div class="lineBtm"></div>
                </div>
            </div>
        </div>
        <div class="nofloat"></div>
    </div>
</div>