<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<#if (section.pages??) && (section.pages?size > 0)>
<div class="background-full background-bar-shadow hidden-lg"></div>
<div class="breadcrumbSection background-full background-page-title visible-lg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 component component-breadcrumb">
                <ul>
                    <#assign pageCount = section.pages?size/>
                    <#assign i = 0/>
                    <#assign className = "first"/>
                    <#list section.pages as page>
                        <#if (i == pageCount - 1)>
                            <#assign className = "last" />
                            <#else/>
                            <#if (i != 0)>
                                <#assign className = "" />
                            </#if>
                        </#if>
                        <li class="${className}">
                            <#if i != pageCount - 1 || section.includeLast>
                                <a href="${page.link}">${page.name}</a>&nbsp;&nbsp;>&nbsp;&nbsp;
                                <#else/>
                                ${page.name}
                            </#if>
                        </li>
                        <#assign i = i + 1 />
                    </#list>
                </ul>
            </div>
        </div>
    </div>
</div>
</#if>