<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<div class="col-md-9 col-xs-12 orderDetailsSection section section-shopping-cart section-order-details">
    <span id="error">
        <#if section.error??>
        <p class="error">
           ${section.error}
        </p>
        </#if>
    </span>
    <#-- Order Details -->
    <#if section.orderData??>
    <div style="display: none">
        <input id="orderId" value="${section.orderData.id}" type="hidden"/>
        <#assign raw = section.rawOrderData />
        <input id="orderSubtotal" value="${raw.subtotal}" type="hidden"/>
        <input id="orderTotal" value="${raw.total}" type="hidden"/>
        <input id="shippingCost" value="${raw.shippingCost}" type="hidden"/>
        <input id="currencyCode" value="${raw.currencyCode}" type="hidden"/>
        <input id="netOrderSubtotal" value="${raw.netSubtotal}" type="hidden"/>
        <input id="discountTotal" value="${raw.discountTotal}" type="hidden"/>
    </div>

    <div class="wrapperOrderDet component component-order-details">
        <div class="orderDet">
        
                    <p class="first widthfix">Thank you for your order.</p>

                    <p class="orderNo">
                        <span class="name"><@js.message "order_no"/>:</span>
                        <span class="last">${section.orderData.id}</span>
                    </p>
                    <p class="orderDate even">
                        <span class="name"><@js.message "order_date"/>:</span>
                        <span class="last">${section.orderData.orderDateObj?string('dd MMM yyyy')}</span>
                    </p>
        </div>
        <input type="hidden" name="prevLink" value="${section.currentLink}"/>
        <form action="${section.submitLink}" enctype="text/plain">
            <button class="button button-green" type="submit">GO TO HOME PAGE ></button>
        </form>
    </div>
    </#if>
    <#-- End Order Details -->

    <#-- Shopping Cart Details -->
   <#if section.itemList??>
    <div class="wrapperShoppingCartDet component component-shopping-cart-details">
        <div class="shoppingCartDet">
            <table class="base">
                <thead>
                    <tr>
                        <th><@js.message "productName"/></th>
                        <th>Qty</th>
                        <th><@js.message "price"/></th>
                        <th class="hidden-xs"><@js.message "subtotal"/></th>
                    </tr>
                </thead>
                <tbody>
    <#assign i = 1/>
    <#list section.itemList as item>
    <#assign isEven = (i % 2 == 0)/>
        <#if isEven>
            <#assign trclass = "even"/>
        <#else/>
            <#assign trclass = ""/>
        </#if>
            <#if item.options??>
                <#assign rowspan = item.options?size + 1>
                    <tr class="${trclass}">
                        <td class="name">
                            <a href="${item.link}">
                                <img class="image-thumb" alt="${item.name} - ${item.SKU}" title="${item.name} - ${item.SKU}" src="${item.imageLink}"/>${item.name}
                                <#list item.options as option>
                                <p class="$nameOption">
                                    <span class="nameOption">${option.name}: <b>${option.value}</b></span>
                                    <span class="price">${option.price}</span>
                                </p>
                                </#list>
                            </a>
                        </td>
                        <td class="qty" rowspan="${rowspan}">${item.quantity}</td>
                        <td class="price">${item.price}</td>
                        <td class="price last hidden-xs" rowspan="${rowspan}">${item.subtotal}</td>
                    </tr>
            <#else>
                    <tr class="${trclass}">
                        <td class="name"><a href="${item.link}"><img class="image-thumb" alt="${item.name} - ${item.SKU}" title="${item.name} - ${item.SKU}" src="${item.imageLink}"/>${item.name}</a></td>
                        <td class="qty">${item.quantity}</td>
                        <td class="price">${item.price}</td>
                        <td class="price last hidden-xs">${item.subtotal}</td>
                    </tr>
            </#if>
    <#assign i = i + 1/>
    </#list>
                </tbody>
            </table>
        </div>
    </div> 
    </#if>
    <#-- End Shopping Cart Details -->

    <#-- Billing Details -->
    <#if (section.billingDetails?? && section.billingDetails.hasInfo)>
    <#assign bilDet = section.billingDetails.info>
    <div class="wrapperBillingDet component component-address-details left">
        <div class="billingDet">
            <table class="base alignLeft">
                <thead>
                    <tr>
                        <th class="first widthfix"><@js.message "billingDetails"/></th>
                        <th class="last"></th>
                    </tr>
                </thead>
                <tbody>
    <#assign i = 1/>
    <#list bilDet as line>
        <#assign className = "${line.name?replace(' ', '')}"/>
        <#assign className = "${className?replace('/', '')}"/>
        <#assign className = "${className?uncap_first}"/>
        <#assign isEven = (i % 2 == 0)/>
        <#if isEven>
                    <tr class="${className} even">
        <#else/>
                    <tr class="${className}">
        </#if>
                        <td class="name"><span>${line.name}</span></td>
                        <td class="last"><span>${line.value}</span></td>
                    </tr>
        <#assign i = i + 1/>
    </#list>
                </tbody>
            </table>
        </div>
    </div>
    </#if>
    <#-- End Billing Details -->

    <#-- Shipping Details -->
    <#if (section.shippingDetails?? && section.shippingDetails.hasInfo)>
    <#assign shipDet = section.shippingDetails.info>
    <div class="wrapperShippingDet component component-address-details">
        <div class="shippingDet">
            <table class="base alignLeft">
                <thead>
                    <tr>
                        <th class="first widthfix"><@js.message "shippingDetails"/></th>
                        <th class="last"></th>
                    </tr>
                </thead>
                <tbody>
    <#assign i = 1/>
    <#list shipDet as line>
        <#assign className = "${line.name?replace(' ', '')}"/>
        <#assign className = "${className?replace('/', '')}"/>
        <#assign className = "${className?uncap_first}"/>
        <#assign isEven = (i % 2 == 0)/>
            <#if isEven>
                    <tr class="${className} even">
            <#else/>
                    <tr class="${className}">
            </#if>
                        <td class="name"><span>${line.name}</span></td>
                        <td class="last"><span>${line.value}</span></td>
                    </tr>
        <#assign i = i + 1/>
        </#list>
                </tbody>
            </table>
        </div>
    </div>
    </#if>
    <#-- End Shipping Details -->

    <#-- Totals Block -->
    <#if section.shoppingCartData??>
    <div class="wrapperTotals component component-order-totals">
        <div class="totals">
            <table class="totalBlock">
                <thead>
                    <tr>
                        <th><@js.message "subtotal"/></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="productSubtotal">
                        <td><span><@js.message "productSubtotal"/></span></td>
                        <td class="price last"><span>${section.shoppingCartData.subtotal}</span></td>
                    </tr>
                    <tr class="shippingTotal">
                        <td><span><@js.message "shipping"/></span></td>
                        <td class="price last"><span>${section.shoppingCartData.shippingTotal}</span></td>
                    </tr>
                    <#if section.orderData.downloadsUri??>
                    <tr class="shippingTotal">
                        <td><span>Digital Download url</span></td>
                        <td class="price last"><span><a href="${section.orderData.downloadsUri}">Download now!</a></span></td>
                    </tr>
                    </#if>
                    <#if section.orderData.digitalDownloadExpiryDate??>
                    <tr class="shippingTotal">
                        <td><span>Digital Download Expiry date</span></td>
                        <td class="price last"><span>${section.orderData.digitalDownloadExpiryDate}</span></td>
                    </tr>
                    </#if>
                    <#if section.shoppingCartData.discount??>
                        <tr class="discount">
                            <td><span>${section.shoppingCartData.discount.name}</span></td>
                            <td class="price last"><span>- ${section.shoppingCartData.discount.price}</span></td>
                        </tr>
                    </#if>

                    <#if section.shoppingCartData.oldTaxList??>
                        <#list section.shoppingCartData.oldTaxList as tax>
                    <#assign className = "${tax.name?replace(' ', '')}"/>
                    <#assign className = "${className?uncap_first}"/>
                    <tr class="${className}">
                        <td><span>${tax.name}</span></td>
                        <td class="price last"><span>${tax.cost}</span></td>
                    </tr>
                        </#list>
                    <tr class="total">
                        <td><span><@js.message "total"/></span></td>
                        <td class="price last">
                            <#if section.shoppingCartData.total??><span>${section.shoppingCartData.total}</span></#if>
                        </td>
                    </tr>
                    <#else>
                    <#if section.shoppingCartData.taxIncluded>
                        <#if section.shoppingCartData.flatTaxList??>
                        <#list section.shoppingCartData.flatTaxList as tax>
                            <#assign className = "${tax.name?replace(' ', '')}"/>
                            <#assign className = "${className?uncap_first}"/>
                    <tr class="${className}">
                        <td><span>${tax.name}</span></td>
                        <td class="price last"><span>${tax.cost}</span></td>
                    </tr>
                        </#list>
                        </#if>
                    <tr class="total">
                        <td><span><@js.message "total"/></span></td>
                        <td class="price last">
                            <#if section.shoppingCartData.total??><span>${section.shoppingCartData.total}</span></#if>
                        </td>
                    </tr>
                    <#else>
                        <#if section.shoppingCartData.percTaxList??>
                        <#list section.shoppingCartData.percTaxList as tax>
                            <#assign className = "${tax.name?replace(' ', '')}"/>
                            <#assign className = "${className?uncap_first}"/>
                    <tr class="${className}">
                        <td><span>${tax.name}</span></td>
                        <td class="price last"><span>${tax.cost}</span></td>
                    </tr>
                        </#list>
                        </#if>
                        <#if section.shoppingCartData.flatTaxList??>
                        <#list section.shoppingCartData.flatTaxList as tax>
                            <#assign className = "${tax.name?replace(' ', '')}"/>
                            <#assign className = "${className?uncap_first}"/>
                    <tr class="${className}">
                        <td><span>${tax.name}</span></td>
                        <td class="price last"><span>${tax.cost}</span></td>
                    </tr>
                        </#list>
                        </#if>
                    <tr class="total">
                        <td><span><@js.message "total"/></span></td>
                        <td class="price last">
                            <#if section.shoppingCartData.total??><span>${section.shoppingCartData.total}</span>
                            </#if>
                        </td>
                    </tr>
                    </#if>
                </#if>
                </tbody>
            </table>
        </div>
    </div>
    <#-- End Totals Block -->

    <input type="hidden" name="prevLink" value="${section.currentLink}"/>
    <form action="${section.submitLink}" enctype="text/plain">
        <button class="button button-green button-last" type="submit">GO TO HOME PAGE ></button>
    </form>

</div>
<#else>
    <#if !section.error??>
        <@js.message "order-details_thankYouForOrder"/>
    </#if>
</#if>
<#if section.googleAnalyticsEcommerceOrderData?? && common.googleAnalyticsAccount??>

    <#-- UPDATED ECOMMERCE GOOGLE ANALYTICS USING THE ASYNC METHOD -->

        <script type="text/javascript">

			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', '${common.googleAnalyticsAccount}']);
			_gaq.push(['_setDomainName', 'none']);
			_gaq.push(['_setAllowLinker', true]);

            <#assign order = section.googleAnalyticsEcommerceOrderData>

            _gaq.push(['_addTrans',
                "<#if order.orderId??>${order.orderId}</#if>",
                "<#if order.affiliation??>${order.affiliation}</#if>",
                "<#if order.total??>${order.total}</#if>",
                "<#if order.taxTotal??>${order.taxTotal}</#if>",
                "<#if order.shipping??>${order.shipping}</#if>",
                "<#if order.city??>${order.city}</#if>",
                "<#if order.state??>${order.state}</#if>",
                "<#if order.country??>${order.country}</#if>"
            ]);

            <#if order.googleAnalyticsEcommerceOrderItemData??>
                <#list order.googleAnalyticsEcommerceOrderItemData as item>
                    _gaq.push(['_addItem',
                        "<#if order.orderId??>${order.orderId}</#if>",
                        "<#if item.SKU??>${item.SKU}</#if>",
                        "<#if item.productName??>${item.productName}</#if>",
                        "<#if item.category??>${item.category}</#if>",
                        "<#if item.price??>${item.price}</#if>",
                        "<#if item.quantity??>${item.quantity}</#if>"
                    ]);
                </#list>
            </#if>

            _gaq.push(['_trackTrans']);

            (function() {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();

        </script>

    <#-- /END OF UPDATED ECOMMERCE GOOGLE ANALYTICS USING THE ASYNC METHOD -->

</#if>
