<#--
 * Copyright (c) 2007-2013 Powa Technologies Limited.  All Rights Reserved.
 *
 * Distribution, copy or usage of any part of this file is prohibited without
 * written consent of Powa Technologies Limited.
 -->
<#macro printValues lines>
    <#list lines as line>
        <#if line?? && line.value!="">${line.value} <br /></#if>
    </#list>
</#macro>

<#if section.orders??>
    <#list section.orders as theOrder>
        <#if section.header??>
            ${section.header}
        </#if>

        <div class='order'>
            <div class='float-left'>
                    <b><@js.message "billingDetails"/></b>
                    <div>
                        <@printValues theOrder.billingDetails.info/>
                    </div>
                    <div class='height10'></div>
                    <b><@js.message "shippingDetails"/></b>
                    <div>
                        <#if theOrder.shippingDetails?? && theOrder.shippingDetails.hasInfo>
                            <@printValues theOrder.shippingDetails.info/>
                        <#else>
                            <@printValues theOrder.billingDetails.info/>
                        </#if>
                    </div>
            </div>
            <div class='float-right'>

                <label><@js.message "print-order_invoiceNumber"/></label>
                <div class='value'><#if theOrder.invoiceNumber??>${theOrder.invoiceNumber}</#if></div>

                <label><@js.message "order_orderNo"/></label>

                <div class='value'><#if theOrder.orderNumber??>${theOrder.orderNumber}</#if></div>

                <label><@js.message "print-order_orderDate"/></label>
                <div class='value'> ${theOrder.orderData.orderDateObj?string('yyyy-MMM-dd HH:mm')}</div>

                <div class='height10'></div>
                <label><@js.message "paymentMethod"/></label>

                <div class='value'>${theOrder.paymentMethod}</div>

                <label><@js.message "print-order_transactionRef"/></label>

                <div class='value'><#if theOrder.transactionId??>${theOrder.transactionId}</#if></div>

                <#if (theOrder.orderData.externalUkashTransactionRef)??>
                    <label><@js.message "print-order_ukashTransactionRef"/></label>

                    <div class='value'>${theOrder.orderData.externalUkashTransactionRef}</div>
                </#if>

                <div class='height10'></div>
                <label><@js.message "shippingMethod"/></label>

                <div class='value'><#if theOrder.shippingMethod??>${theOrder.shippingMethod}</#if></div>
                <label><@js.message "print-order_shippingWeight"/></label>

                <div class='value'><#if theOrder.totalWeight??>${theOrder.totalWeight}</#if></div>

            </div>

            <div class='nofloat'></div>

            <table>
                <thead>
                    <tr>
                        <th width='80'><@js.message "sku"/></th>
                        <th width='434'><@js.message "print-order_product"/></th>
                        <th><@js.message "price"/></th>
                        <th><@js.message "quantity"/></th>
                        <th><@js.message "subtotal"/></th>
                    </tr>
                </thead>
                <tbody>
                    <#if theOrder.itemList??>
                        <#list theOrder.itemList as item>
                <#if item.options??>
                <#assign rowspan = item.options?size + 1>

                                <tr class="optionLine1">
                                    <#if item.SKU??>
                                        <#assign code = item.SKU>
                                    <#else>
                                        <#assign code = "">
                                    </#if>
                                   <td><#if !theOrder.orderData.creditNote><b>${code}</b></#if></td>
                                   <td class="name"><b>${item.name}</b></td>
                                   <td valign='top' align='right'>${item.price}</td>
                                   <td rowspan="${rowspan}" align='right'>${item.quantity}</td>
                                   <td rowspan="${rowspan}" align='right'><b>${item.subtotal}</b></td>
                    </tr>
                <#list item.options as option>
                    <tr class="optionLine2">
                   <td><#if option.sku??>${option.sku}</#if></td>
                   <td>${option.name}: ${option.value}</td>
                   <td align='right'>${option.price}</td>
                    </tr>
                </#list>
               <#else/>
                                <tr>
                                    <#if item.SKU??>
                                        <#assign code = item.SKU>
                                    <#else>
                                        <#assign code = "">
                                    </#if>
                                   <td><#if !theOrder.orderData.creditNote><b>${code}</b></#if></td>
                                   <td class="name"><b>${item.name}</b></td>
                                   <td valign='top' align='right'>${item.price}</td>
                                   <td align='right'>${item.quantity}</td>
                                   <td align='right'><b>${item.subtotal}</b></td>
                    </tr>
               </#if>
                        </#list>
                    </#if>
                </tbody>
            </table>
            <table class='total float-right'>
                <tr><td><@js.message "subtotal"/></td>
                <td align='right'>${theOrder.shoppingCartData.subtotal}</td></tr>
                <tr><td><@js.message "print-order_shippingCost"/></td>
                <td align='right'>${theOrder.shoppingCartData.shippingTotal}</td></tr>
                <#if theOrder.shoppingCartData.discount??>
                    <tr><td>${theOrder.shoppingCartData.discount.name}</td>
                <td align='right' class='red'>- ${theOrder.shoppingCartData.discount.price}</td></tr>
                </#if>
                <#if theOrder.shoppingCartData.oldTaxList??>
                    <#list theOrder.shoppingCartData.oldTaxList as tax>
                        <tr><td>${tax.name}</td>
                <td align='right'>${tax.cost}</td></tr>
                    </#list>
                    <tr><td><@js.message "total"/></td>
                <td align='right'>${theOrder.shoppingCartData.total}</td></tr>
                <#else>
                    <#if theOrder.shoppingCartData.taxIncluded>
                        <#if theOrder.shoppingCartData.flatTaxList??>
                        <#list theOrder.shoppingCartData.flatTaxList as tax>
                            <tr><td>${tax.name}</td>
                <td align='right'>${tax.cost}</td></tr>
                        </#list>
                        </#if>
                        <tr><td><@js.message "total"/></td>
                <td align='right'>${theOrder.shoppingCartData.total}</td></tr>
                        <#list theOrder.shoppingCartData.percTaxList as tax>
                            <tr><td>${tax.name}</td>
                <td align='right'>${tax.cost}</td></tr>
                        </#list>
                    <#else>
                        <#if theOrder.shoppingCartData.percTaxList??>
                        <#list theOrder.shoppingCartData.percTaxList as tax>
                            <tr><td>${tax.name}</td>
                <td align='right'>${tax.cost}</td></tr>
                        </#list>
                        </#if>
                        <#if theOrder.shoppingCartData.flatTaxList??>
                        <#list theOrder.shoppingCartData.flatTaxList as tax>
                            <tr><td>${tax.name}</td>
                <td align='right'>${tax.cost}</td></tr>
                        </#list>
                        </#if>
                        <tr><td><@js.message "total"/></td>
                <td align='right'><b>${theOrder.shoppingCartData.total}</b></td></tr>
                    </#if>
                </#if>
            </table>
        </div>
       <div class='horLine'></div>
        <#if section.footer??>
        ${section.footer}
        </#if>

        <#if section.printDeliveryNote>
    <div class='delivery'>
        <p><img src='${common.resourcePath}/images/scissors.png' alt=''/></p>
            <div class="details">
                <label>Shipping Details</label>
                <div class='value'>
                    <#if theOrder.shippingDetails?? && theOrder.shippingDetails.hasInfo>
                            <@printValues theOrder.shippingDetails.shortInfo/>
                        <#else>
                    <@printValues theOrder.billingDetails.shortInfo/>
                    </#if>
                </div>
            </div>
        </div>
        <div class="horLine"></div>
    </div>
        </#if>
    <#--PAGE BREAK STARTS-->
        <#if theOrder_has_next>
        <p style="page-break-before: always"></p>
        </#if>
    <#--PAGE BREAK ENDS-->
    </#list>
</#if>
